package pl.taktycznie.egory;


import android.app.Activity;

import androidx.appcompat.widget.Toolbar;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import androidx.test.runner.lifecycle.Stage;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;

import pl.taktycznie.egory.utilities.ClickOnEditButton;
import pl.taktycznie.egory.utilities.MyEditAction;
import pl.taktycznie.egory.utilities.MyUtils;
import pl.taktycznie.egory.view.activities.GotPathsMngmtActivity;
import pl.taktycznie.egory.view.activities.MainActivity;
import pl.taktycznie.egory.view.activities.PathListActivity;

import static androidx.test.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class PathModificationTest {

    private final int TIME_TO_WAIT = 4000; // ms

    @Rule
    public IntentsTestRule<MainActivity> intentsTestRule = new IntentsTestRule<>(MainActivity.class);
    public ActivityTestRule<PathListActivity> pathActivityTestRule = new ActivityTestRule<>(
            PathListActivity.class);

    @Test
    public void eGoryNameTest(){
        String toolbarTitile = getInstrumentation().getTargetContext().getString(R.string.app_name);
        onView(allOf(isAssignableFrom(TextView.class), withParent(isAssignableFrom(Toolbar.class)))).check(matches(withText(toolbarTitile)));
        //onView(isAssignableFrom(AppCompatTextView.class)).check(matches(withText(R.string.app_name)));
    }

    @Test public void gotPathAddTest(){

        onView(withId(R.id.switch_isLeader)).perform(click());
        goToPathList();

        onView(withId(R.id.button_add))
                .perform(click());

        waitForApp(TIME_TO_WAIT);

        fillAddTest();

        onView(withId(R.id.button_add_path_agree))
                .perform(click());

    }

    @Test
    public void gotPathAddCancelTest(){

        onView(withId(R.id.switch_isLeader)).perform(click());
        goToPathList();

        onView(withId(R.id.button_add))
                .perform(click());

        waitForApp(TIME_TO_WAIT);

        fillAddTest();

        onView(withId(R.id.button_add_path_cancel))
                .perform(click());

    }

    @Test
    public void gotPathManagementTest(){

        String upPoints = "18";
        String downPoints = "16";
        int countOfElements = 1;
        onView(withId(R.id.switch_isLeader)).perform(click());

        goToPathList();

        waitForApp(TIME_TO_WAIT/2);

        if (checkIfListOfValuesIsGraterThan(countOfElements)){
            onView(withId(R.id.paths_recycler_view)).perform(
                    RecyclerViewActions.actionOnItemAtPosition(0, MyEditAction.clickChildViewWithId(R.id.button_modify)));

            waitForApp(TIME_TO_WAIT/3);

            onView(withId(R.id.editText_modify_path_points_UP))
                    .perform(clearText());
            onView(withId(R.id.editText_modify_path_points_UP))
                    .perform(typeText(upPoints));

            onView(withId(R.id.editText_modify_path_points_DN))
                    .perform(clearText());
            onView(withId(R.id.editText_modify_path_points_DN))
                    .perform(typeText(downPoints));

            waitForApp(500);

            closeSoftKeyboard();

            onView(withId(R.id.button_modify_path_agree))
                    .perform(click());

        }
        else {
            onView(withId(R.id.paths_recycler_view)).perform(
                    RecyclerViewActions.actionOnItemAtPosition(0, MyEditAction.clickChildViewWithId(R.id.button_modify)));

            waitForApp(TIME_TO_WAIT/3);

            onView(withId(R.id.button_modify_path_cancel))
                    .perform(click());
        }

        waitForApp(TIME_TO_WAIT/2);

    }

    @Test
    public void gotPathRemoveItem(){

        onView(withId(R.id.switch_isLeader)).perform(click());
        goToPathList();

        if ( checkIfListOfValuesIsGraterThan(1)){
            onView(withId(R.id.paths_recycler_view))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(0, swipeRight()));
            waitForApp(TIME_TO_WAIT/2);
            onView(withId(R.id.paths_recycler_view))
                    .check(matches(MyUtils.atPosition(0, hasDescendant(withText("Rabe")))));
        }

        waitForApp(TIME_TO_WAIT/2);

    }

    private boolean checkIfListOfValuesIsGraterThan(int i) {
        PathListActivity currentActivity = (PathListActivity) getCurrentActivity();
        int sizeOfList = 0;
        int number = 0;
        if (currentActivity != null) {
            sizeOfList = currentActivity.getListSize();
            RecyclerView  rv = currentActivity.findViewById(R.id.paths_recycler_view);
            number = rv.getAdapter().getItemCount();
        }
        if ( number > i){
            return true;
        }
        else {
            return false;
        }

        //Log.v("OUTPUT", "Size: " + String.valueOf(sizeOfList) + " number: " + String.valueOf(number));
    }

    public Activity getCurrentActivity() {
        final Activity[] currentActivity = new Activity[1];
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                Collection<Activity> allActivities = ActivityLifecycleMonitorRegistry.getInstance()
                        .getActivitiesInStage(Stage.RESUMED);
                if (!allActivities.isEmpty()) {
                    currentActivity[0] = allActivities.iterator().next();
                }
            }
        });
        return currentActivity[0];
    }

    private void goToPathList(){
        onView(withId(R.id.btn_got_paths_mngmt)).perform(click());

        intended(hasComponent(GotPathsMngmtActivity.class.getName()));

        waitForApp(TIME_TO_WAIT);

        //onView(withId(R.id.ranges_recycler_view)).check(matches(hasRangesDataForPosition(1, "Beskidy wschodnie")));

        onView(withId(R.id.ranges_recycler_view))
                .check(matches(MyUtils.atPosition(0, hasDescendant(withText("Beskidy wschodnie")))));

        onView(withId(R.id.ranges_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        waitForApp(TIME_TO_WAIT);

        onView(withId(R.id.subranges_recycler_view))
                .check(matches(MyUtils.atPosition(0, hasDescendant(withText("Bieszczady")))));

        onView(withId(R.id.subranges_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        waitForApp(TIME_TO_WAIT);

    }

    private void fillAddTest(){

        String upPoints = "24";
        String downPoints = "21";
        String startSelection = "Bystry";
        String endSelection = "Mików";

        onView(withId(R.id.spinner_add_path_start_point))
                .perform(click());

        onData(allOf(is(instanceOf(String.class)), is(startSelection)))
                .perform(click());

        onView(withId(R.id.spinner_add_path_start_point))
                .check(matches(withSpinnerText(containsString(startSelection))));

        onView(withId(R.id.spinner_add_path_end_point))
                .perform(click());

        onData(allOf(is(instanceOf(String.class)), is(endSelection)))
                .perform(click());

        onView(withId(R.id.spinner_add_path_end_point))
                .check(matches(withSpinnerText(containsString(endSelection))));

        waitForApp(500);

        onView(withId(R.id.editText_add_path_points_UP))
                .perform(typeText(upPoints));


        onView(withId(R.id.editText_add_path_points_DN))
                .perform(typeText(downPoints));

        closeSoftKeyboard();

        waitForApp(500);
    }

    private void clickOnEditButton(int position){
        onView(withId(R.id.paths_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(position, new ClickOnEditButton()));
    }

    private void waitForApp(int miliS){
        try {
            Thread.sleep(miliS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
