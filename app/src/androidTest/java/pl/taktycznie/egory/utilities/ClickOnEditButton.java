package pl.taktycznie.egory.utilities;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;

import static androidx.test.espresso.action.ViewActions.click;
import android.view.View;

import org.hamcrest.Matcher;

import pl.taktycznie.egory.R;

public class ClickOnEditButton implements ViewAction {
    ViewAction click = click();


    @Override
    public Matcher<View> getConstraints() {
        return click.getConstraints();
    }

    @Override
    public String getDescription() {
        return "Click on custom edit view";
    }

    @Override
    public void perform(UiController uiController, View view) {
        click.perform(uiController, view.findViewById(R.id.button_modify));

    }
}
