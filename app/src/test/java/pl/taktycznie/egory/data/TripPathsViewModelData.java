package pl.taktycznie.egory.data;

import pl.taktycznie.egory.model.TripPath;

public class TripPathsViewModelData {

    private static final TripPath tp1 = new TripPath(1,
            99,
            "StartPoint1",
            "StartPointSubrange1",
            "EndPoint1",
            "EndPointSubrange1",
            11,
            1,
            1,
            0,
            1,
            false);

    private static final TripPath tp2 = new TripPath(2,
            99,
            "StartPoint2",
            "StartPointSubrange2",
            "EndPoint2",
            "EndPointSubrange2",
            12,
            2,
            1,
            0,
            2,
            false);

    private static final TripPath tp3 = new TripPath(3,
            99,
            "StartPoint3",
            "StartPointSubrange3",
            "EndPoint3",
            "EndPointSubrange3",
            13,
            3,
            1,
            0,
            3,
            false);

    private static final TripPath tp4 = new TripPath(4,
            99,
            "StartPoint4",
            "StartPointSubrange4",
            "EndPoint4",
            "EndPointSubrange4",
            14,
            4,
            1,
            0,
            4,
            false);

    private static final TripPath tp5 = new TripPath(5,
                    99,
                    "StartPoint5",
                    "StartPointSubrange5",
                    "EndPoint5",
                    "EndPointSubrange5",
                    15,
                    5,
                    1,
                    0,
                    5,
                    false);

    public static final TripPath[] TRIP_PATHS_ORDERED = {
            tp1, tp2, tp3, tp4, tp5
    };

    public static final TripPath[] TRIP_PATHS_REVERSED_ORDERED = {
            tp5, tp4, tp3, tp2, tp1
    };

    public static final TripPath[] TRIP_PATHS_LAST_ELEMS_WRONG_ORDER = {
            tp1, tp2, tp3, tp5, tp4
    };

    public static final TripPath[] TRIP_PATHS_FIRST_ELEMS_WRONG_ORDER = {
            tp2, tp1, tp3, tp4, tp5
    };

    public static final TripPath[] TRIP_PATHS_UNORDERED = {
            tp3, tp5, tp1, tp4, tp2
    };
}
