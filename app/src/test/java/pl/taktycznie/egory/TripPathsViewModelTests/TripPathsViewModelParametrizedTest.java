package pl.taktycznie.egory.TripPathsViewModelTests;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.viewmodel.TripPathsViewModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static pl.taktycznie.egory.data.TripPathsViewModelData.*;

@RunWith(Parameterized.class)
public class TripPathsViewModelParametrizedTest {

    TripPathsViewModel viewModel;
    MutableLiveData<List<TripPath>> tripPathsFromRepo;

    @Mock
    TripPathsRepository tripPathsRepository;

    @Mock
    LifecycleOwner lifecycleOwner;

    @Rule
    public InstantTaskExecutorRule executionRule = new InstantTaskExecutorRule();

    LifecycleRegistry lifecycle;

    @Parameterized.Parameters
    public static Iterable<TripPath[][]> parameters() {
        return Arrays.asList(
                new TripPath[][][] {
                    {TRIP_PATHS_ORDERED, TRIP_PATHS_ORDERED},
                    {TRIP_PATHS_ORDERED, TRIP_PATHS_REVERSED_ORDERED},
                    {TRIP_PATHS_ORDERED, TRIP_PATHS_LAST_ELEMS_WRONG_ORDER},
                    {TRIP_PATHS_ORDERED, TRIP_PATHS_FIRST_ELEMS_WRONG_ORDER},
                    {TRIP_PATHS_ORDERED, TRIP_PATHS_UNORDERED}
                });
    }

    @Parameterized.Parameter(0)
    public TripPath[] expected;

    @Parameterized.Parameter(1)
    public TripPath[] inputParam;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        tripPathsFromRepo = new MutableLiveData<>();
        viewModel = new TripPathsViewModel(tripPathsRepository);
        lifecycle = new LifecycleRegistry(Mockito.mock(LifecycleOwner.class));
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
        Mockito.when(lifecycleOwner.getLifecycle()).thenReturn(lifecycle);
        Mockito.when(tripPathsRepository.getTripPaths(Mockito.anyInt())).thenReturn(tripPathsFromRepo);
        viewModel.init(1);
        viewModel.getTripPaths().observe(lifecycleOwner, tripPaths -> {});
    }

    @Test
    public void getTripPathsGivesDataOrderedByOrderingNumber() {
        tripPathsFromRepo.setValue(Arrays.asList(inputParam));
        LiveData<List<TripPath>> tripPathsLd = viewModel.getTripPaths();
        List<TripPath> tripPathList = tripPathsLd.getValue();
        assertNotNull(tripPathList);
        assertEquals(expected.length, tripPathList.size());
        int iterator = 0;
        for(TripPath tripPath: tripPathList) {
            assertEquals(expected[iterator], tripPath);
            iterator++;
        }
    }
}
