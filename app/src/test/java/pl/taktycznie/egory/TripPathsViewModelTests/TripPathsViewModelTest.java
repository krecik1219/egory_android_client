package pl.taktycznie.egory.TripPathsViewModelTests;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.helpers.TripPathParamsHolder;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.model.MountainPoint;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.viewmodel.TripPathsViewModel;

import static org.junit.Assert.*;
import static pl.taktycznie.egory.data.TripPathsViewModelData.*;

@RunWith(MockitoJUnitRunner.class)
public class TripPathsViewModelTest {

    TripPathsViewModel viewModel;
    MutableLiveData<List<TripPath>> tripPathsFromRepo;
    MutableLiveData<DataAccessError> errorFromRepo;

    @Mock
    TripPathsRepository tripPathsRepository;

    @Mock
    LifecycleOwner lifecycleOwner;

    @Rule
    public InstantTaskExecutorRule executionRule = new InstantTaskExecutorRule();

    LifecycleRegistry lifecycle;

    public void addSingleValueToList(TripPath tripPath) {
        List<TripPath> tripPathList = tripPathsFromRepo.getValue();
        if(tripPathList == null)
            return;
        tripPathList.add(tripPath);
        tripPathsFromRepo.setValue(tripPathList);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        tripPathsFromRepo = new MutableLiveData<>();
        errorFromRepo = new MutableLiveData<>();
        viewModel = new TripPathsViewModel(tripPathsRepository);
        lifecycle = new LifecycleRegistry(Mockito.mock(LifecycleOwner.class));
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
        Mockito.when(lifecycleOwner.getLifecycle()).thenReturn(lifecycle);
        Mockito.when(tripPathsRepository.getTripPaths(Mockito.anyInt())).thenReturn(tripPathsFromRepo);
        Mockito.when(tripPathsRepository.getError()).thenReturn(errorFromRepo);
        viewModel.init(1);
        viewModel.getTripPaths().observe(lifecycleOwner, tripPaths -> {});
    }

    @Test
    public void initTripPathsCallsRepositoryGetTripPaths1Time() {
        tripPathsRepository = Mockito.mock(TripPathsRepository.class);
        Mockito.when(tripPathsRepository.getTripPaths(Mockito.anyInt())).thenReturn(tripPathsFromRepo);
        viewModel = new TripPathsViewModel(tripPathsRepository);
        viewModel.init(1);
        Mockito.verify(tripPathsRepository, Mockito.times(
                1))
                .getTripPaths(1);
    }

    @Test
    public void getTripPathsGivesEmptyListDataWhenRepoDoesNotContainData() {
        tripPathsFromRepo.setValue(new ArrayList<>());
        LiveData<List<TripPath>> tripPathsLd = viewModel.getTripPaths();
        assertNotNull(tripPathsLd.getValue());
        assertEquals(0, tripPathsLd.getValue().size());
    }

    @Test
    public void getTripPathsGivesProperFilledWithDataList() {
        tripPathsFromRepo.setValue(Arrays.asList(TRIP_PATHS_ORDERED));
        LiveData<List<TripPath>> tripPathsLd = viewModel.getTripPaths();
        List<TripPath> tripPathList = tripPathsLd.getValue();
        assertNotNull(tripPathList);
        assertEquals(TRIP_PATHS_ORDERED.length, tripPathList.size());
        int iterator = 0;
        for(TripPath tripPath: tripPathList) {
            assertEquals(TRIP_PATHS_ORDERED[iterator], tripPath);
            iterator++;
        }
    }

    @Test
    public void initTripPathsCallsRepositoryGetError1Time() {
        tripPathsRepository = Mockito.mock(TripPathsRepository.class);
        Mockito.when(tripPathsRepository.getTripPaths(Mockito.anyInt())).thenReturn(tripPathsFromRepo);
        viewModel = new TripPathsViewModel(tripPathsRepository);
        viewModel.init(1);
        Mockito.verify(tripPathsRepository, Mockito.times(
                1))
                .getError();
    }

    @Test
    public void getErrorReturnsRepoError() {
        assertEquals(errorFromRepo, viewModel.getError());
    }

    @Test
    public void addTripPathCallsRepositoryAddTripPath1Time() {
        GotPath gotPath = Mockito.mock(GotPath.class, Mockito.RETURNS_DEEP_STUBS);
        TripPathParamsHolder tripPathParamsHolder = new TripPathParamsHolder(gotPath, 1);
        viewModel.addTripPath(tripPathParamsHolder);
        Mockito.verify(tripPathsRepository, Mockito.times(
                1))
                .addTripPath(Mockito.any(TripPath.class));
    }

    @Test
    public void removeTripPathCallsRepositoryRemoveTripPath1Time() {
        TripPath tripPath = Mockito.mock(TripPath.class);
        viewModel.removeTripPath(tripPath);
        Mockito.verify(tripPathsRepository, Mockito.times(
                1))
                .removeTripPath(tripPath);
    }

    @Test
    public void switchCheckDoneCallsRepositorySwitchCheckDone1Time() {
        TripPath tripPath = Mockito.mock(TripPath.class);
        viewModel.switchCheckDone(tripPath);
        Mockito.verify(tripPathsRepository, Mockito.times(
                1))
                .switchCheckDone(tripPath);
    }

    @Test
    public void addTripPathWhenDataIsEmptyResultsWithDataFilledWithOneElementWithOrderingNumOne() {
        tripPathsFromRepo.setValue(new ArrayList<>());
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            TripPath arg = (TripPath) args[0];
            addSingleValueToList(arg);
            return null;
        }).when(tripPathsRepository).addTripPath(Mockito.any(TripPath.class));
        GotPath gotPath = new GotPath(
                777,
                new MountainPoint(
                        11,
                        "StartPoint11",
                        11,
                        "StartPointSubrange11",
                        11,
                        "StartPointRange11", 0, 0, 0),
                new MountainPoint(
                        11,
                        "EndPoint11",
                        11,
                        "EndPointSubrange11",
                        11,
                        "EndPointRange11", 0, 0, 0),
                20,
                17);
        TripPathParamsHolder holder = new TripPathParamsHolder(gotPath, 1);
        viewModel.addTripPath(holder);
        LiveData<List<TripPath>> tripPathsLd = viewModel.getTripPaths();
        List<TripPath> tripPathList = tripPathsLd.getValue();
        assertNotNull(tripPathList);
        assertEquals(1, tripPathList.size());
        TripPath expected = new TripPath(
                0,
                777,
                "StartPoint11",
                "StartPointSubrange11",
                "EndPoint11",
                "EndPointSubrange11",
                20,
                17,
                1,
                1,
                1,
                false);
        assertEquals(expected, tripPathList.get(0));
    }

    @Test
    public void addNewTripPathResultsInDataWithNewElementWhichHasOrderingNumOneMoreThanPreviousMax() {
        List<TripPath> tripPathsForRepo = new ArrayList<>(Arrays.asList(TRIP_PATHS_ORDERED));
        final int maxPathOrderNum = 99;
        TripPath tripPath = new TripPath(6,
                99,
                "StartPoint6",
                "StartPointSubrange6",
                "EndPoint6",
                "EndPointSubrange6",
                16,
                6,
                1,
                0,
                maxPathOrderNum,
                false);
        tripPathsForRepo.add(tripPath);
        tripPathsFromRepo.setValue(tripPathsForRepo);
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            TripPath arg = (TripPath) args[0];
            addSingleValueToList(arg);
            return null;
        }).when(tripPathsRepository).addTripPath(Mockito.any(TripPath.class));
        GotPath gotPath = new GotPath(
                777,
                new MountainPoint(
                        11,
                        "StartPoint11",
                        11,
                        "StartPointSubrange11",
                        11,
                        "StartPointRange11", 0, 0, 0),
                new MountainPoint(
                        11,
                        "EndPoint11",
                        11,
                        "EndPointSubrange11",
                        11,
                        "EndPointRange11", 0, 0, 0),
                20,
                17);
        TripPathParamsHolder holder = new TripPathParamsHolder(gotPath, 1);
        viewModel.addTripPath(holder);
        LiveData<List<TripPath>> tripPathsLd = viewModel.getTripPaths();
        List<TripPath> tripPathList = tripPathsLd.getValue();
        assertNotNull(tripPathList);
        assertEquals(TRIP_PATHS_ORDERED.length + 2, tripPathList.size());
        TripPath expectedNewTripPath = new TripPath(
                0,
                777,
                "StartPoint11",
                "StartPointSubrange11",
                "EndPoint11",
                "EndPointSubrange11",
                20,
                17,
                1,
                1,
                maxPathOrderNum + 1,
                false);
        for(int i = 0; i < tripPathList.size() - 2; i++) {
            assertEquals(TRIP_PATHS_ORDERED[i], tripPathList.get(i));
        }
        assertEquals(tripPath, tripPathList.get(tripPathList.size() - 2));
        assertEquals(expectedNewTripPath, tripPathList.get(tripPathList.size() - 1));
    }

    @Test
    public void addNewTripPathResultsInDataWithNewElementWhichIsProperlyOrdered() {
        tripPathsFromRepo.setValue(new ArrayList<>(Arrays.asList(TRIP_PATHS_ORDERED)));
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            TripPath arg = (TripPath) args[0];
            addSingleValueToList(arg);
            return null;
        }).when(tripPathsRepository).addTripPath(Mockito.any(TripPath.class));
        GotPath gotPath = new GotPath(
                777,
                new MountainPoint(
                        11,
                        "StartPoint11",
                        11,
                        "StartPointSubrange11",
                        11,
                        "StartPointRange11", 0, 0, 0),
                new MountainPoint(
                        11,
                        "EndPoint11",
                        11,
                        "EndPointSubrange11",
                        11,
                        "EndPointRange11", 0, 0, 0),
                20,
                17);
        TripPathParamsHolder holder = new TripPathParamsHolder(gotPath, 1);
        viewModel.addTripPath(holder);
        LiveData<List<TripPath>> tripPathsLd = viewModel.getTripPaths();
        List<TripPath> tripPathList = tripPathsLd.getValue();
        assertNotNull(tripPathList);
        assertEquals(TRIP_PATHS_ORDERED.length + 1, tripPathList.size());
        TripPath expectedNewTripPath = new TripPath(
                0,
                777,
                "StartPoint11",
                "StartPointSubrange11",
                "EndPoint11",
                "EndPointSubrange11",
                20,
                17,
                1,
                1,
                TRIP_PATHS_ORDERED[TRIP_PATHS_ORDERED.length - 1].getPathOrderNumber() + 1,
                false);
        for(int i = 0; i < tripPathList.size() - 1; i++) {
            assertEquals(TRIP_PATHS_ORDERED[i], tripPathList.get(i));
        }
        assertEquals(expectedNewTripPath, tripPathList.get(tripPathList.size() - 1));
    }

}
