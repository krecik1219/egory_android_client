package pl.taktycznie.egory.CoreFunctionTests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import pl.taktycznie.egory.data.TripPathsViewModelData;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.model.TripPath;

@RunWith(JUnitParamsRunner.class)
public class TripUnitTests {

    private int PATH_POINTS_SCORE = 65; //80
    private static final TripPathsViewModelData tripPathsData = new TripPathsViewModelData();

    @Test
    @Parameters( {
            "1", "2", "3"
    })
    public void CountingPointsTest(int num){

        TripPath[] paths;
        switch (num){
            case 1:
                paths = tripPathsData.TRIP_PATHS_ORDERED;
                break;
            case 2:
                paths = tripPathsData.TRIP_PATHS_REVERSED_ORDERED;
                break;
            default:
                paths = tripPathsData.TRIP_PATHS_UNORDERED;
        }

        Trip trip = new Trip("TestTrip");

        for(TripPath tp : paths){
            if( !tp.isDone()){
                tp.switchDone();
            }
            trip.addTripPath(tp);
        }

        int points = trip.calculateTripPoints();
        System.out.println(points);
        Assert.assertEquals(PATH_POINTS_SCORE, points);

    }

}
