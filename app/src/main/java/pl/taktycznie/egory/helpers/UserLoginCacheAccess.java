package pl.taktycznie.egory.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import pl.taktycznie.egory.model.user.User;

public class UserLoginCacheAccess {

    private static final String LOGIN_CACHE_SHARED_PREFERENCES = "LOGIN_CACHE_SHARED_PREFERENCES";

    private Context context;

    public UserLoginCacheAccess(Context context) {
        this.context = context;
    }

    public void storeLoginInfo(User user) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                LOGIN_CACHE_SHARED_PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putInt("userId", user.getUserId());
        editor.putString("login", user.getLogin());
        editor.putString("name", user.getName());
        editor.putString("surname", user.getSurname());
        editor.apply();
    }

    public User retrieveLoginInfo() {
        SharedPreferences prefs = context.getSharedPreferences(
                LOGIN_CACHE_SHARED_PREFERENCES, Context.MODE_PRIVATE);

        if(prefs == null)
            return null;

        int userId = prefs.getInt("userId", 0);
        if(userId == 0)
            return null;
        String login = prefs.getString("login", "");
        if("".equals(login))
            return null;
        String name = prefs.getString("name", "");
        if("".equals(name))
            return null;
        String surname = prefs.getString("surname", "");
        if("".equals(surname))
            return null;
        return new User(userId, login, name, surname);
    }
}
