package pl.taktycznie.egory.helpers.validators;

public class TripDataValidator {

    private static final int TRIP_NAME_MAX_LEN = 255;

    public boolean validateTripName(final String name) {
        return name != null && name.length() > 0;
    }

}
