package pl.taktycznie.egory.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class BoolPaired<Item> {
    private final Item item;
    private boolean flag;

    public static <Item> List<BoolPaired<Item>> makeBoolPaierdCollection(Collection<Item> collection) {
        List<BoolPaired<Item>> boolPairedCollection = new ArrayList<>();
        for(Item i : collection)
            boolPairedCollection.add(new BoolPaired<>(i));
        return  boolPairedCollection;
    }

    public BoolPaired(Item item, boolean flag) {
        this.item = item;
        this.flag = flag;
    }

    public BoolPaired(Item item) {
        this.item = item;
        this.flag = false;
    }

    public Item getItem() {
        return item;
    }

    public boolean flag() {
        return flag;
    }

    public void setFlag() {
        flag = true;
    }

    public void unsetFlag() {
        flag = false;
    }

    public void setFlagTo(boolean flag) {
        this.flag = flag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoolPaired<?> that = (BoolPaired<?>) o;
        return Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item);
    }

    @Override
    public String toString() {
        return "BoolPaired{" +
                "item=" + item +
                ", flag=" + flag +
                '}';
    }
}
