package pl.taktycznie.egory.helpers;

import pl.taktycznie.egory.model.GotPath;

public class TripPathParamsHolder {
    public final GotPath gotPath;
    public final int tripId;

    public TripPathParamsHolder(final GotPath gotPath, final int tripId) {
        this.gotPath = gotPath;
        this.tripId = tripId;
    }
}
