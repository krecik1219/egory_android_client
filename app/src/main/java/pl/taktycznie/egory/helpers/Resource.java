package pl.taktycznie.egory.helpers;

import android.util.Log;

import pl.taktycznie.egory.dao.DataAccessError;

public class Resource <Data> {
    private final Data data;
    private final DataAccessError error;

    public Resource(Data data, DataAccessError error) {
        this.data = data;
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public DataAccessError getError() {
        return error;
    }

    public boolean isSuccessful() {
        Log.d("Reource isSuccessful", "is successful entered");
        return data != null && error == null;
    }


}
