package pl.taktycznie.egory.globals;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.model.GotBook;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.model.user.User;
import pl.taktycznie.egory.repository.persistance.UserDataDb;

public class UserDataSingleton {

    private static UserDataSingleton instance = null;

    private final GotBook gotBook = new GotBook();
    private boolean isAdmin = false;
    private List<GotPath> _gotPaths;

    private UserDataSingleton() {
        _gotPaths = new ArrayList<>();
    }

    /**
     * User data singleton should never be used, now proper way is to access UserDb.
     * @return UserDataSingleton
     */
    @Deprecated
    public static UserDataSingleton getInstance() {
        if(instance == null)
            instance = new UserDataSingleton();
        return instance;
    }

    public int getUserId() {
        User user = UserDataDb.getInstance().getDataValue();
        return user.getUserId();
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setLeadership(){
        isAdmin = true;
    }

    public void unSetLeadership(){
        isAdmin = false;
    }

    public GotBook getGotBook() {
        return gotBook;
    }

    public void setGotPathsList(List<GotPath> paths){
        _gotPaths = paths;
    }

    public void clearGotPathsList(List<GotPath> paths){
        _gotPaths.clear();
    }

}
