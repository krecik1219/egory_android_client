package pl.taktycznie.egory.model.tracking;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

import pl.taktycznie.egory.utils.DateUtils;


public class LocationSession {

    private int locationSessionId;
    private String sessionName;
    private Date startDatetime;
    private Date endDatetime;
    private List<LocationPoint> locationPoints;
    private Queue<PhotoTag> awaitingTagsForLastPoint;

    private Queue<LocationPoint> syncAwaiting = new LinkedList<>();

    private boolean isIdSet;

    public LocationSession(String sessionName, Date startDatetime) {
        this.sessionName = sessionName;
        this.startDatetime = startDatetime;
        this.locationPoints = new ArrayList<>();
        this.isIdSet = false;
        this.awaitingTagsForLastPoint = new ArrayDeque<>();
    }

    public LocationSession(int locationSessionId, String sessionName, Date startDatetime, Date endDatetime, List<LocationPoint> locationPoints) {
        this.locationSessionId = locationSessionId;
        this.sessionName = sessionName;
        this.startDatetime = startDatetime;
        this.endDatetime = endDatetime;
        this.locationPoints = locationPoints;
        this.isIdSet = true;
        this.awaitingTagsForLastPoint = new ArrayDeque<>();
    }

    public LocationSession() {
        this.locationPoints = new ArrayList<>();
        this.sessionName = "";
        this.startDatetime = DateUtils.getCurrentDatetime();
        this.isIdSet = false;
        this.awaitingTagsForLastPoint = new ArrayDeque<>();
    }

    // copy ctor
    public LocationSession(LocationSession other) {
        this.locationSessionId = other.locationSessionId;
        this.sessionName = other.sessionName;
        if(other.startDatetime != null)
            this.startDatetime = (Date) other.startDatetime.clone();
        if(other.endDatetime != null)
            this.endDatetime = (Date) other.endDatetime.clone();
        this.locationPoints = new ArrayList<>();
        for(LocationPoint point: other.locationPoints) {
            this.locationPoints.add(new LocationPoint(point));
        }
        this.syncAwaiting = new LinkedList<>();
        for(LocationPoint point: other.syncAwaiting) {
            this.syncAwaiting.add(new LocationPoint(point));
        }
        this.awaitingTagsForLastPoint = new ArrayDeque<>();
        for(PhotoTag tag : other.awaitingTagsForLastPoint)
            this.awaitingTagsForLastPoint.add(new PhotoTag(tag));
        this.isIdSet = other.isIdSet;
    }

    public int getLocationSessionId() {
        return locationSessionId;
    }

    public void setLocationSessionId(int locationSessionId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, location session id override attempt");
        this.locationSessionId = locationSessionId;
        this.isIdSet = true;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public void setLocationPoints(List<LocationPoint> locationPoints) {
        this.locationPoints = locationPoints;
    }

    public void addLocationPoint(LocationPoint location) {
        if(!awaitingTagsForLastPoint.isEmpty()) {
            location.addPhotoTags(awaitingTagsForLastPoint);
            awaitingTagsForLastPoint.clear();
        }
        locationPoints.add(location);
    }

    public int getLastIndex() {
        return locationPoints.size() - 1;
    }

    public LocationPoint getLastPoint() {
        if(locationPoints.isEmpty())
            return null;
        return locationPoints.get(locationPoints.size() - 1);
    }

    public List<LocationPoint> getLocationPoints() {
        return locationPoints;
    }

    public void pushLocationToSyncAwaiting(LocationPoint location) {
        syncAwaiting.offer(location);
    }

    public Queue<LocationPoint> getSyncAwaiting() {
        return syncAwaiting;
    }

    public void clearSyncAwaiting() {
        syncAwaiting.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationSession that = (LocationSession) o;
        if(locationSessionId != 0 && that.locationSessionId != 0 && locationSessionId != that.locationSessionId)
            return false;
        return Objects.equals(startDatetime, that.startDatetime) &&
                Objects.equals(locationPoints, that.locationPoints);
    }

    public boolean equalsWithoutLastPoint(LocationSession that) {
        if (this == that) return true;
        if (that == null || getClass() != that.getClass()) return false;
        if(locationSessionId != 0 && that.locationSessionId != 0 && locationSessionId != that.locationSessionId)
            return false;
        if(!Objects.equals(startDatetime, that.startDatetime))
            return false;
        for(int i = 0; i < locationPoints.size() - 1; i++) {
            if(!locationPoints.get(i).equals(that.locationPoints.get(i)))
                return false;
        }
        return true;
    }

    public boolean setLastPointPhotoTag(PhotoTag photoTag) {
        if(locationPoints.isEmpty()) {
            awaitingTagsForLastPoint.offer(photoTag);
            return false;
        }
        else {
            locationPoints.get(locationPoints.size() - 1).addPhotoTag(photoTag);
            return true;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDatetime, locationPoints);
    }

    @Override
    public String toString() {
        return "LocationSession{" +
                "locationSessionId=" + locationSessionId +
                ", sessionName='" + sessionName + '\'' +
                ", startDatetime=" + startDatetime +
                ", endDatetime=" + endDatetime +
                ", locationPoints=" + locationPoints +
                ", syncAwaiting=" + syncAwaiting +
                '}';
    }
}
