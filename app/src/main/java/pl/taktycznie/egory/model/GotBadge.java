package pl.taktycznie.egory.model;

import java.util.Date;
import java.util.GregorianCalendar;

import pl.taktycznie.egory.utils.DateUtils;

public class GotBadge {

    private final int badgeId;
    private final String badgeName;
    private final int requiredPoints;
    private String awardDateString;
    private Date awardDate;

    public GotBadge(int badgeId, String badgeName, int requiredPoints, String awardDateString) {
        this.badgeId = badgeId;
        this.badgeName = badgeName;
        this.requiredPoints = requiredPoints;
        this.awardDateString = awardDateString;
        this.awardDate = DateUtils.getDateFromString(this.awardDateString);
    }

    public GotBadge(int badgeId, String badgeName, int requiredPoints, Date awardDate) {
        this.badgeId = badgeId;
        this.badgeName = badgeName;
        this.requiredPoints = requiredPoints;
        this.awardDate = awardDate;
        this.awardDateString = DateUtils.dateToString(this.awardDate);
    }

    public GotBadge(int badgeId, String badgeName, int requiredPoints) {
        this.badgeId = badgeId;
        this.badgeName = badgeName;
        this.requiredPoints = requiredPoints;
        awardDateString = "";
        awardDate = new GregorianCalendar(2019, 01, 01).getTime();
    }

    public int getBadgeId() {
        return badgeId;
    }

    public String getBadgeName() {
        return badgeName;
    }

    public int getRequiredPoints() {
        return requiredPoints;
    }

    public String getAwardDateString() {
        return awardDateString;
    }

    public Date getAwardDate() {
        return awardDate;
    }

    public void setAwardDate(String awardDateString) {
        this.awardDateString = awardDateString;
        this.awardDate = DateUtils.getDateFromString(this.awardDateString);
    }

    public void setAwardDate(Date awardDate) {
        this.awardDate = awardDate;
        this.awardDateString = DateUtils.dateToString(this.awardDate);
    }

    @Override
    public String toString() {
        return "GotBadge{" +
                "badgeId=" + badgeId +
                ", badgeName='" + badgeName + '\'' +
                ", requiredPoints=" + requiredPoints +
                ", awardDateString='" + awardDateString +
                '}';
    }
}
