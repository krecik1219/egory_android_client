package pl.taktycznie.egory.model;

public enum PathStatus {
    UNCONFIRMED,
    CONFIRMED;

    public static PathStatus pathStatusFromInt(int pathStatusId) {
        switch(pathStatusId) {
            case 1:
                return UNCONFIRMED;
            case 2:
            default:
                return CONFIRMED;
        }
    }
}
