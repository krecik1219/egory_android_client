package pl.taktycznie.egory.model.tracking;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.services.LocationProvidingService;

public class Tracker {

    public enum TrackerState {
        ACTIVE,
        INACTIVE
    }

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + Tracker.class.getSimpleName();

    private Context context;

    public Tracker(Context context) {
        this.context = context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public TrackerState toggleTracking(LocationMaintainer locationMaintainer) {
        if(!isTrackingServiceRunning()) {
            locationMaintainer.startNewLocationSession();
            startTrackingService();
            return TrackerState.ACTIVE;
        }
        else {
            stopTrackingService();
            return TrackerState.INACTIVE;
        }
    }

    public TrackerState getTrackerState() {
        if(isTrackingServiceRunning())
            return TrackerState.ACTIVE;
        else
            return TrackerState.INACTIVE;
    }

    public boolean isTrackingServiceRunning() {
        Log.d(TAG, "isTrackingServiceRunning()");
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if(manager == null)
            return false;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (LocationProvidingService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void startTrackingService() {
        Log.d(TAG, "startTrackingService()");
        Intent i = new Intent(context, LocationProvidingService.class);
        context.startService(i);
    }

    private void stopTrackingService() {
        Log.d(TAG, "stopTrackingService()");
        Intent i = new Intent(context, LocationProvidingService.class);
        context.stopService(i);
    }
}
