package pl.taktycznie.egory.model;

import android.annotation.SuppressLint;

import java.util.HashMap;
import java.util.Map;

public class GotBook {

    private final Map<Integer, Trip> trips;
    private final Map<Integer, GotBadge> gotBadges;

    @SuppressLint("UseSparseArrays")
    public GotBook() {
        this.trips = new HashMap<>();
        this.gotBadges = new HashMap<>();
    }

    public Map<Integer, Trip> getTrips() {
        return trips;
    }

    public Map<Integer, GotBadge> getGotBadges() {
        return gotBadges;
    }

    public void addTrip(Trip trip) {
        trips.put(trip.getTripId(), trip);
    }

    public void addGotBadge(GotBadge badge) {
        gotBadges.put(badge.getBadgeId(), badge);
    }
}
