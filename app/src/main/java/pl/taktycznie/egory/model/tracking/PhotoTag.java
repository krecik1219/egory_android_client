package pl.taktycznie.egory.model.tracking;

import android.os.Environment;

public class PhotoTag {

    public static final String PHOTO_TAGS_DIR = Environment.getExternalStorageDirectory().getPath() + "/0/dev/PhotoTags";

    private int photoTagId;
    private String imageFilePath;

    private boolean isIdSet;

    public PhotoTag(int photoTagId, String imageFilePath) {
        this.photoTagId = photoTagId;
        this.imageFilePath = imageFilePath;
        this.isIdSet = true;
    }

    public PhotoTag(String imageFilePath) {
        this.imageFilePath = imageFilePath;
        this.isIdSet = false;
    }

    public PhotoTag(PhotoTag other) {
        this.photoTagId = other.photoTagId;
        this.imageFilePath = other.imageFilePath;
        this.isIdSet = other.isIdSet;
    }

    public int getPhotoTagId() {
        return photoTagId;
    }

    public void setPhotoTagId(int photoTagId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, photo tag id override attempt");
        this.photoTagId = photoTagId;
        this.isIdSet = true;
    }

    public String getImageFilePath() {
        return imageFilePath;
    }

    @Override
    public String toString() {
        return "PhotoTag{" +
                "photoTagId=" + photoTagId +
                ", imageFilePath='" + imageFilePath + '\'' +
                '}';
    }
}
