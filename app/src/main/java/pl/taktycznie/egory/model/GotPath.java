package pl.taktycznie.egory.model;



public class GotPath {

    /***
     * @param upPoints Points by vertical distance
     * @param downPoints Points by horizontal distance
     */

    private int pathId;
    private final MountainPoint startPoint;
    private final MountainPoint endPoint;
    private int upPoints;
    private int downPoints;

    private boolean isIdSet;

    public GotPath(MountainPoint startPoint, MountainPoint endPoint, int upPoints, int downPoints) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.upPoints = upPoints;
        this.downPoints = downPoints;
        this.isIdSet = false;
    }

    public GotPath(int pathId, MountainPoint startPoint, MountainPoint endPoint, int upPoints, int downPoints) {
        this(startPoint, endPoint, upPoints, downPoints);
        this.pathId = pathId;
        this.isIdSet = true;
    }

    public int getPathId() {
        return pathId;
    }

    public void setPathId(int pathId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, got path id override attempt");
        this.pathId = pathId;
        this.isIdSet = true;
    }

    public MountainPoint getStartPoint() {
        return startPoint;
    }

    public MountainPoint getEndPoint() {
        return endPoint;
    }

    public int getUpPoints() {
        return upPoints;
    }

    public void setUpPoints(int upPoints) {
        this.upPoints = upPoints;
    }

    public int getDownPoints() {
        return downPoints;
    }

    public void setDownPoints(int downPoints) {
        this.downPoints = downPoints;
    }

    public void  calculatePoints()
    {
        int points=0;
        points+=Math.abs(startPoint.getAltitude()-endPoint.getAltitude())/100;

        int distance=(int)distance(startPoint.getLatitude(),endPoint.getLatitude(),startPoint.getLongitude(),endPoint.getLongitude(),startPoint.getAltitude(),endPoint.getAltitude());
        points+=distance/1000;

        setDownPoints(points);
        setUpPoints(points);
    }

    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double al1, double al2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = al1 - al2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    @Override
    public String toString() {
        return "pathId: " + pathId +
                " ; startPoint: " + startPoint +
                " ; endPoint: " + endPoint +
                " ; upPoints: " + upPoints +
                " ; downPoints: " + downPoints;
    }
}
