package pl.taktycznie.egory.model;

import android.graphics.Bitmap;

public class PhotoConfirmation {
    private int photoConfirmationId;
    private final Bitmap image;
    private final int tripPathId;

    private boolean isIdSet;

    public PhotoConfirmation(Bitmap image, int tripPathId) {
        this.image = image;
        this.tripPathId = tripPathId;
        this.isIdSet = false;
    }

    public PhotoConfirmation(int photoConfirmationId, Bitmap image, int tripPathId) {
        this(image, tripPathId);
        this.photoConfirmationId = photoConfirmationId;
        this.isIdSet = true;
    }

    public int getPhotoConfirmationId() {
        return photoConfirmationId;
    }

    public void setPhotoConfirmationId(int photoConfirmationId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, photo confirmation id override attempt");
        this.photoConfirmationId = photoConfirmationId;
        this.isIdSet = true;
    }

    public Bitmap getImage() {
        return image;
    }

    public int getTripPathId() {
        return tripPathId;
    }

    @Override
    public String toString() {
        return "photoConfirmationId: " + photoConfirmationId + " ; bitmap address: " + image + " ; tripPathId: " + tripPathId;
    }
}
