package pl.taktycznie.egory.model;

public class MountainPoint {

    private final int pointId;
    private final String pointName;
    private final MountainSubrange subrange;
    private final double latitude;
    private final double longitude;
    private final double altitude;

    public MountainPoint(int pointId, String pointName, int subrangeId, String subrangeName, int rangeId, String rangeName,
                         double latitude, double longitude, double altitude) {
        this.pointId = pointId;
        this.pointName = pointName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.subrange = new MountainSubrange(subrangeId, subrangeName, rangeId, rangeName);
    }

    public int getPointId() {
        return pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public MountainSubrange getSubrange() {
        return subrange;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }
    public boolean equals (MountainPoint another)
    {
        if(this.altitude==another.getAltitude())
            if(this.latitude==another.getLatitude())
                if(this.longitude==another.getLongitude())
                    return true;


                return false;
    }


    @Override
    public String toString() {
        return "ID: " + pointId + " ; name: " + pointName + " ; latitude: " + latitude + " ; longitude: " + longitude + " ; altitude: " + altitude +
                " ; subrange ID: " + subrange.getSubrangeId() + " ; subrange name: " + subrange.getSubrangeName();
    }
}
