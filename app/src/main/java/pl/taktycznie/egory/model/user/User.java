package pl.taktycznie.egory.model.user;

import java.util.Objects;

public class User {

    private int userId;
    private boolean isAdmin = false;
    private String login;
    private String name;
    private String surname;

    public User() {
        this.userId = 0;
        this.isAdmin = false;
    }

    public User(int userId) {
        this.userId = userId;
        this.isAdmin = false;
    }

    public User(int userId, String login, String name, String surname) {
        this.userId = userId;
        this.isAdmin = false;
        this.login = login;
        this.name = name;
        this.surname = surname;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setLeadership(){
        isAdmin = true;
    }

    public void unSetLeadership(){
        isAdmin = false;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId &&
                login.equals(user.login) &&
                name.equals(user.name) &&
                surname.equals(user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, login, name, surname);
    }
}
