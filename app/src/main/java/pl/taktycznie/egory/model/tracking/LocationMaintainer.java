package pl.taktycznie.egory.model.tracking;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import pl.taktycznie.egory.globals.GlobalConstants;

public class LocationMaintainer {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationMaintainer.class.getSimpleName();

    private int currentSessionIndex;
    private List<LocationSession> locationSessions;
    private Queue<LocationSession> syncAwaitingQueue = new LinkedList<>();

    // Default constructor is necessary for LocationDb
    @SuppressLint("UseSparseArrays")
    public LocationMaintainer() {
        this.locationSessions = new ArrayList<>();
        this.currentSessionIndex = -1;
    }

    // copy ctor
    @SuppressLint("UseSparseArrays")
    public LocationMaintainer(LocationMaintainer other) {
        this.currentSessionIndex = other.currentSessionIndex;
        this.locationSessions = new ArrayList<>();
        for(LocationSession ls : other.locationSessions) {
            this.locationSessions.add(new LocationSession(ls));
        }
        for(LocationSession ls : other.syncAwaitingQueue) {
            this.syncAwaitingQueue.offer(new LocationSession(ls));
        }
    }

    public int handleNewLocationProvided(LocationPoint locationPoint) {
        locationSessions.get(currentSessionIndex).addLocationPoint(locationPoint);
        return locationSessions.get(currentSessionIndex).getLastIndex() + 1;
    }

    public int getNextOrderNum() {
        return locationSessions.get(currentSessionIndex).getLastIndex() + 2;
    }

    public List<LocationSession> getLocationSessions() {
        return locationSessions;
    }

    public LocationSession startNewLocationSession() {
        locationSessions.add(new LocationSession());
        currentSessionIndex = locationSessions.size() - 1;
        return locationSessions.get(currentSessionIndex);
    }

    public LocationSession getCurrentSession() {
        if(locationSessions.size() == 0)
            return null;
        return locationSessions.get(currentSessionIndex);
    }

    public LocationSession getLocationSession(int sessionIndex) {
        return locationSessions.get(sessionIndex);
    }

    public LocationSession getLocationSessionMatchingProvided(LocationSession providedSession) {
        Log.d(TAG, "getLocationSessionMatchingProvided");
        Log.d(TAG, "provided session: " + providedSession);
        Iterator<LocationSession> it = locationSessions.iterator();
        while(it.hasNext()) {
            LocationSession temp = it.next();
            if(temp.equalsWithoutLastPoint(providedSession)) {
                Log.d(TAG, "MATCHED: " + temp);
                return temp;
            }
            else {
                Log.d(TAG, "NOT MATCHED: " + temp);
            }
        }
        return null;
    }

    public void setLocationSessions(List<LocationSession> locationSessions) {
        this.locationSessions = locationSessions;
        this.currentSessionIndex = this.locationSessions.size() - 1;
    }

    public void syncLocationSessions(List<LocationSession> locationSessions) {
        this.locationSessions.clear();
        for(LocationSession ls: locationSessions) {
            boolean isAwaiting = false;
            for(LocationSession awaiting: syncAwaitingQueue) {
                if(ls.getLocationSessionId() == awaiting.getLocationSessionId()) {
                    isAwaiting = true;
                    break;
                }
            }
            if(!isAwaiting)
                this.locationSessions.add(ls);
        }
        this.locationSessions.addAll(syncAwaitingQueue);
        currentSessionIndex = this.locationSessions.size() - 1;
    }

    public void pushCurrentToSyncAwaiting() {
        if(!syncAwaitingQueue.contains(getCurrentSession()))
            syncAwaitingQueue.offer(getCurrentSession());
    }

    public void pushCurrentToSyncAwaiting(LocationPoint locationPoint) {
        if(!syncAwaitingQueue.contains(getCurrentSession()))
            syncAwaitingQueue.offer(getCurrentSession());
        getCurrentSession().pushLocationToSyncAwaiting(locationPoint);
    }

    public Queue<LocationSession> getSyncAwaitingQueue() {
        return syncAwaitingQueue;
    }

    public void clearSyncAwaiting() {
        for(LocationSession ls : syncAwaitingQueue)
            ls.clearSyncAwaiting();
        syncAwaitingQueue.clear();
    }

    public boolean setPhotoTagTLastSessionPoint(PhotoTag photoTag) {
        LocationSession currentSession = getCurrentSession();
        if(currentSession != null)
            return currentSession.setLastPointPhotoTag(photoTag);
        return false;
    }
}
