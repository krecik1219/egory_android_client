package pl.taktycznie.egory.model.user;

import pl.taktycznie.egory.helpers.UserLoginCacheAccess;
import pl.taktycznie.egory.repository.persistance.UserDataDb;

public class UserLoginManager {

    private UserLoginCacheAccess loginCache;

    public UserLoginManager(UserLoginCacheAccess loginCache) {
        this.loginCache = loginCache;
    }

    public void logUser(User user) {
        loginCache.storeLoginInfo(user);
        UserDataDb instance = UserDataDb.getInstance();
        instance.setDataValue(user);
    }

    public boolean isUserLogged() {
        User user = loginCache.retrieveLoginInfo();
        return user != null && user.getUserId() != 0;
    }

    public void logoutUser() {
        loginCache.storeLoginInfo(new User());
        UserDataDb instance = UserDataDb.getInstance();
        instance.setDataValue(new User());
    }

    public void logCachedUser() {
        logUser(loginCache.retrieveLoginInfo());
    }
}
