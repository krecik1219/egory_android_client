package pl.taktycznie.egory.model.tracking;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

public class LocationPoint {

    private int trackingPointId;
    private double latitude;
    private double longitude;
    private double altitude;
    private int orderNum;
    private List<PhotoTag> photoTags;

    private boolean isIdSet;

    public static LocationPoint getPointFromLocation(Location location, int orderNum) {
        return new LocationPoint(location.getLatitude(), location.getLongitude(), location.getAltitude(), orderNum);
    }

    public LocationPoint(int trackingPointId, double latitude, double longitude, double altitude, int orderNum) {
        this.trackingPointId = trackingPointId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.orderNum = orderNum;
        this.isIdSet = true;
        this.photoTags = new ArrayList<>();
    }

    public LocationPoint(double latitude, double longitude, double altitude, int orderNum) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.orderNum = orderNum;
        this.isIdSet = false;
        this.photoTags = new ArrayList<>();
    }

    public LocationPoint(LocationPoint other) {
        this.trackingPointId = other.trackingPointId;
        this.latitude = other.latitude;
        this.longitude = other.longitude;
        this.altitude = other.altitude;
        this.orderNum = other.orderNum;
        this.isIdSet = other.isIdSet;
        this.photoTags = new ArrayList<>();
        for(PhotoTag otherTag : other.photoTags)
            this.photoTags.add(new PhotoTag(otherTag));
    }

    public int getTrackingPointId() {
        return trackingPointId;
    }

    public void setTrackingPointId(int trackingPointId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, tracking point id override attempt");
        this.trackingPointId = trackingPointId;
        this.isIdSet = true;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationPoint that = (LocationPoint) o;
        if(trackingPointId != 0 && that.trackingPointId != 0 && trackingPointId != that.trackingPointId)
            return false;
        return Double.compare(that.latitude, latitude) == 0 &&
                Double.compare(that.longitude, longitude) == 0 &&
                Double.compare(that.altitude, altitude) == 0 &&
                orderNum == that.orderNum;
    }

    public void addPhotoTags(Queue<PhotoTag> awaitingTagsForLastPoint) {
        for(PhotoTag tag : awaitingTagsForLastPoint)
            addPhotoTag(tag);
    }


    public void addPhotoTag(PhotoTag photoTag) {
        photoTags.add(photoTag);
    }

    public List<PhotoTag> getPhotoTags() {
        return photoTags;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude, orderNum);
    }

    @Override
    public String toString() {
        return "LocationPoint{" +
                "trackingPointId=" + trackingPointId +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", altitude=" + altitude +
                ", orderNum=" + orderNum +
                ", photoTags=" + photoTags +
                '}';
    }
}
