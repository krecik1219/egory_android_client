package pl.taktycznie.egory.model;

public class MountainRange {

    private final int rangeId;
    private final String rangeName;

    public MountainRange(int rangeId, String rangeName) {
        this.rangeId = rangeId;
        this.rangeName = rangeName;
    }

    public int getRangeId() {
        return rangeId;
    }

    public String getRangeName() {
        return rangeName;
    }

    @Override
    public String toString() {
        return "ID: " + rangeId + " ; range name: " + rangeName;
    }
}
