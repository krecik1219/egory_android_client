package pl.taktycznie.egory.model;

public class MountainSubrange {

    private final int subrangeId;
    private final String subrangeName;
    private final MountainRange parentRange;

    public MountainSubrange(int subrangeId, String subrangeName, int rangeId, String rangeName) {
        this.subrangeId = subrangeId;
        this.subrangeName = subrangeName;
        this.parentRange = new MountainRange(rangeId, rangeName);
    }
    public int getSubrangeId() {
        return subrangeId;
    }

    public String getSubrangeName() {
        return subrangeName;
    }

    public MountainRange getParentRange() {
        return parentRange;
    }

    @Override
    public String toString() {
        return "ID: " + subrangeId + " ; subrange name: " +
                subrangeName + " ; range id: " + parentRange.getRangeId() +
                " ; range name: " + parentRange.getRangeName();
    }
}
