package pl.taktycznie.egory.model;

public enum TripStatus {
    UNVERIFIED,
    VERIFIED;

    public static TripStatus tripStatusFromInt(int tripStatusId) {
        switch(tripStatusId) {
            case 1:
                return UNVERIFIED;
            case 2:
            default:
                return VERIFIED;
        }
    }
}
