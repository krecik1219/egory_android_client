package pl.taktycznie.egory.model;

import java.util.Objects;

public class TripPath {

    private int tripPathId;
    private final int gotPathId;
    private final String startPointName;
    private final String startPointSubrangeName;
    private final String endPointName;
    private final String endPointSubrangeName;
    private final int upPoints;
    private final int downPoints;
    private final int tripId;
    private PathStatus pathStatus;
    private final int pathOrderNumber;
    private boolean isDone;



    //Kulawy
    //if value is < 0 then it hasn't set already
    private int distance; //distance in meters
    private int heightDifference; //height in meters

    private boolean isIdSet;

    public TripPath(int gotPathId, String startPointName,
                    String startPointSubrangeName, String endPointName,
                    String endPointSubrangeName, int upPoints, int downPoints, int tripId,
                    int pathOrderNumber) {
        this.gotPathId = gotPathId;
        this.startPointName = startPointName;
        this.startPointSubrangeName = startPointSubrangeName;
        this.endPointName = endPointName;
        this.endPointSubrangeName = endPointSubrangeName;
        this.upPoints = upPoints;
        this.downPoints = downPoints;
        this.tripId = tripId;
        this.pathStatus = PathStatus.UNCONFIRMED;
        this.pathOrderNumber = pathOrderNumber;
        this.isDone = false;
        this.isIdSet = false;

    }


    public TripPath(int tripPathId, int gotPathId, String startPointName,
                    String startPointSubrangeName, String endPointName,
                    String endPointSubrangeName, int upPoints, int downPoints, int tripId,
                    int pathStatusId, int pathOrderNumber, boolean isDone) {
        this(
                gotPathId,
                startPointName,
                startPointSubrangeName,
                endPointName,
                endPointSubrangeName,
                upPoints,
                downPoints,
                tripId,
                pathOrderNumber);
        this.tripPathId = tripPathId;
        this.pathStatus = PathStatus.pathStatusFromInt(pathStatusId);
        this.isDone = isDone;
        this.isIdSet = true;
    }

    public int getTripPathId() {
        return tripPathId;
    }

    public void setTripPathId(int tripPathId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, trip path id override attempt");
        this.tripPathId = tripPathId;
        this.isIdSet = true;
    }

    public int getGotPathId() {
        return gotPathId;
    }

    public String getStartPointName() {
        return startPointName;
    }

    public String getStartPointSubrangeName() {
        return startPointSubrangeName;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public String getEndPointSubrangeName() {
        return endPointSubrangeName;
    }

    public int getUpPoints() {
        return upPoints;
    }

    public int getDownPoints() {
        return downPoints;
    }

    public int getTripId() {
        return tripId;
    }

    public PathStatus getPathStatus() {
        return pathStatus;
    }

    public int getPathOrderNumber() {
        return pathOrderNumber;
    }

    public boolean isDone() {
        return isDone;
    }

    public void switchDone() {
        this.isDone = !this.isDone;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getHeightDifference() {
        return heightDifference;
    }

    public void setHeightDifference(int heightDifference) {
        this.heightDifference = heightDifference;
    }

    public int calculatePoints(){

        int points;
        if ( upPoints == 0 && downPoints == 0 )
        {
            points = distance/1000 + heightDifference/100;
        }
        else{
            if (IsUpDirection()){
                points = upPoints;
            }
            else{
                points = downPoints;
            }
        }
        return points;
    }

    private boolean IsUpDirection() {

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripPath tripPath = (TripPath) o;
        return tripPathId == tripPath.tripPathId &&
                gotPathId == tripPath.gotPathId &&
                upPoints == tripPath.upPoints &&
                downPoints == tripPath.downPoints &&
                tripId == tripPath.tripId &&
                pathOrderNumber == tripPath.pathOrderNumber &&
                isDone == tripPath.isDone &&
                Objects.equals(startPointName, tripPath.startPointName) &&
                Objects.equals(startPointSubrangeName, tripPath.startPointSubrangeName) &&
                Objects.equals(endPointName, tripPath.endPointName) &&
                Objects.equals(endPointSubrangeName, tripPath.endPointSubrangeName) &&
                pathStatus == tripPath.pathStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripPathId, gotPathId, startPointName, startPointSubrangeName, endPointName, endPointSubrangeName, upPoints, downPoints, tripId, pathStatus, pathOrderNumber, isDone, isIdSet);
    }

    @Override
    public String toString() {
        return "tripPathId: " + tripPathId +
                " ; gotPathId: " + gotPathId +
                " ; startPointName: " + startPointName +
                " ; startPointSubrangeName: " + startPointSubrangeName +
                " ; endPointName: " + endPointName +
                " ; endPointSubrangeName: " + endPointSubrangeName +
                " ; upPoints: " + upPoints +
                " ; downPoints: " + downPoints +
                " ; tripId: " + tripId +
                " ; pathStatus: " + pathStatus +
                " ; orderNumber: " + pathOrderNumber +
                " ; isDone: " + isDone;
    }

}


