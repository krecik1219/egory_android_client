package pl.taktycznie.egory.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.taktycznie.egory.utils.DateUtils;

public class Trip {

    private int tripId;
    private String tripName;
    private final String creationDateString;
    private final Date creationDate;
    private int tripStatusId;
    private String tripStatusName;
    private final List<TripPath> tripPaths;

    private boolean isIdSet;

    public Trip(String tripName) {
        this.tripName = tripName;
        this.creationDateString = DateUtils.getCurrentDateString();
        this.creationDate = DateUtils.getDateFromString(this.creationDateString);
        this.tripStatusId = 1;
        this.tripStatusName = "UNVERIFIED";
        this.tripPaths = new ArrayList<>();
        this.isIdSet = false;
    }

    public Trip(int tripId, String tripName, String creationDateString, int tripStatusId, String tripStatusName) {
        this.tripId = tripId;
        this.tripName = tripName;
        this.creationDateString = creationDateString;
        this.creationDate = DateUtils.getDateFromString(this.creationDateString);
        this.tripStatusId = tripStatusId;
        this.tripStatusName = tripStatusName;
        this.tripPaths = new ArrayList<>();
        this.isIdSet = true;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        if(isIdSet)
            throw new RuntimeException("Internal error, trip id override attempt");
        this.tripId = tripId;
        this.isIdSet = true;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getCreationDateString() {
        return creationDateString;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public int getTripStatusId() {
        return tripStatusId;
    }

    public String getTripStatusName() {
        return tripStatusName;
    }

    public List<TripPath> getTripPaths() {
        return tripPaths;
    }

    public void addTripPath(TripPath tripPath) {
        tripPaths.add(tripPath);
    }

    public int calculateTripPoints(){

        int points = 0;
        if ( isDone()){
            for ( TripPath tp : tripPaths) {
                points += tp.calculatePoints();
            }
        }
        return  points;
    }

    @Override
    public String toString() {
        return "tripId: " + tripId + " ; tripName: " +
                tripName + " ; creationDate: " +
                creationDateString + " ; tripStatusId: " +
                tripStatusId + " ; tripStatusName: " + tripStatusName;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Trip))
            return false;
        return this.tripId == ((Trip) obj).tripId;
    }

    private boolean isDone(){
        //TODO: odznaczyć jak będzie wycieczka która ma wszystkie odcinki zrobione.
//        for( TripPath tp: tripPaths){
//            if ( !tp.isDone()){
//                return false;
//            }
//        }
        return true;
    }
}
