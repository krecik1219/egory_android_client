package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.model.GotBadge;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.repository.TripsRepository;

public class AchievementsViewModel extends ViewModel {

    private TripPathsRepository tripPathsRepository;

    private LiveData<List<Trip>> userTrips;
    private LiveData<List<TripPath>> userTripPaths;
    private final TripsRepository tripsRepository;
    private final TripDataValidator validator;

    private List<GotBadge> _badgesList;

    private LiveData<List<TripPath>> tripPaths;

    private LiveData<DataAccessError> error;

    private int userId;

    public AchievementsViewModel(TripsRepository tripsRepository, TripPathsRepository tripPathsRepository, TripDataValidator validator) {
        this.tripsRepository = tripsRepository;
        this.validator = validator;
        this.tripPathsRepository = tripPathsRepository;

    }

    public int calculateAchievementPoints(List<Trip> trips){
        int points = 0;
        //initTrips(userId);
        if(trips == null || trips.size() == 0)
            return 0;
        else {
            for ( Trip t : trips){
                points += t.calculateTripPoints();
//                for ( TripPath tp : t.getTripPaths()){
//                    points += tp.calculatePoints();
//                }
            }
        }

        return points;
    }

    public void initTrips(int userId) {
        this.userId = userId;
        this.userTrips = tripsRepository.getUserTrips(userId);
        this.error = tripsRepository.getError();
    }

    public List<TripPath> tripPathsForTrip(Trip trip) {

        List<TripPath> tripPathsOfTrip = Transformations.map(
                tripPathsRepository.getTripPaths(trip.getTripId()),
                input -> {
                    Collections.sort(input, (o1, o2) -> o1.getPathOrderNumber() - o2.getPathOrderNumber());
                    return input;
                }).getValue();
        this.error = tripPathsRepository.getError();

        return tripPathsOfTrip;

    }

    public void initTripPaths(int tripId) {
        this.tripPaths = Transformations.map(
                tripPathsRepository.getTripPaths(tripId),
                input -> {
                    Collections.sort(input, (o1, o2) -> o1.getPathOrderNumber() - o2.getPathOrderNumber());
                    return input;
                });
        this.error = tripPathsRepository.getError();

    }

    public LiveData<List<Trip>> getUserTrips() {
        return userTrips;
    }

    public LiveData<List<TripPath>> getUserTripPaths(Trip trip){
        return tripPathsRepository.getTripPaths(trip.getTripId());

    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public List<GotBadge> getBadgesList(){
        initBadgesList();
        return _badgesList;
    }

    public int getNextBadgeRequirePoints(int points, List<GotBadge> badges) {

        int reqired = 0;//badges.get(0).getRequiredPoints();
        for (int i = 0; i < badges.size(); i++) {
            if ( reqired > points) {
                reqired += badges.get(i).getRequiredPoints();
                break;
            }else {
                reqired += badges.get(i).getRequiredPoints();
            }
        }
        return reqired;
    }

    public GotBadge getActualBadge(int points, List<GotBadge> badges) {

        GotBadge actual = badges.get(0);
        points -= actual.getRequiredPoints();
        for (int i = 0; i < badges.size()-1; i++) {
            if ( badges.get(i).getRequiredPoints() < points && badges.get(i+1).getRequiredPoints() > points ) {
                actual = badges.get(i);
                return actual;
            }else {
                points -= badges.get(i+1).getRequiredPoints();
            }
        }
        return actual;
    }

    public int getActualScoreForBadge(int points, List<GotBadge> badges){
        int reqired =  0;
        for (int i = 0; i < badges.size(); i++) {
            if (badges.get(i).getRequiredPoints() > points - reqired) {
                return points - reqired;
            }
            else {
                reqired += badges.get(i).getRequiredPoints();
            }
        }
        return reqired;
    }

    private void initBadgesList(){
        _badgesList = new ArrayList<GotBadge>();
        _badgesList.add(new GotBadge(1,"W Góry - Brązowa", 15));
        _badgesList.add(new GotBadge(2,"W Góry - Srebrna", 30));
        _badgesList.add(new GotBadge(3,"W Góry - Złota", 45));
        _badgesList.add(new GotBadge(4,"Popularna", 60));
        _badgesList.add(new GotBadge(5,"Mała Brązowa", 120));
        _badgesList.add(new GotBadge(6,"Mała srebrna", 360));
        _badgesList.add(new GotBadge(7,"Mała złota", 720));
        _badgesList.add(new GotBadge(8,"Duża brązowa", 800));
        _badgesList.add(new GotBadge(9,"Duża srebrna", 880));
        _badgesList.add(new GotBadge(10,"Duża złota", 960));
        _badgesList.add(new GotBadge(11,"Za wytrwałość", 1040));
        _badgesList.add(new GotBadge(12,"Przodownik", 1120));
        _badgesList.add(new GotBadge(13,"Honorowy Przodownik", 1250));
    }


}
