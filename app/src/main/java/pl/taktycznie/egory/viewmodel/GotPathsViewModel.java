package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.repository.GotPathsRepository;

public class GotPathsViewModel extends ViewModel {

    private int subrangeId;
    private LiveData<List<GotPath>> gotPathsBySubrange;
    private LiveData<DataAccessError> error;
    private GotPathsRepository gotPathsRepository;

    public GotPathsViewModel(GotPathsRepository gotPathsRepository) {
        this.gotPathsRepository = gotPathsRepository;
    }

    public void initWithSubrangeId(int subrangeId) {
        this.subrangeId = subrangeId;
        this.gotPathsBySubrange = gotPathsRepository.getGotPathsBySubrange(subrangeId);
        this.error = gotPathsRepository.getError();
    }

    public LiveData<List<GotPath>> getGotPathsBySubrange() {
        return gotPathsBySubrange;
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public void addGotPath(GotPath gotPath){
        this.gotPathsRepository.addGotPath(gotPath);
    }

    public void removeGotPath(GotPath gotPath){
        this.gotPathsRepository.removeTripPath(gotPath);
    }

    public void editGotPath(GotPath gotPath, int upPoints, int downPoints){
        this.gotPathsRepository.editGotPath(gotPath, upPoints, downPoints);
    }


}
