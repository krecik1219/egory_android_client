package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.repository.SubrangesRepository;
import pl.taktycznie.egory.viewmodel.SubrangesViewModel;

public class SubrangesViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final SubrangesRepository _subrangesRepository;

    public SubrangesViewModelFactory(SubrangesRepository subrangesRepository){
        _subrangesRepository = subrangesRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SubrangesViewModel(_subrangesRepository);
    }


}
