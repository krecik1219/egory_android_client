package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.repository.RangesRepository;
import pl.taktycznie.egory.viewmodel.RangesViewModel;
import pl.taktycznie.egory.viewmodel.RangesViewModel;

public class RangesViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final RangesRepository _rangesRepository;

    public RangesViewModelFactory(RangesRepository rangesRepository){
        _rangesRepository = rangesRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new RangesViewModel(_rangesRepository);
    }

}
