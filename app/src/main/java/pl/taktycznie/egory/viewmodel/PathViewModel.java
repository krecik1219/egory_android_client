package pl.taktycznie.egory.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.annotation.NonNull;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessException;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.model.GotPath;

public class PathViewModel extends AndroidViewModel {

    //private PathsRepository repository;
    private GotPathDao repo;
    private List<GotPath> allPaths;

    public PathViewModel(@NonNull Application application, int subRange) {
        super(application);
        //repository = new PathsRepository(application);
        repo = new GotPathDao(application);

//        try {
//            allPaths = repo.getGotPathsBySubrange(subRange);
//        } catch (DataAccessException e) {
//            e.printStackTrace();
//        }
    }

    public List<GotPath> getAllPathsForSubRange(){
        return allPaths;
    }



}
