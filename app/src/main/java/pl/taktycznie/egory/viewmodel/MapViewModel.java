package pl.taktycznie.egory.viewmodel;

import android.app.Application;

import androidx.collection.ArrayMap;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;

import android.util.Log;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.BoolPaired;
import pl.taktycznie.egory.helpers.Pair;
import pl.taktycznie.egory.model.tracking.LocationMaintainer;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.model.tracking.PhotoTag;
import pl.taktycznie.egory.model.tracking.Tracker;
import pl.taktycznie.egory.repository.LocationRepository;
import pl.taktycznie.egory.repository.persistance.LocationDb;
import pl.taktycznie.egory.utils.ColorProvider;
import pl.taktycznie.egory.utils.DateUtils;
import pl.taktycznie.egory.utils.IMarkerOnClickFn;
import pl.taktycznie.egory.utils.PolylineMaker;

public class MapViewModel extends AndroidViewModel {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MapViewModel.class.getSimpleName();

    private LocationRepository locationRepository;
    private Tracker tracker;
    private int userId;
    private LiveData<LocationMaintainer> locationMaintainerLD;
    private Tracker.TrackerState trackingState;
    private MutableLiveData<Boolean> trackingStateChanged;

    // UI state to counteract rotations
    private List<BoolPaired<LocationSession>> locationSessionsList;
    private BoolPaired<LocationSession> currentTrackingSession;
    private List<GuiShape> guiShapes;
    private Map<LatLng, IMarkerOnClickFn> markerLocationToOnClickFnMap;

    private PolylineMaker polylineMaker;

    public MapViewModel(@NonNull Application application, LocationRepository locationRepository, Tracker tracker, int userId) {
        super(application);
        Log.d(TAG, "MapViewModel ctor");
        this.locationRepository = locationRepository;
        this.tracker = tracker;
        this.userId = userId;
        this.trackingState = tracker.getTrackerState();
        this.trackingStateChanged = new MutableLiveData<>();
        this.trackingStateChanged.setValue(false);
        this.locationSessionsList = new ArrayList<>();
        this.guiShapes = new ArrayList<>();
        this.markerLocationToOnClickFnMap = new ArrayMap<>();
        this.polylineMaker = new PolylineMaker(new ColorProvider());
    }

    public void init() {
        LocationDb.lock("getLocationMaintainerLD");
        locationMaintainerLD = locationRepository.getLocationMaintainerLD(userId);
        LocationDb.release("getLocationMaintainerLD");
    }

    public Tracker.TrackerState toggleTracking() {
        LocationDb.lock("toggleTracking");
        Tracker.TrackerState state = tracker.toggleTracking(locationRepository.getLocationMaintainerLD().getValue());
        setTrackingState(state);
        if (state == Tracker.TrackerState.ACTIVE) {
            locationRepository.saveNewTrackingSession(userId);
        } else
            locationRepository.updateCurrentSession(null, DateUtils.getCurrentDatetime());
        LocationDb.release("toggleTracking");
        return state;
    }

    private void setTrackingState(Tracker.TrackerState trackingState) {
        if (this.trackingState == trackingState)
            return;
        this.trackingState = trackingState;
        this.trackingStateChanged.setValue(true);
    }

    public void setTrackingSessionName(@NonNull String sessionName) {
        locationRepository.updateCurrentSession(sessionName, null);
    }

    public Tracker.TrackerState getTrackerState() {
        return tracker.getTrackerState();
    }

    public LiveData<LocationMaintainer> getLocationMaintainerLD() {
        return locationMaintainerLD;
    }

    public LiveData<Boolean> getTrackingStateChangedStatus() {
        return trackingStateChanged;
    }

    public List<BoolPaired<LocationSession>> getLocationSessionsList() {
        return locationSessionsList;
    }

    public void setLocationSessionsList(List<BoolPaired<LocationSession>> locationSessionsList) {
        this.locationSessionsList = locationSessionsList;
    }

    public BoolPaired<LocationSession> getCurrentTrackingSession() {
        return currentTrackingSession;
    }

    public void setCurrentTrackingSession(BoolPaired<LocationSession> currentTrackingSession) {
        this.currentTrackingSession = currentTrackingSession;
    }

    public IMarkerOnClickFn getMarkerOnClickFn(Marker marker) {
        return markerLocationToOnClickFnMap.get(marker.getPosition());
    }

    public List<GuiShape> getGuiShapes() {
        return guiShapes;
    }

    public void setGuiShapes(List<GuiShape> guiShapes) {
        this.guiShapes = guiShapes;
    }

    public void destroyGuiShapes() {
        Log.d(TAG, "MapViewModel destroyGuiShapes");
        for (GuiShape guiShape : guiShapes) {
            guiShape.polyline.remove();
            for(Marker m : guiShape.markers)
                m.remove();
        }
        guiShapes.clear();
        Log.d(TAG, "MapViewModel destroyGuiShapes: after: " + guiShapes);
    }

    public void updateSessionName(String sessionName, LocationSession session) {
        Log.d(TAG, "MapViewModel updateSessionName");
        LocationDb.lock("updateSessionName");
        locationRepository.updateSession(session, sessionName, null);
        LocationDb.release("updateSessionName");
    }

    public void addGuiShape(List<LatLng> points, List<Pair<LatLng, List<PhotoTag>>> pointsTags, GoogleMap gMap, LocationSession locationSession) {
        PolylineOptions polylineOptions = polylineMaker.makePolylineOptions(points);
        Polyline polyline = gMap.addPolyline(polylineOptions);
        List<Marker> markers = new ArrayList<>();
        for(Pair<LatLng, List<PhotoTag>> pair : pointsTags) {
            MarkerOptions markerOptions = new MarkerOptions().position(pair.first);
            final List<String> imageFiles = Stream.of(pair.second).map(PhotoTag::getImageFilePath).collect(Collectors.toList());
            markerLocationToOnClickFnMap.put(pair.first, () -> imageFiles);
            markers.add(gMap.addMarker(markerOptions));
        }
        guiShapes.add(new GuiShape(polyline, locationSession, markers));
    }

    public Pair<GuiShape, Integer> getGuiShapeMatchingTrackingSession(LocationSession locationSession) {
        Log.d(TAG, "getGuiShapeMatchingTrackingSession(): locationSession: " + locationSession);
        GuiShape gs = null;
        int index = -1;
        List<GuiShape> guiShapes = getGuiShapes();
        for (int i = 0; i < guiShapes.size() && gs == null; i++) {
            GuiShape guiShape = guiShapes.get(i);
            if (guiShape.locationSession.equalsWithoutLastPoint(locationSession)) {
                Log.d(TAG, "getGuiShapeMatchingTrackingSession(): FOUND: index=" + i + " ; location session: " + locationSession);
                gs = guiShape;
                index = i;
            }
        }
        return new Pair<>(gs, index);
    }

    public void removeGuiShape(Pair<GuiShape, Integer> guiShapeAndIndex) {
        polylineMaker.freeColor(guiShapeAndIndex.first.polyline);
        guiShapeAndIndex.first.polyline.remove();
        for(Marker m : guiShapeAndIndex.first.markers) {
            markerLocationToOnClickFnMap.remove(m.getPosition());
            m.remove();
        }
        guiShapes.remove(guiShapeAndIndex.second.intValue());
    }

    public static final class GuiShape {
        public Polyline polyline;
        public LocationSession locationSession;
        public List<Marker> markers;

        public GuiShape(Polyline polyline, LocationSession locationSession) {
            this.polyline = polyline;
            this.locationSession = locationSession;
            this.markers = new ArrayList<>();
        }

        public GuiShape(Polyline polyline, LocationSession locationSession, List<Marker> markers) {
            this.polyline = polyline;
            this.locationSession = locationSession;
            this.markers = markers;
        }

        @Override
        public String toString() {
            return "GuiShape{" +
                    "polyline=" + polyline +
                    ", locationSession=" + locationSession +
                    ", markers=" + markers +
                    '}';
        }
    }
}
