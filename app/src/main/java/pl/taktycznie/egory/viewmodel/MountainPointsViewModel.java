package pl.taktycznie.egory.viewmodel;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import android.os.Build;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.model.MountainPoint;
import pl.taktycznie.egory.repository.MtPointRepository;

public class MountainPointsViewModel  extends ViewModel {

    private int _subrangeId;
    private LiveData<List<MountainPoint>> _mountainPoints;
    private LiveData<List<List<MountainPoint>>> _listMountainPoints;
    private List<MountainPoint>_mountainPointsList;
    private LiveData<DataAccessError> _error;
    private MtPointRepository _mtPointsRepository;

    public MountainPointsViewModel(MtPointRepository mtPointRepository){
        _mtPointsRepository = mtPointRepository;
    }

    public void init(){
        this._mountainPoints = _mtPointsRepository.getSpecificMountainPoints();// tu z All na specific
        _error = _mtPointsRepository.getError();
    }
    public void init2(){
        this._listMountainPoints = _mtPointsRepository.getSpecificMountainPoints2();// tu z All na specific
        _error = _mtPointsRepository.getError();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void initWithSubrangeId(int subrangeId){
        _subrangeId = subrangeId;
        _mountainPoints = _mtPointsRepository.getAllMountainPoints();
        _mountainPointsList = Stream.of(_mountainPoints.getValue())
                .filter( p -> (p.getSubrange().getSubrangeId() == subrangeId))
                .collect(Collectors.toList());
        _error = _mtPointsRepository.getError();
    }

    public LiveData<List<List<MountainPoint>>> getMountainPoints2() {
        return _listMountainPoints;
    }
    public LiveData<List<MountainPoint>> getMountainPoints() {
        return _mountainPoints;
    }

    public List<MountainPoint> getMoutainPoints(){
        return  _mountainPointsList;
    }

    public LiveData<DataAccessError> getError() {
        return _error;
    }

}
