package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.repository.TripsRepository;
import pl.taktycznie.egory.viewmodel.TripsViewModel;

public class TripsViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final TripsRepository tripsRepository;
    private final TripDataValidator validator;

    public TripsViewModelFactory(TripsRepository tripsRepository, TripDataValidator validator) {
        this.tripsRepository = tripsRepository;
        this.validator = validator;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new TripsViewModel(tripsRepository, validator);
    }
}
