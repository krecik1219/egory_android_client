package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.model.MountainRange;
import pl.taktycznie.egory.repository.RangesRepository;

public class RangesViewModel extends ViewModel {

    private LiveData<List<MountainRange>> _mountainRanges;
    private LiveData<DataAccessError> _error;
    private RangesRepository _rangesRepository;

    public RangesViewModel(RangesRepository rangesRepository){
        _rangesRepository = rangesRepository;
    }

    public void init(){
        _mountainRanges = _rangesRepository.getAllMountainRanges();
        _error = _rangesRepository.getError();
    }

    public LiveData<List<MountainRange>> getMountainRanges(){
        return _mountainRanges;
    }

    public LiveData<DataAccessError> getError(){
        return _error;
    }

}
