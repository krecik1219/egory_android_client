package pl.taktycznie.egory.viewmodel.factories;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.repository.TripsRepository;
import pl.taktycznie.egory.viewmodel.AchievementsViewModel;

public class AchievementsViewModelFactory extends ViewModelProvider.NewInstanceFactory{

    private final TripsRepository tripsRepository;
    private final TripPathsRepository tripPathsRepository;
    private final TripDataValidator validator;

    public AchievementsViewModelFactory(TripsRepository tripsRepository,TripPathsRepository tripPathsRepository, TripDataValidator validator) {
        this.tripsRepository = tripsRepository;
        this.tripPathsRepository = tripPathsRepository;
        this.validator = validator;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new AchievementsViewModel(tripsRepository, tripPathsRepository, validator);
    }


}
