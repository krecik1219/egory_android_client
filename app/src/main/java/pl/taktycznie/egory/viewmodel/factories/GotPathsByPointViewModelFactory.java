package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.repository.GotPathsRepository;
import pl.taktycznie.egory.viewmodel.GotPathsByPointViewModel;

public class GotPathsByPointViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final GotPathsRepository gotPathsRepository;

    public GotPathsByPointViewModelFactory(GotPathsRepository gotPathsRepository) {
        this.gotPathsRepository = gotPathsRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new GotPathsByPointViewModel(gotPathsRepository);
    }
}
