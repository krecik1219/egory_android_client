package pl.taktycznie.egory.viewmodel.factories;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import pl.taktycznie.egory.repository.UserDataRepository;
import pl.taktycznie.egory.viewmodel.UserLoginRegisterViewModel;

public class UserLoginRegisterViewModelFactory extends ViewModelProvider.AndroidViewModelFactory {

    private final Application application;
    private final UserDataRepository userDataRepository;

    public UserLoginRegisterViewModelFactory(@NonNull Application application, UserDataRepository userDataRepository) {
        super(application);
        this.application = application;
        this.userDataRepository = userDataRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new UserLoginRegisterViewModel(application, userDataRepository);
    }
}
