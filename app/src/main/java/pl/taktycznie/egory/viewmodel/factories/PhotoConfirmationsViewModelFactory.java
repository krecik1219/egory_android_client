package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.repository.PhotoConfirmationsRepository;
import pl.taktycznie.egory.viewmodel.PhotoConfirmationsViewModel;

public class PhotoConfirmationsViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PhotoConfirmationsRepository photoConfirmationsRepository;

    public PhotoConfirmationsViewModelFactory(PhotoConfirmationsRepository photoConfirmationsRepository) {
        this.photoConfirmationsRepository = photoConfirmationsRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new PhotoConfirmationsViewModel(photoConfirmationsRepository);
    }
}
