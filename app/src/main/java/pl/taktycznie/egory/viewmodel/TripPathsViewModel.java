package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.Collections;
import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.TripPathParamsHolder;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.TripPathsRepository;

public class TripPathsViewModel extends ViewModel {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripPathsViewModel.class.getSimpleName();

    private int tripId;
    private LiveData<List<TripPath>> tripPaths;
    private LiveData<DataAccessError> error;
    private TripPathsRepository tripPathsRepository;

    public TripPathsViewModel(TripPathsRepository tripPathsRepository) {
        this.tripPathsRepository = tripPathsRepository;
    }

    public void init(int tripId) {
        this.tripId = tripId;
        this.tripPaths = Transformations.map(
                tripPathsRepository.getTripPaths(tripId),
                input -> {
                    Collections.sort(input, (o1, o2) -> o1.getPathOrderNumber() - o2.getPathOrderNumber());
                    return input;
                });
        this.error = tripPathsRepository.getError();
    }

    public LiveData<List<TripPath>> getTripPaths() {
        return tripPaths;
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public void addTripPath(TripPathParamsHolder paramsHolder) {
        int lastOrderingNumber = getLastOrderingNumber();
        TripPath tripPath = new TripPath(
                paramsHolder.gotPath.getPathId(),
                paramsHolder.gotPath.getStartPoint().getPointName(),
                paramsHolder.gotPath.getStartPoint().getSubrange().getSubrangeName(),
                paramsHolder.gotPath.getEndPoint().getPointName(),
                paramsHolder.gotPath.getEndPoint().getSubrange().getSubrangeName(),
                paramsHolder.gotPath.getUpPoints(),
                paramsHolder.gotPath.getDownPoints(),
                paramsHolder.tripId,
                lastOrderingNumber + 1
        );
        this.tripPathsRepository.addTripPath(tripPath);
    }

    private int getLastOrderingNumber() {
        List<TripPath> tripPathsList = tripPaths.getValue();
        if(tripPathsList == null)
            return 0;
        int maxOrdering = 0;
        for(TripPath tripPath: tripPathsList) {
            if(tripPath.getPathOrderNumber() > maxOrdering)
                maxOrdering = tripPath.getPathOrderNumber();
        }
        return maxOrdering;
    }

    public void removeTripPath(TripPath tripPath) {
        this.tripPathsRepository.removeTripPath(tripPath);
    }

    public void switchCheckDone(TripPath tripPath) {
        this.tripPathsRepository.switchCheckDone(tripPath);
    }
}
