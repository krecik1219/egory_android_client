package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;


import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.repository.TripsRepository;

public class TripsViewModel extends ViewModel {

    private int userId;
    private LiveData<List<Trip>> userTrips;
    private LiveData<DataAccessError> error;
    private final TripsRepository tripsRepository;
    private final TripDataValidator validator;

    public TripsViewModel(TripsRepository tripsRepository, TripDataValidator validator) {
        this.tripsRepository = tripsRepository;
        this.validator = validator;
    }

    public void init(int userId) {
        this.userId = userId;
        this.userTrips = tripsRepository.getUserTrips(userId);
        this.error = tripsRepository.getError();
    }

    public LiveData<List<Trip>> getUserTrips() {
        return userTrips;
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public void addTrip(Trip trip) {
        if(validator.validateTripName(trip.getTripName()))
            tripsRepository.addTrip(trip, userId);
    }

    public void editTrip(Trip trip, String tripName) {
        if(validator.validateTripName(tripName))
            tripsRepository.editTrip(trip, tripName);
    }

    public void removeTrip(Trip trip) {
        tripsRepository.removeTrip(trip);
    }
}
