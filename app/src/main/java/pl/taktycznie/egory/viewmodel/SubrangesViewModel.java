package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.model.MountainSubrange;
import pl.taktycznie.egory.repository.SubrangesRepository;

public class SubrangesViewModel extends ViewModel {

    private LiveData<List<MountainSubrange>> _mountainSubranges;
    private LiveData<DataAccessError> _error;
    private SubrangesRepository _subrangesRepository;

    public SubrangesViewModel(SubrangesRepository rangesRepository){
        _subrangesRepository = rangesRepository;
    }

    public void init(int rangeId){
        _mountainSubranges = _subrangesRepository.getMountainSubrangesByRange(rangeId);
        _error = _subrangesRepository.getError();
    }

    public LiveData<List<MountainSubrange>> getMountainSubranges(){
        return _mountainSubranges;
    }

    public LiveData<DataAccessError> getError(){
        return _error;
    }

}
