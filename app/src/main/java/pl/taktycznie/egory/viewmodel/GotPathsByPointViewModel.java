package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.repository.GotPathsRepository;

public class GotPathsByPointViewModel extends ViewModel {

    public enum SearchMode {
        GIVEN_START_POINT,
        GIVEN_END_POINT;

        public static SearchMode fromBoolean(boolean mode) {
            if(mode)
                return GIVEN_END_POINT;
            else
                return GIVEN_START_POINT;
        }
    }

    private SearchMode searchMode;
    private LiveData<List<GotPath>> gotPathsByPoint;
    private LiveData<DataAccessError> error;
    private GotPathsRepository gotPathsRepository;

    public GotPathsByPointViewModel(GotPathsRepository gotPathsRepository) {
        this.gotPathsRepository = gotPathsRepository;
    }

    public void init(SearchMode searchMode) {
        this.searchMode = searchMode;
        this.error = gotPathsRepository.getError();
    }

    public void changeSearchMode(SearchMode newSearchMode) {
        this.searchMode = newSearchMode;
    }

    public LiveData<List<GotPath>> getGotPathsByPoint(String pointName) {
        fetchGotPathsWithRespectToSearchMode(pointName);
        return gotPathsByPoint;
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    private void fetchGotPathsWithRespectToSearchMode(String pointName) {
        //noinspection ConstantConditions
        switch (this.searchMode) {
            case GIVEN_START_POINT:
                this.gotPathsByPoint = gotPathsRepository.getGotPathsByStartPoint(pointName);
                break;
            case GIVEN_END_POINT:
                this.gotPathsByPoint = gotPathsRepository.getGotPathsByEndPoint(pointName);
                break;
        }
    }
}
