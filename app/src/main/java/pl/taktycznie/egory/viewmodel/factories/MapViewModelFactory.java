package pl.taktycznie.egory.viewmodel.factories;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.model.tracking.Tracker;
import pl.taktycznie.egory.repository.LocationRepository;
import pl.taktycznie.egory.viewmodel.MapViewModel;

public class MapViewModelFactory extends ViewModelProvider.AndroidViewModelFactory {

    private final Application application;
    private final LocationRepository locationRepository;
    private final Tracker tracker;
    private final int userId;

    public MapViewModelFactory(@NonNull Application application, LocationRepository locationRepository, Tracker tracker, int userId) {
        super(application);
        this.application = application;
        this.locationRepository = locationRepository;
        this.tracker = tracker;
        this.userId = userId;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MapViewModel(application, locationRepository, tracker, userId);
    }
}
