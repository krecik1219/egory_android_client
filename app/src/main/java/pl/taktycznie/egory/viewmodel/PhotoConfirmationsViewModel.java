package pl.taktycznie.egory.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Bitmap;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.model.PhotoConfirmation;
import pl.taktycznie.egory.repository.PhotoConfirmationsRepository;

public class PhotoConfirmationsViewModel extends ViewModel {

    private int tripPathId;
    private LiveData<List<PhotoConfirmation>> photoConfirmations;
    private LiveData<DataAccessError> error;
    private PhotoConfirmationsRepository photoConfirmationsRepository;

    public PhotoConfirmationsViewModel(PhotoConfirmationsRepository photoConfirmationsRepository) {
        this.photoConfirmationsRepository = photoConfirmationsRepository;
    }

    public void init(int tripPathId) {
        this.tripPathId = tripPathId;
        this.photoConfirmations = photoConfirmationsRepository.getPhotoConfirmations(tripPathId);
        this.error = photoConfirmationsRepository.getError();
    }

    public LiveData<List<PhotoConfirmation>> getPhotoConfirmations() {
        return photoConfirmations;
    }

    public void addPhotoConfirmation(Bitmap image) {
        PhotoConfirmation photoConfirmation = new PhotoConfirmation(image, tripPathId);
        photoConfirmationsRepository.addPhotoConfirmation(photoConfirmation);
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }
}
