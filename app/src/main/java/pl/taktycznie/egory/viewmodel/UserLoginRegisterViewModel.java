package pl.taktycznie.egory.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.helpers.UserLoginCacheAccess;
import pl.taktycznie.egory.model.user.User;
import pl.taktycznie.egory.model.user.UserLoginManager;
import pl.taktycznie.egory.repository.UserDataRepository;
import pl.taktycznie.egory.repository.persistance.UserDataDb;
import pl.taktycznie.egory.view.fragments.helpers.LoginParametersPack;
import pl.taktycznie.egory.view.fragments.helpers.RegisterParametersPack;

public class UserLoginRegisterViewModel extends AndroidViewModel {

    private UserDataRepository userDataRepository;
    private UserLoginManager userLoginManager;
    private LiveData<User> userLiveData;
    private LiveData<DataAccessError> error;

    public UserLoginRegisterViewModel(Application app, UserDataRepository userDataRepository) {
        super(app);
        this.userDataRepository = userDataRepository;
        this.userLoginManager = new UserLoginManager(new UserLoginCacheAccess(app));
        this.userDataRepository.setUserLoginManager(this.userLoginManager);
        this.error = userDataRepository.getError();
        this.userLiveData = UserDataDb.getInstance().getData();
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public boolean logIfCached() {
        if(!userLoginManager.isUserLogged())
            return false;

        userLoginManager.logCachedUser();
        return true;
    }

    public void logUser(LoginParametersPack params) {
        userDataRepository.loginUser(params);
    }

    public void registerUser(RegisterParametersPack params) {
        userDataRepository.registerUser(params);
    }

    public LiveData<User> getUser() {
        return userLiveData;
    }

    public boolean isValidUser(User user) {
        if(user == null || user.getUserId() == 0)
            return false;

        return user.equals(userLiveData.getValue());
    }

}
