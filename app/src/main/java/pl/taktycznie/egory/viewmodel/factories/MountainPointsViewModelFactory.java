package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.model.MountainPoint;
import pl.taktycznie.egory.repository.MtPointRepository;
import pl.taktycznie.egory.viewmodel.MountainPointsViewModel;

public class MountainPointsViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final MtPointRepository _mtPointsRepository;

    public MountainPointsViewModelFactory(MtPointRepository mtPointsRepository){
        _mtPointsRepository = mtPointsRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MountainPointsViewModel(_mtPointsRepository);
    }

}
