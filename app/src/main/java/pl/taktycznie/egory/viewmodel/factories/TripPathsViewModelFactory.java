package pl.taktycznie.egory.viewmodel.factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.viewmodel.TripPathsViewModel;

public class TripPathsViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final TripPathsRepository tripPathsRepository;

    public TripPathsViewModelFactory(TripPathsRepository tripPathsRepository) {
        this.tripPathsRepository = tripPathsRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new TripPathsViewModel(tripPathsRepository);
    }
}
