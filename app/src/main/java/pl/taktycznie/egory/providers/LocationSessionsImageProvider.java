package pl.taktycznie.egory.providers;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.repository.persistance.LocationDb;

public class LocationSessionsImageProvider {
    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationSessionsImageProvider.class.getSimpleName();

    public List<LocationSession> getLocationSessionsImage() {
        Log.d(TAG, "getLocationSessionsImage");
        List<LocationSession> image = new ArrayList<>();
        LocationDb.lock("getLocationSessionsImage");
        List<LocationSession> locationSessions = LocationDb.getInstance().getDataValue().getLocationSessions();
        for(LocationSession ls : locationSessions)
            image.add(new LocationSession(ls));
        LocationDb.release("getLocationSessionsImage");
        return image;
    }
}
