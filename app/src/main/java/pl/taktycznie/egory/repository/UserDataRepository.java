package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.UserDataDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.user.User;
import pl.taktycznie.egory.model.user.UserLoginManager;
import pl.taktycznie.egory.view.fragments.helpers.LoginParametersPack;
import pl.taktycznie.egory.view.fragments.helpers.RegisterParametersPack;

public class UserDataRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + UserDataRepository.class.getSimpleName();

    private UserDataDao userDataDao;
    private MutableLiveData<DataAccessError> error;
    private UserLoginManager userLoginManager;

    public UserDataRepository(UserDataDao userDataDao) {
        this.userDataDao = userDataDao;
        this.error = new MutableLiveData<>();
    }

    public void setUserLoginManager(UserLoginManager userLoginManager) {
        this.userLoginManager = userLoginManager;
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public void loginUser(LoginParametersPack params) {
        userDataDao.loginUser(params, resource -> {
            if(resource.isSuccessful()) {
                userLoginManager.logUser(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void registerUser(RegisterParametersPack params) {
        userDataDao.registerUser(params, resource -> {
            if(resource.isSuccessful()) {
                User u = new User(
                        resource.getData(),
                        params.login,
                        params.name,
                        params.surname
                );
                userLoginManager.logUser(u);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }
}
