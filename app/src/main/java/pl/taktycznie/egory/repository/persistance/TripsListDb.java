package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.Trip;

public final class TripsListDb extends AbstractListDb<Trip> {

    private static TripsListDb instance = null;

    synchronized public static TripsListDb getInstance() {
        if(instance == null)
            instance = new TripsListDb();
        return instance;
    }
}
