package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.TripPathDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.persistance.TripPathsListDb;

public class TripPathsRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripPathsRepository.class.getSimpleName();

    private TripPathDao tripPathDao;
    private TripPathsListDb tripPathsDb = TripPathsListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public TripPathsRepository(TripPathDao tripPathDao) {
        this.tripPathDao = tripPathDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public LiveData<List<TripPath>> getTripPaths(int tripId) {
        tripPathDao.getTripPathsByTrip(tripId, resource -> {
            if(resource.isSuccessful()) {
                tripPathsDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return tripPathsDb.getData();
    }

    public void addTripPath(TripPath tripPath) {
        tripPathDao.insertTripPath(tripPath, resource -> {
            if(resource.isSuccessful()) {
                tripPath.setTripPathId(resource.getData());
                tripPathsDb.addSingleValue(tripPath);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void removeTripPath(TripPath tripPath) {
        tripPathDao.deleteTripPath(tripPath, resource -> {
            if(resource.isSuccessful()) {
                tripPathsDb.removeSingleValue(tripPath);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void switchCheckDone(TripPath tripPath) {
        Log.d(TAG + " switchCheckDone", "trying to use dao to switch done in: " + tripPath);
        tripPathDao.updateTripPath(tripPath, resource -> {
            if(resource.isSuccessful()) {
                tripPath.switchDone();
                tripPathsDb.editNotify();
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }
}
