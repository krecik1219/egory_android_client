package pl.taktycznie.egory.repository.persistance;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;


public abstract class AbstractListDb<T> {

    private MutableLiveData<List<T>> data;
    private MutableLiveData<List<List<T>>> data2;

    AbstractListDb() {
        this.data = new MutableLiveData<>();
        this.data.setValue(new ArrayList<>());
        this.data2=new MutableLiveData<>();
        this.data2.setValue(new ArrayList<>());
    }

    public LiveData<List<T>> getData() {
        return data;
    }

    public LiveData<List<List<T>>> getData2() {
        return data2;
    }

    public void setDataValue(List<T> data) {
        this.data.setValue(data);
    }

    public void setDataValue2(List<List<T>> data) {
        this.data2.setValue(data);
    }

    public void addSingleValue(T value) {
        List<T> data = this.data.getValue();
        if(data == null)
            data = new ArrayList<>();
        data.add(value);
        this.data.setValue(data);
    }

    public void addSingleValueInFront(T value) {
        List<T> data = this.data.getValue();
        if(data == null)
            data = new ArrayList<>();
        data.add(0, value);
        this.data.setValue(data);
    }

    public void editNotify() {
        this.data.setValue(this.data.getValue());
    }

    public void removeSingleValue(T value) {
        List<T> data = this.data.getValue();
        if(data == null)
            data = new ArrayList<>();
        data.remove(value);
        this.data.setValue(data);
    }
}
