package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.MountainRangeDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainRange;
import pl.taktycznie.egory.repository.persistance.RangesListDb;

public class RangesRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + RangesRepository.class.getSimpleName();

    private MountainRangeDao _rangeDao;
    private RangesListDb _rangesDb = RangesListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public RangesRepository(MountainRangeDao rangeDao){
        _rangeDao = rangeDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError(){
        return error;
    }

    public LiveData<List<MountainRange>> getAllMountainRanges(){
        _rangeDao.getAllMountainRanges(resource -> {
            if (resource.isSuccessful()) {
                _rangesDb.setDataValue(resource.getData());
                error.setValue(null);
            } else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));

        return _rangesDb.getData();
    }



}
