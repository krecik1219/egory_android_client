package pl.taktycznie.egory.repository.persistance;

public class DbRuntimeException extends RuntimeException {

    public DbRuntimeException() {
    }

    public DbRuntimeException(String message) {
        super(message);
    }
}
