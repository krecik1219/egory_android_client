package pl.taktycznie.egory.repository.persistance;

public class DbException extends Exception {

    public DbException() {
    }

    public DbException(String message) {
        super(message);
    }
}
