package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.TripPath;

public final class TripPathsListDb extends AbstractListDb<TripPath> {

    private static TripPathsListDb instance = null;

    synchronized public static TripPathsListDb getInstance() {
        if(instance == null)
            instance = new TripPathsListDb();
        return instance;
    }
}
