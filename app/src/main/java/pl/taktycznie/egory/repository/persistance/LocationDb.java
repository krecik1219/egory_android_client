package pl.taktycznie.egory.repository.persistance;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.concurrent.Semaphore;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.tracking.LocationMaintainer;

public class LocationDb extends AbstractObjectDb<LocationMaintainer> {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE +  LocationDb.class.getSimpleName();

    private static LocationDb instance = null;
    private MutableLiveData<DataAccessError> error;

    private static Semaphore lock = new Semaphore(1);

    public static void lock() {
        Log.d(TAG, "acquire: thread id: " + Thread.currentThread().getId() + " ; thread name: " + Thread.currentThread().getName());
        try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void lock(String logMsg) {
        Log.d(TAG, "acquire: " + logMsg + " ; thread id: " + Thread.currentThread().getId() + " ; thread name: " + Thread.currentThread().getName());
        try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void release() {
        Log.d(TAG, "release: thread id: " + Thread.currentThread().getId() + " ; thread name: " + Thread.currentThread().getName());
        lock.release();
    }

    public static void release(String logMsg) {
        Log.d(TAG, "release: " + logMsg + " ; thread id: " + Thread.currentThread().getId() + " ; thread name: " + Thread.currentThread().getName());
        lock.release();
    }

    private LocationDb() {
        super(LocationMaintainer.class);
        error = new MutableLiveData<>();
    }

    public static LocationDb getInstance() {
        if(instance == null)
            instance = new LocationDb();
        return instance;
    }

    public void setError(DataAccessError error) {
        this.error.postValue(error);
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public void putValue(LocationMaintainer locationMaintainer) {
        data.postValue(locationMaintainer);
    }

    public LiveData<LocationMaintainer> getValueLD() {
        return data;
    }

    public LocationMaintainer getDataValue() {
        return data.getValue();
    }

    public void editNotify() {
        this.data.postValue(this.data.getValue());
    }
}
