package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.MountainSubrange;

public class SubrangesListDb extends AbstractListDb<MountainSubrange> {

    private static SubrangesListDb instance = null;

    synchronized public static SubrangesListDb getInstance() {
        if(instance == null)
            instance = new SubrangesListDb();
        return instance;
    }

}
