package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.GotPath;

public final class GotPathsListDb extends AbstractListDb<GotPath> {

    private static GotPathsListDb instance = null;

    synchronized public static GotPathsListDb getInstance() {
        if(instance == null)
            instance = new GotPathsListDb();
        return instance;
    }
}
