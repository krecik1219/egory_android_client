package pl.taktycznie.egory.repository.persistance;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractObjectDb<T> {

    protected MutableLiveData<T> data;

    AbstractObjectDb(Class<T> clazz) {
        try {
            this.data = new MutableLiveData<>();
            T value = clazz.getConstructor().newInstance();
            this.data.setValue(value);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new DbRuntimeException("Class: " + clazz.getSimpleName() + " must be default constructable");
        }
    }

    public LiveData<T> getData() {
        return data;
    }

    public T getDataValue() {
        return data.getValue();
    }

    public void setDataValue(T data) {
        this.data.setValue(data);
    }

    public void editNotify() {
        this.data.setValue(this.data.getValue());
    }

}
