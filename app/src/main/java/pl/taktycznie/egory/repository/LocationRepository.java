package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.Date;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.tracking.LocationMaintainer;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.repository.persistance.LocationDb;
import pl.taktycznie.egory.services.LocationDataAccessService;
import pl.taktycznie.egory.utils.DateUtils;

public class LocationRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationRepository.class.getSimpleName();

    private Context context;
    private LocationDb locationDb = LocationDb.getInstance();
    private MutableLiveData<DataAccessError> error;


    public LocationRepository(Context context) {
        this.context = context;
        this.error = new MutableLiveData<>();
    }

    public void saveNewTrackingSession(int userId) {
        Log.d(TAG + " saveNewTrackingSession", "entered");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        LocationDataAccessService.startActionSaveTrackingSession(context, userId, locationMaintainer.getCurrentSession());
    }

    public void updateCurrentSession(String sessionName, Date endDatetime) {
        Log.d(TAG + " updateCurrentSession", "entered");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        LocationSession currentSession = locationMaintainer.getCurrentSession();
        currentSession.setEndDatetime(DateUtils.getCurrentDatetime());
        if(sessionName != null)
            currentSession.setSessionName(sessionName);
        if(endDatetime != null)
            currentSession.setEndDatetime(endDatetime);
        LocationDb.release();
        LocationDataAccessService.startActionUpdateSession(context, currentSession);
    }

    public void updateSession(LocationSession session, String sessionName, Date endDatetime) {
        Log.d(TAG + " updateSession", "entered");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        LocationSession storedSession = locationMaintainer.getLocationSessionMatchingProvided(session);
        storedSession.setEndDatetime(DateUtils.getCurrentDatetime());
        if(sessionName != null)
            storedSession.setSessionName(sessionName);
        if(endDatetime != null)
            storedSession.setEndDatetime(endDatetime);
        LocationDb.release();
        LocationDataAccessService.startActionUpdateSession(context, storedSession);
    }

    public LiveData<LocationMaintainer> getLocationMaintainerLD(int userId) {
        Log.d(TAG + " getLocationMaintainer", "with dao entered");
        LocationDataAccessService.startActionGetLocationSessions(context, userId);
        return locationDb.getData();
    }

    public LiveData<LocationMaintainer> getLocationMaintainerLD() {
        Log.d(TAG + " getLocationMaintainer", "entered");
        return locationDb.getData();
    }

    public void bindPhotoAsTagToLastSessionPoint(String photoFilePath) {
        Log.d(TAG, "bindPhotoAsTagToLastSessionPoint(): " + photoFilePath);
        LocationDataAccessService.startActionBindPhotoTagWithLastPoint(context, photoFilePath);
    }

}
