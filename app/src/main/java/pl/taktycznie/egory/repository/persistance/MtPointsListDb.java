package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.MountainPoint;

public class MtPointsListDb extends AbstractListDb<MountainPoint> {

    private static MtPointsListDb instance = null;

    synchronized public static MtPointsListDb getInstance() {
        if (instance == null)
            instance = new MtPointsListDb();
        return instance;
    }

}
