package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.MountainPointDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainPoint;
import pl.taktycznie.egory.repository.persistance.MtPointsListDb;

public class MtPointRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + RangesRepository.class.getSimpleName();

    private MountainPointDao _pointsDao;
    private MtPointsListDb _pointsDb = MtPointsListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public MtPointRepository(MountainPointDao pointsDao){
        _pointsDao = pointsDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError(){
        return error;
    }

    public LiveData<List<MountainPoint>> getAllMountainPoints(){
        _pointsDao.getAllMountainPoints(resource -> {
            if (resource.isSuccessful()) {
                _pointsDb.setDataValue( resource.getData());
                error.setValue(null);
            } else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));

        return _pointsDb.getData();
    }

    public LiveData<List<List<MountainPoint>>> getSpecificMountainPoints2(){//public LiveData<List<MountainPoint>> getSpecificMountainPoints()
        _pointsDao.getSpecificMountainPoints2(resource -> {
            if (resource.isSuccessful()) {
                _pointsDb.setDataValue2(resource.getData());
                error.setValue(null);
            } else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));

        return _pointsDb.getData2();
    }

    public LiveData<List<MountainPoint>> getSpecificMountainPoints(){//public LiveData<List<MountainPoint>> getSpecificMountainPoints()
        _pointsDao.getSpecificMountainPoints(resource -> {
            if (resource.isSuccessful()) {
                _pointsDb.setDataValue(resource.getData());
                error.setValue(null);
            } else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));

        return _pointsDb.getData();
    }





}
