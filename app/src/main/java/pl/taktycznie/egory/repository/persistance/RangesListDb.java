package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.MountainRange;

public final class RangesListDb extends AbstractListDb<MountainRange> {

    private static RangesListDb instance = null;

    synchronized public static RangesListDb getInstance() {
        if(instance == null)
            instance = new RangesListDb();
        return instance;
    }

}
