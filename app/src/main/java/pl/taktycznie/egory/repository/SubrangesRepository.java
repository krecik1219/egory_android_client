package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.MountainSubrangeDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainSubrange;
import pl.taktycznie.egory.repository.persistance.SubrangesListDb;

public class SubrangesRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + SubrangesRepository.class.getSimpleName();

    private MountainSubrangeDao _subrangeDao;
    private SubrangesListDb _subrangesDb = SubrangesListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public SubrangesRepository(MountainSubrangeDao subrangeDao){
        _subrangeDao = subrangeDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError(){
        return error;
    }

    public LiveData<List<MountainSubrange>> getMountainSubrangesByRange(int rangeId){
        _subrangeDao.getMountainSubrangesByRange( rangeId, resource -> {
            if (resource.isSuccessful()) {
                _subrangesDb.setDataValue(resource.getData());
                error.setValue(null);
            } else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));

        return _subrangesDb.getData();
    }

}
