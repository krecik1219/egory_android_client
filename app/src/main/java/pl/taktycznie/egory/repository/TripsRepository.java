package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.TripDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.repository.persistance.TripsListDb;

public class TripsRepository {
    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripsRepository.class.getSimpleName();

    private TripDao tripDao;
    private TripsListDb tripsDb = TripsListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public TripsRepository(TripDao tripDao) {
        this.tripDao = tripDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public LiveData<List<Trip>> getUserTrips(int userId) {
        tripDao.getTripsByUser(userId, resource -> {
            if(resource.isSuccessful()) {
                tripsDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return tripsDb.getData();
    }

    public void addTrip(Trip trip, int userId) {
        tripDao.insertTripToDb(trip, userId, resource -> {
            if(resource.isSuccessful()) {
                trip.setTripId(resource.getData());
                tripsDb.addSingleValueInFront(trip);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void editTrip(Trip trip, String tripName) {
        tripDao.updateTrip(trip, tripName, resource -> {
            if(resource.isSuccessful()) {
                trip.setTripName(tripName);
                tripsDb.editNotify();
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void removeTrip(Trip trip) {
        tripDao.deleteTrip(trip, resource -> {
            if(resource.isSuccessful()) {
                tripsDb.removeSingleValue(trip);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }
}
