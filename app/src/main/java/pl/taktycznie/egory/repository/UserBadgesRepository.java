package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.List;

import pl.taktycznie.egory.dao.BadgeDao;
import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.GotBadge;
import pl.taktycznie.egory.repository.persistance.UserBadgesDb;

public class UserBadgesRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + UserBadgesRepository.class.getSimpleName();

    private BadgeDao badgeDao;
    private UserBadgesDb userBadgesDb = UserBadgesDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public UserBadgesRepository(BadgeDao badgeDao) {
        this.badgeDao = badgeDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public LiveData<List<GotBadge>> getUserBadgesPaths(int userId) {
        badgeDao.getBadgesByUser(userId, resource -> {
            if(resource.isSuccessful()) {
                userBadgesDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return userBadgesDb.getData();
    }

    public void addUserBadge(GotBadge badge, int userId) {
        badgeDao.insertUserBadgeToDb(badge, userId, resource -> {
            if(resource.isSuccessful()) {
                // award date should be set before inserting to db via dao
                userBadgesDb.addSingleValue(badge);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }
}
