package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.repository.persistance.GotPathsListDb;

public class GotPathsRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + GotPathsRepository.class.getSimpleName();

    private GotPathDao gotPathDao;
    private GotPathsListDb gotPathsDb = GotPathsListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public GotPathsRepository(GotPathDao gotPathDao) {
        this.gotPathDao = gotPathDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public LiveData<List<GotPath>> getGotPathsBySubrange(int subrangeId) {
        gotPathDao.getGotPathsBySubrange(subrangeId, resource -> {
            if(resource.isSuccessful()) {
                gotPathsDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return gotPathsDb.getData();
    }

    public LiveData<List<GotPath>> getGotPathsByStartPoint(String startPointName) {
        gotPathDao.getGotPathsByStartPoint(startPointName, resource -> {
            if(resource.isSuccessful()) {
                gotPathsDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return gotPathsDb.getData();
    }

    public LiveData<List<GotPath>> getGotPathsByEndPoint(String endPointName) {
        gotPathDao.getGotPathsByEndPoint(endPointName, resource -> {
            if(resource.isSuccessful()) {
                gotPathsDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return gotPathsDb.getData();
    }

    public void addGotPath(GotPath gotPath) {
        gotPathDao.insertGotPath(gotPath, resource -> {
            if(resource.isSuccessful()) {
                gotPath.setPathId(resource.getData());
                gotPathsDb.addSingleValue(gotPath);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void editGotPath(GotPath gotPath, int upPoints, int downPoints) {
        gotPathDao.updateGotPath(gotPath, upPoints, downPoints, resource -> {
            if(resource.isSuccessful()) {
                gotPath.setUpPoints(upPoints);
                gotPath.setDownPoints(downPoints);
                gotPathsDb.editNotify();
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }

    public void removeTripPath(GotPath gotPath) {
        gotPathDao.deleteGotPath(gotPath, resource -> {
            if(resource.isSuccessful()) {
                gotPathsDb.removeSingleValue(gotPath);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }
}
