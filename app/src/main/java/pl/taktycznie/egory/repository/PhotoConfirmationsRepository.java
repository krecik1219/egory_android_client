package pl.taktycznie.egory.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.PhotoConfirmationDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.PhotoConfirmation;
import pl.taktycznie.egory.repository.persistance.PhotoConfirmationsListDb;

public class PhotoConfirmationsRepository {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + PhotoConfirmationsRepository.class.getSimpleName();

    private PhotoConfirmationDao photoConfirmationDao;
    private PhotoConfirmationsListDb tripsDb = PhotoConfirmationsListDb.getInstance();
    private MutableLiveData<DataAccessError> error;

    public PhotoConfirmationsRepository(PhotoConfirmationDao photoConfirmationDao) {
        this.photoConfirmationDao = photoConfirmationDao;
        this.error = new MutableLiveData<>();
    }

    public LiveData<DataAccessError> getError() {
        return error;
    }

    public LiveData<List<PhotoConfirmation>> getPhotoConfirmations(int tripPathId) {
        photoConfirmationDao.getPhotoConfirmationsByTripPath(tripPathId, resource -> {
            if(resource.isSuccessful()) {
                tripsDb.setDataValue(resource.getData());
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
        return tripsDb.getData();
    }

    public void addPhotoConfirmation(PhotoConfirmation photoConfirmation) {
        photoConfirmationDao.insertPhotoConfirmationToDb(photoConfirmation, resource -> {
            if(resource.isSuccessful()) {
                photoConfirmation.setPhotoConfirmationId(resource.getData());
                tripsDb.addSingleValue(photoConfirmation);
                error.setValue(null);
            }
            else
                error.setValue(resource.getError());
        }, dataAccessError -> error.setValue(dataAccessError));
    }
}
