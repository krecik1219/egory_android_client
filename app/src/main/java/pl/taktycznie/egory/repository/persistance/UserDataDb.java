package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.user.User;

public class UserDataDb extends AbstractObjectDb<User> {

    private static UserDataDb instance = null;

    private UserDataDb() {
        super(User.class);
    }

    synchronized public static UserDataDb getInstance() {
        if(instance == null)
            instance = new UserDataDb();
        return instance;
    }
}
