package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.PhotoConfirmation;

public final class PhotoConfirmationsListDb extends AbstractListDb<PhotoConfirmation> {

    private static PhotoConfirmationsListDb instance = null;

    synchronized public static PhotoConfirmationsListDb getInstance() {
        if(instance == null)
            instance = new PhotoConfirmationsListDb();
        return instance;
    }
}
