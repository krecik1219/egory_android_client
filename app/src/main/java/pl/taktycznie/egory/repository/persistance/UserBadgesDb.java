package pl.taktycznie.egory.repository.persistance;

import pl.taktycznie.egory.model.GotBadge;

public final class UserBadgesDb extends AbstractListDb<GotBadge> {

    private static UserBadgesDb instance = null;

    synchronized public static UserBadgesDb getInstance() {
        if(instance == null)
            instance = new UserBadgesDb();
        return instance;
    }
}
