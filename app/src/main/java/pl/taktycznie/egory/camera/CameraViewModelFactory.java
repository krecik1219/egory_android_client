package pl.taktycznie.egory.camera;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import pl.taktycznie.egory.repository.LocationRepository;

public class CameraViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private LocationRepository locationRepository;

    public CameraViewModelFactory(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CameraViewModel(locationRepository);
    }
}
