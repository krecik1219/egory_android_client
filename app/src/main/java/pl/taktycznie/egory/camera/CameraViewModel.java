package pl.taktycznie.egory.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.lifecycle.ViewModel;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.repository.LocationRepository;
import pl.taktycznie.egory.repository.persistance.LocationDb;


public class CameraViewModel extends ViewModel {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + CameraViewModel.class.getSimpleName();

    private int userId;
    private LocationRepository locationRepository;

    public CameraViewModel(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public void init(int userId) {
        this.userId = userId;
    }

    public void bindPhotoAsTagToLastSessionPoint(String photoFilePath) {
        Log.d(TAG, "bindPhotoAsTagToLastSessionPoint(): " + photoFilePath);
        LocationDb.lock("getLocationMaintainerLD");
        locationRepository.bindPhotoAsTagToLastSessionPoint(photoFilePath);
        LocationDb.release("getLocationMaintainerLD");
    }

}
