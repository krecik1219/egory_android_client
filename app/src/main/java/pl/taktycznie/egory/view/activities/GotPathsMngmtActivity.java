package pl.taktycznie.egory.view.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.MountainRangeDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainRange;
import pl.taktycznie.egory.repository.RangesRepository;
import pl.taktycznie.egory.view.adapters.RangeListAdapter;
import pl.taktycznie.egory.viewmodel.RangesViewModel;
import pl.taktycznie.egory.viewmodel.factories.RangesViewModelFactory;


public class GotPathsMngmtActivity extends CoreActivity {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MainActivity.class.getSimpleName();

    private RecyclerView _recyclerView;
    private RangeListAdapter _adapter;

    private List<MountainRange> _rangesList;
    private RangesViewModel _rangesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_got_paths_mngmt);

        initView();
        makeViewModelBinding();
    }

    private void initView() {
        _rangesList = new ArrayList<>();
        _recyclerView = findViewById(R.id.ranges_recycler_view);
        _recyclerView.setHasFixedSize(true);
        _recyclerView.setLayoutManager(new LinearLayoutManager(this));
        _adapter = new RangeListAdapter(_rangesList, this);
        _recyclerView.setAdapter(_adapter);
    }

    private void makeViewModelBinding() {
        MountainRangeDao rangesDao = new MountainRangeDao(this);
        RangesRepository rangesRepository = new RangesRepository(rangesDao);
        ViewModelProvider.Factory factory = new RangesViewModelFactory(rangesRepository);
        _rangesViewModel = ViewModelProviders.of(this, factory).get(RangesViewModel.class);
        _rangesViewModel.init();
        _rangesViewModel.getMountainRanges().observe(
                this,
                mountainRanges -> refreshUI(_rangesList, mountainRanges, _adapter));
        _rangesViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }
}
