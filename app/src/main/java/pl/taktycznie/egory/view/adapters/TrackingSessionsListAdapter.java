package pl.taktycznie.egory.view.adapters;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.helpers.BoolPaired;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.utils.DateUtils;

public class TrackingSessionsListAdapter extends RecyclerView.Adapter<TrackingSessionsListAdapter.ViewHolder> {

    private OnTrackingSessionClickListener onTrackingSessionClickListener;
    private List<BoolPaired<LocationSession>> locationSessions;

    public TrackingSessionsListAdapter(@NonNull OnTrackingSessionClickListener onTrackingSessionClickListener,
                                       @NonNull List<BoolPaired<LocationSession>> locationSessions) {
        this.onTrackingSessionClickListener = onTrackingSessionClickListener;
        this.locationSessions = locationSessions;
    }

    @NonNull
    @Override
    public TrackingSessionsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_rv_tracking_sessions_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrackingSessionsListAdapter.ViewHolder holder, int position) {
        BoolPaired<LocationSession> item = locationSessions.get(position);
        LocationSession locationSession = item.getItem();
        boolean flag = item.flag();
        holder.tvTrackingSessionName.setText(locationSession.getSessionName());
        if(locationSession.getStartDatetime() != null)
            holder.tvTrackingSessionStartDatetime.setText(DateUtils.datetimeToString(locationSession.getStartDatetime()));
        else
            holder.tvTrackingSessionStartDatetime.setText("");
        if(locationSession.getEndDatetime() != null)
            holder.tvTrackingSessionEndDatetime.setText(DateUtils.datetimeToString(locationSession.getEndDatetime()));
        else
            holder.tvTrackingSessionEndDatetime.setText("");
        Log.d("adapter", "flag? " + flag + " ; item=" + item);
        if(flag) {
            holder.cbTrackingSessionCheck.setChecked(true);
        }
        else {
            holder.cbTrackingSessionCheck.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return locationSessions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ConstraintLayout clTrackingSessionListItemLayout;
        private TextView tvTrackingSessionName;
        private TextView tvTrackingSessionStartDatetime;
        private TextView tvTrackingSessionEndDatetime;
        private AppCompatCheckBox cbTrackingSessionCheck;

        public ViewHolder(View itemView) {
            super(itemView);
            clTrackingSessionListItemLayout = itemView.findViewById(R.id.cl_tracking_session_list_item_layout);
            tvTrackingSessionName = itemView.findViewById(R.id.tv_tracking_session_name);
            tvTrackingSessionStartDatetime = itemView.findViewById(R.id.tv_tracking_session_start_datetime);
            tvTrackingSessionEndDatetime = itemView.findViewById(R.id.tv_tracking_session_end_datetime);
            cbTrackingSessionCheck = itemView.findViewById(R.id.cb_tracking_session_check);
            cbTrackingSessionCheck.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onTrackingSessionClickListener.onTrackingSessionClick(cbTrackingSessionCheck, locationSessions.get(getAdapterPosition()));
        }
    }

    public interface OnTrackingSessionClickListener {
        void onTrackingSessionClick(CheckBox v, BoolPaired<LocationSession> locationSession);
    }
}
