package pl.taktycznie.egory.view.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.GotPath;

public class GotPathsByPointListAdapter extends RecyclerView.Adapter<GotPathsByPointListAdapter.ViewHolder> {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + GotPathsByPointListAdapter.class.getSimpleName();

    public List<GotPath> gotPathsByPoint;
    private OnGotPathByPointClickListener onGotPathByPointClickListener;



    public GotPathsByPointListAdapter(@NonNull List<GotPath> gotPathsByPoint)
    {
        this.gotPathsByPoint = gotPathsByPoint;
    }

                                      public GotPathsByPointListAdapter(@NonNull List<GotPath> gotPathsByPoint, @NonNull OnGotPathByPointClickListener onGotPathByPointClickListener) {
        this.gotPathsByPoint = gotPathsByPoint;
        this.onGotPathByPointClickListener = onGotPathByPointClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.got_path_by_point_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GotPath gotPathByPoint = gotPathsByPoint.get(position);
        holder.tvPathByPointStartPoint.setText(gotPathByPoint.getStartPoint().getPointName());
        holder.tvPathByPointStartPointSubrange
                .setText(String.format("(%s)", gotPathByPoint.getStartPoint().getSubrange().getSubrangeName()));
        holder.tvPathByPointEndPoint.setText(gotPathByPoint.getEndPoint().getPointName());
        holder.tvPathByPointEndPointSubrange
                .setText(String.format("(%s)", gotPathByPoint.getEndPoint().getSubrange().getSubrangeName()));
        holder.tvPathByPointPoints.setText(String.format(Locale.ENGLISH, "%d ", gotPathByPoint.getUpPoints()));
        //holder.tvPathByPointPoints.setText(String.format(Locale.ENGLISH, "%d / %d", gotPathByPoint.getUpPoints(), gotPathByPoint.getDownPoints()));
    }

    @Override
    public int getItemCount() {
        return gotPathsByPoint.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvPathByPointStartPoint;
        private TextView tvPathByPointStartPointSubrange;
        private TextView tvPathByPointEndPoint;
        private TextView tvPathByPointEndPointSubrange;
        private TextView tvPathByPointPoints;

        ViewHolder(View itemView) {
            super(itemView);
            tvPathByPointStartPoint = itemView.findViewById(R.id.tv_path_by_point_start_point);
            tvPathByPointStartPointSubrange = itemView.findViewById(R.id.tv_path_by_point_start_point_subrange);
            tvPathByPointEndPoint = itemView.findViewById(R.id.tv_path_by_point_end_point);
            tvPathByPointEndPointSubrange = itemView.findViewById(R.id.tv_path_by_point_end_point_subrange);
            tvPathByPointPoints = itemView.findViewById(R.id.tv_path_by_point_points);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onGotPathByPointClickListener.onGotPathByPointClick(gotPathsByPoint.get(getAdapterPosition()));
        }
    }

    public interface OnGotPathByPointClickListener {
        void onGotPathByPointClick(GotPath gotPathByPoint);
    }
}
