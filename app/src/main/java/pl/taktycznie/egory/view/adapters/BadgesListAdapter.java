package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.model.GotBadge;

public class BadgesListAdapter extends RecyclerView.Adapter<BadgesListAdapter.BadgesListHolder> {

    List<GotBadge> _badges;


    public BadgesListAdapter(List<GotBadge> badges, Context ctx){
        _badges = badges;
    }


    @NonNull
    @Override
    public BadgesListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView  = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.badge_list_item, parent, false);

       return new BadgesListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BadgesListHolder holder, int position) {

        GotBadge currentBadge = _badges.get(position);
        int points = 0;
        for (int i = 0; i <= position; i++) {
            points += _badges.get(i).getRequiredPoints();
        }
        holder.textViewBadgeName.setText(currentBadge.getBadgeName());
        holder.textViewBadgePoints.setText(String.valueOf(points));
        setImage(currentBadge.getBadgeId(), holder.imageViewBadgePic);
        //Picasso.get().load(R.drawable.badge_popularna).into(holder.imageViewBadgePic);

    }

    private void setImage(int badgeId, ImageView imageViewBadgePic) {
        switch (badgeId){
            case 1:
                Picasso.get().load(R.drawable.badge_w_gory_brazowa).into(imageViewBadgePic);
                break;
            case 2:
                Picasso.get().load(R.drawable.badge_w_gory_srebrna).into(imageViewBadgePic);
                break;
            case 3:
                Picasso.get().load(R.drawable.badge_w_gory_zlota).into(imageViewBadgePic);
                break;
            case 4:
                Picasso.get().load(R.drawable.badge_popularna).into(imageViewBadgePic);
                break;
            case 5:
                Picasso.get().load(R.drawable.badge_mala_brazowa).into(imageViewBadgePic);
                break;
            case 6:
                Picasso.get().load(R.drawable.badge_mala_srebrna).into(imageViewBadgePic);
                break;
            case 7:
                Picasso.get().load(R.drawable.badge_mala_zlota).into(imageViewBadgePic);
                break;
            case 8:
                Picasso.get().load(R.drawable.badge_duza_brazowa).into(imageViewBadgePic);
                break;
            case 9:
                Picasso.get().load(R.drawable.badge_duza_srebrna).into(imageViewBadgePic);
                break;
            case 10:
                Picasso.get().load(R.drawable.badge_duza_zlota).into(imageViewBadgePic);
                break;
            case 11:
                Picasso.get().load(R.drawable.badge_za_wytrwalosc).into(imageViewBadgePic);
                break;
            case 12:
                Picasso.get().load(R.drawable.badge_przodownik).into(imageViewBadgePic);
                break;
            case 13:
                Picasso.get().load(R.drawable.badge_honorowy_przodownik).into(imageViewBadgePic);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return _badges.size();
    }

    public class BadgesListHolder extends RecyclerView.ViewHolder{

        private TextView textViewBadgeName;
        private TextView textViewBadgePoints;
        private ImageView imageViewBadgePic;
        private ConstraintLayout container;

        public BadgesListHolder(View itemView) {
            super(itemView);
            textViewBadgeName = itemView.findViewById(R.id.badge_list_item__name_text_view);
            textViewBadgePoints = itemView.findViewById(R.id.badge_list_item__points_text_view);
            imageViewBadgePic = itemView.findViewById(R.id.badge_list_item__pic_image_view);
            //container = itemView.findViewById(R.id.badge_list_item_container);
        }

    }



}
