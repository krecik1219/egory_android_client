package pl.taktycznie.egory.view.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.PhotoConfirmationDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.PathStatus;
import pl.taktycznie.egory.model.PhotoConfirmation;
import pl.taktycznie.egory.repository.PhotoConfirmationsRepository;
import pl.taktycznie.egory.view.adapters.PhotoConfirmationListAdapter;
import pl.taktycznie.egory.viewmodel.PhotoConfirmationsViewModel;
import pl.taktycznie.egory.viewmodel.factories.PhotoConfirmationsViewModelFactory;

public class TripPathDetailsActivity extends CoreActivity implements PhotoConfirmationListAdapter.OnPhotoClickListener {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripPathDetailsActivity.class.getSimpleName();

    private static final int LOAD_IMAGE_REQ = 1;

    int tripPathdId;
    private String startPointName;
    private String startPointSubrangeName;
    private String endPointName;
    private String endPointSubrangeName;
    private int upPoints;
    private int downPoints;
    private PathStatus pathStatus;
    private boolean isDone;

    private PhotoConfirmationListAdapter photoConfirmationListAdapter;
    private List<PhotoConfirmation> photoConfirmations = new ArrayList<>();
    private PhotoConfirmationsViewModel photoConfirmationsViewModel;

    private RecyclerView rvPhotoConfirmations;
    private TextView tvStartPoint;
    private TextView tvStartPointSubrange;
    private TextView tvEndPoint;
    private TextView tvEndPointSubrange;
    private TextView tvPoints;
    private TextView tvPathStatus;
    private TextView tvIsDone;
    private ImageButton ibAddPhotoConfirmation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_path_details);

        Intent intent = getIntent();
        tripPathdId = intent.getIntExtra("tripPathId", 0);
        startPointName = intent.getStringExtra("startPointName");
        startPointSubrangeName = intent.getStringExtra("startPointSubrangeName");
        endPointName = intent.getStringExtra("endPointName");
        endPointSubrangeName = intent.getStringExtra("endPointSubrangeName");
        upPoints = intent.getIntExtra("upPoints", 0);
        downPoints = intent.getIntExtra("downPoints", 0);
        pathStatus = (PathStatus) intent.getSerializableExtra("pathStatus");
        isDone = intent.getBooleanExtra("isDone", false);

        initViews();
        makeMainViewModelBinding();
    }

    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case LOAD_IMAGE_REQ:
                handleLoadImageReqResult(resultCode, data);
                break;
        }
    }

    private void initViews() {
        initDetails();
        initPhotoConfirmationsRecyclerView();
        ibAddPhotoConfirmation = findViewById(R.id.ib_add_photo_confirmation);
        ibAddPhotoConfirmation.setOnClickListener(v -> pickPhoto());
    }

    private void initDetails() {
        tvStartPoint = findViewById(R.id.tv_trip_path_details_start_point);
        tvStartPointSubrange = findViewById(R.id.tv_trip_path_details_start_point_subrange);
        tvEndPoint = findViewById(R.id.tv_trip_path_details_end_point);
        tvEndPointSubrange = findViewById(R.id.tv_trip_path_details_end_point_subrange);
        tvPoints = findViewById(R.id.tv_trip_path_details_points);
        tvPathStatus = findViewById(R.id.tv_trip_path_details_status);
        tvIsDone = findViewById(R.id.tv_trip_path_details_is_done);

        tvStartPoint.setText(startPointName);
        tvStartPointSubrange.setText(startPointSubrangeName);
        tvEndPoint.setText(endPointName);
        tvEndPointSubrange.setText(endPointSubrangeName);
        tvPoints.setText(String.format(Locale.ENGLISH, "%d / %d", upPoints, downPoints));
        tvPathStatus.setText(pathStatus.toString());
        tvIsDone.setText(isDone? "Done" : "Not done");
    }

    private void initPhotoConfirmationsRecyclerView() {
        rvPhotoConfirmations = findViewById(R.id.rv_confirmations);
        rvPhotoConfirmations.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        photoConfirmationListAdapter = new PhotoConfirmationListAdapter(photoConfirmations, this);
        rvPhotoConfirmations.setAdapter(photoConfirmationListAdapter);
    }

    private void makeMainViewModelBinding() {
        PhotoConfirmationDao photoConfirmationDao = new PhotoConfirmationDao(this);
        PhotoConfirmationsRepository photoConfirmationsRepository = new PhotoConfirmationsRepository(photoConfirmationDao);
        ViewModelProvider.Factory factory = new PhotoConfirmationsViewModelFactory(photoConfirmationsRepository);
        photoConfirmationsViewModel = ViewModelProviders.of(this, factory).get(PhotoConfirmationsViewModel.class);
        photoConfirmationsViewModel.init(tripPathdId);
        photoConfirmationsViewModel.getPhotoConfirmations().observe(
                this,
                updatedPhotoConfirmations -> refreshUI(
                        photoConfirmations,
                        updatedPhotoConfirmations,
                        photoConfirmationListAdapter
                )
        );
        photoConfirmationsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void pickPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, LOAD_IMAGE_REQ);
    }

    private void handleLoadImageReqResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                if(imageUri != null) {
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    addPhotoConfirmationAction(selectedImage);
                }
                else
                    Toast.makeText(this, "Something went wrong",Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
        else {
            Toast.makeText(this, "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    private void addPhotoConfirmationAction(Bitmap image) {
        photoConfirmationsViewModel.addPhotoConfirmation(image);
    }

    @Override
    public void onPhotoClick(ImageView photo) {
        //TODO
    }
}
