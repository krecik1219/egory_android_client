package pl.taktycznie.egory.view.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.dao.MountainPointDao;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.model.MountainPoint;
import pl.taktycznie.egory.repository.GotPathsRepository;
import pl.taktycznie.egory.repository.MtPointRepository;
import pl.taktycznie.egory.viewmodel.GotPathsViewModel;
import pl.taktycznie.egory.viewmodel.MountainPointsViewModel;
import pl.taktycznie.egory.viewmodel.factories.GotPathsViewModelFactory;
import pl.taktycznie.egory.viewmodel.factories.MountainPointsViewModelFactory;

public class AddPathActivity extends CoreActivity {

    private static final String TAG = AddPathActivity.class.getSimpleName();

    private String TOAST_ACCEPT_TEXT = "PATH ADDED";
    private String TOAST_CANCEL_TEXT = "ADDING CANCELED";
    private String TOAST_ADDING_ERROR_TEXT = "START POINT AND END POINT MUST ME DIFFERENT";

    private Context _context;
    private int _gotPathId;
    private int _subrangeId;

    ArrayAdapter<String> _adapterSpinnerPointsName;
    private MountainPointsViewModel _mountainPointsViewModel;
    private GotPathsViewModel _gotPathsViewModel;
    private List<MountainPoint> _mtPointsList;
    private List<String> _mtPointsNameList;
    //private Spinner _spinnerColorsNumber;
    //private Spinner _spinnerColorsName;
    private Spinner _spinnerStartPoint;
    private Spinner _spinnerEndPoint;
    private EditText editTextPointsUp;
    private EditText editTextPointsDn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_path);
        _context = this;

        _mtPointsList = new ArrayList<>();

        Intent intent = getIntent();
        _subrangeId = intent.getIntExtra("subrangeId", 0);

        initView();

        makeViewModelBinding();
        //setSpinnersForPoints();
        //setSpinnersForColor();

    }



    private void initView() {
        _spinnerStartPoint = (Spinner) findViewById(R.id.spinner_add_path_start_point);
        _spinnerEndPoint = (Spinner) findViewById(R.id.spinner_add_path_end_point);
        editTextPointsUp = (EditText) findViewById(R.id.editText_add_path_points_UP);
        editTextPointsDn = (EditText) findViewById(R.id.editText_add_path_points_DN);

        //_spinnerColorsName = (Spinner) findViewById(R.id.spinner_add_path_color_name);
        //_spinnerColorsNumber = (Spinner) findViewById(R.id.spinner_add_path_color_number);
    }

    private void setSpinnersForPoints(){
        Toast.makeText(this, String.valueOf(_mtPointsNameList.size()), Toast.LENGTH_SHORT).show();
        ArrayAdapter<String> _adapterSpinnerPointsName =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, _mtPointsNameList);
       _adapterSpinnerPointsName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //ArrayAdapter<CharSequence> adapterSpinnerColorNumber = ArrayAdapter.createFromResource(this, R.array.numbers_for__color_spinner, android.R.layout.simple_spinner_item);
        //adapterSpinnerColorName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _spinnerStartPoint.setAdapter(_adapterSpinnerPointsName);
        _spinnerEndPoint.setAdapter(_adapterSpinnerPointsName);
    }

//    private void setSpinnersForColor() {
//        ArrayAdapter<CharSequence> adapterSpinnerColorName = ArrayAdapter.createFromResource(this, R.array.colors_for_spinner, android.R.layout.simple_spinner_item);
//        adapterSpinnerColorName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        _spinnerColorsName.setAdapter(adapterSpinnerColorName);
//        ArrayAdapter<CharSequence> adapterSpinnerColorNumber = ArrayAdapter.createFromResource(this, R.array.numbers_for__color_spinner, android.R.layout.simple_spinner_item);
//        adapterSpinnerColorName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        _spinnerColorsNumber.setAdapter(adapterSpinnerColorNumber);
//    }

    public void addItemAcceptClick(View v){
        String startPointName = String.valueOf(_spinnerStartPoint.getSelectedItem());
        String endPointName = String.valueOf(_spinnerEndPoint.getSelectedItem());
        if (!startPointName.equals(endPointName)){
            addGotPath(startPointName, endPointName);
            this.finish();
        }
        else {
            Toast.makeText(this, TOAST_ADDING_ERROR_TEXT, Toast.LENGTH_SHORT).show();
        }

    }

    private void addGotPath(String startPointName, String endPointName){
        //String colorName = _spinnerColorsName.getSelectedItem().toString();
        String pointsUp = String.valueOf(editTextPointsUp.getText());
        String pointsDn = String.valueOf(editTextPointsDn.getText());

        MountainPoint startPoint = getMountainPointByName(startPointName);
        MountainPoint endPoint = getMountainPointByName(endPointName);

        GotPath newPath = new GotPath( startPoint, endPoint, Integer.valueOf(pointsUp), Integer.valueOf(pointsDn));
        Toast.makeText(this, TOAST_ACCEPT_TEXT, Toast.LENGTH_SHORT).show();
        _gotPathsViewModel.addGotPath(newPath);
    }

    public void addItemCancelClick(View v){
        Toast.makeText(this, TOAST_CANCEL_TEXT, Toast.LENGTH_SHORT).show();
        this.finish();
    }

    public void setMtPointsNameList(){

        _mtPointsNameList = new ArrayList<>();
        for(MountainPoint mp: _mtPointsList){
            _mtPointsNameList.add(mp.getPointName());
        }

    }

    private void makeViewModelBinding(){
        MountainPointDao pointDao = new MountainPointDao(this);
        MtPointRepository mtPointRepository = new MtPointRepository(pointDao);
        ViewModelProvider.Factory factory = new MountainPointsViewModelFactory(mtPointRepository);
        _mountainPointsViewModel = ViewModelProviders.of( this, factory).get(MountainPointsViewModel.class);
        //_mountainPointsViewModel.initWithSubrangeId(_subrangeId);
        _mountainPointsViewModel.init();
        _mountainPointsViewModel.getMountainPoints().observe(this, this::refreshUI);
        _mountainPointsViewModel.getError().observe(this, error->{
            if(error !=null)
                handleDataAccessErrorInfoInUI(error);
        });

        //_mtPointsList = _mountainPointsViewModel.getMoutainPoints();

        GotPathDao pathDao = new GotPathDao(this);
        GotPathsRepository pathsRepository = new GotPathsRepository(pathDao);
        ViewModelProvider.Factory factory_paths = new GotPathsViewModelFactory(pathsRepository);
        _gotPathsViewModel = ViewModelProviders.of( this, factory_paths).get(GotPathsViewModel.class);
        _gotPathsViewModel.initWithSubrangeId(_subrangeId);
        //_gotPathsList = _gotPathsViewModel.getGotPathsBySubrange();
        _gotPathsViewModel.getError().observe(this, error->{
            if(error !=null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void refreshUI(List<MountainPoint> gotPoints) {
        _mtPointsList.clear();
        List<MountainPoint> gotPointsFiltered = Stream.of(gotPoints)
                .filter( p -> p.getSubrange().getSubrangeId() == _subrangeId)
                .collect(Collectors.toList());
        _mtPointsList.addAll(gotPointsFiltered);
        Collections.sort(_mtPointsList, (MountainPoint mp1, MountainPoint mp2) -> mp1.getPointName().compareTo(mp2.getPointName()));
        setMtPointsNameList();
        setSpinnersForPoints();
        //_adapterSpinnerPointsName.notifyDataSetChanged();
    }

    private MountainPoint getMountainPointByName(String pointName) {

        MountainPoint mtpoint = Stream.of(_mtPointsList)
                .filter( mp -> mp.getPointName().equals(pointName))
                .findFirst()
                .get();
        return mtpoint;
    }


}
