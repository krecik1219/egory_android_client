package pl.taktycznie.egory.view.fragments.helpers;

public class RegisterParametersPack {

    public String login;
    public String name;
    public String surname;
    public String password1;
    public String password2;
    public String birthDate;

    public RegisterParametersPack(String login, String name, String surname, String password1, String password2, String birthDate) {
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.password1 = password1;
        this.password2 = password2;
        this.birthDate = birthDate;
    }
}
