package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.PathStatus;
import pl.taktycznie.egory.model.TripPath;

public class TripPathsListAdapter extends RecyclerView.Adapter<TripPathsListAdapter.ViewHolder> {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripPathsListAdapter.class.getSimpleName();

    private Context context;
    public List<TripPath> tripPaths;
    private OnTripPathClickListener onTripPathClickListener;
    private OnCheckDoneClick onCheckDoneClick;

    public TripPathsListAdapter(@NonNull Context context,
                                @NonNull List<TripPath> tripPaths,
                                @NonNull OnTripPathClickListener onTripPathClickListener,
                                @NonNull OnCheckDoneClick onCheckDoneClick) {
        this.context = context;
        this.tripPaths = tripPaths;
        this.onTripPathClickListener = onTripPathClickListener;
        this.onCheckDoneClick = onCheckDoneClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trip_path_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TripPath tripPath = tripPaths.get(position);
        holder.tvTripPathStartPoint.setText(tripPath.getStartPointName());
        holder.tvTripPathStartPointSubrange
                .setText(String.format("(%s)", tripPath.getStartPointSubrangeName()));
        holder.tvTripPathEndPoint.setText(tripPath.getEndPointName());
        holder.tvTripPathEndPointSubrange
                .setText(String.format("(%s)", tripPath.getEndPointSubrangeName()));
        holder.tvTripPathPoints.setText(String.format(Locale.ENGLISH, "%d ", tripPath.getUpPoints()));
        //  holder.tvTripPathPoints.setText(String.format(Locale.ENGLISH, "%d / %d", tripPath.getUpPoints(), tripPath.getDownPoints()));
        holder.clTripPathItemLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.background_item_color_black_border));
        holder.ibTripPathDoneSwitch.setImageResource(R.drawable.check_grey);
        if (tripPath.getPathStatus() == PathStatus.CONFIRMED) {
            holder.clTripPathItemLayout.setBackground(
                    ContextCompat.getDrawable(
                            context, R.drawable.background_item_verified_color_black_border));
        }
        if (tripPath.isDone())
            holder.ibTripPathDoneSwitch.setImageResource(R.drawable.check_green);
    }

    @Override
    public int getItemCount() {
        return tripPaths.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ConstraintLayout clTripPathItemLayout;
        private TextView tvTripPathStartPoint;
        private TextView tvTripPathStartPointSubrange;
        private TextView tvTripPathEndPoint;
        private TextView tvTripPathEndPointSubrange;
        private TextView tvTripPathPoints;
        private ImageButton ibTripPathDoneSwitch;

        ViewHolder(View itemView) {
            super(itemView);
            clTripPathItemLayout = itemView.findViewById(R.id.cl_trip_path_list_item_layout);
            tvTripPathStartPoint = itemView.findViewById(R.id.tv_trip_path_start_point);
            tvTripPathStartPointSubrange = itemView.findViewById(R.id.tv_trip_path_start_point_subrange);
            tvTripPathEndPoint = itemView.findViewById(R.id.tv_trip_path_end_point);
            tvTripPathEndPointSubrange = itemView.findViewById(R.id.tv_trip_path_end_point_subrange);
            tvTripPathPoints = itemView.findViewById(R.id.tv_trip_path_points);
            ibTripPathDoneSwitch = itemView.findViewById(R.id.ib_trip_path_switch_done);
            ibTripPathDoneSwitch.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG + "view on click: ", v.getClass().getName() + " ; id: " + v.getId());
            switch(v.getId()) {
                case R.id.ib_trip_path_switch_done:
                    onCheckDoneClick.onCheckDoneClick(tripPaths.get(getAdapterPosition()));
                    break;
                default:
                    onTripPathClickListener.onTripPathClick(tripPaths.get(getAdapterPosition()));
                    break;
            }
        }
    }

    public interface OnTripPathClickListener {
        void onTripPathClick(TripPath tripPath);
    }

    public interface OnCheckDoneClick {
        void onCheckDoneClick(TripPath tripPath);
    }
}
