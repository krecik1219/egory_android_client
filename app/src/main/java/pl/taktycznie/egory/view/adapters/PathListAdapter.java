package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.view.activities.AddPathActivity;
import pl.taktycznie.egory.view.activities.ModifyPathActivity;

public class PathListAdapter extends RecyclerView.Adapter<PathListAdapter.PathListHolder> {

    private List<GotPath> _paths; // = new ArrayList<>(); tego dopiero uzyjemy kiedy uzyjemy LiveData
    private Context _context;
    private boolean _isLeader;
    private OnPathClickListener _mListener;

    public PathListAdapter(List<GotPath> paths, Context context, boolean isLeader){
        _paths = paths;
        _context = context;
        _isLeader = isLeader;
    }

    @NonNull
    @Override
    public PathListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.path_list_item, parent, false);
        return new PathListHolder(itemView, _mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PathListHolder holder, int position) {

        GotPath currentPath = _paths.get(position);
        holder.textViewStartPoint.setText(currentPath.getStartPoint().getPointName());
        holder.textViewEndPoint.setText(currentPath.getEndPoint().getPointName());
        holder.textViewUpPoints.setText(String.valueOf(currentPath.getUpPoints()));
        holder.textViewDownPoints.setText(String.valueOf(currentPath.getDownPoints()));
        if (_isLeader){
            holder.buttonModify.setVisibility(View.VISIBLE);
            /*holder.buttonModify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(_context, AddPathActivity.class);
                    intent.putExtra("pathId", currentPath.getPathId());
                    _context.startActivity(intent);
                }
            });
            holder.buttonModify.setOnClickListener( (View view) -> {
                        Intent intent = new Intent(_context, ModifyPathActivity.class);
                        intent.putExtra("pathId", currentPath.getPathId());
                        intent.putExtra("subrangeId", currentPath.getStartPoint().getSubrange().getSubrangeId());
                        intent.putExtra("pathEndPointName", currentPath.getEndPoint().getPointName());
                        intent.putExtra("pathStartPointName", currentPath.getStartPoint().getPointName());
                        intent.putExtra("pathPointsUp", currentPath.getUpPoints());
                        intent.putExtra("pathPointsDown", currentPath.getDownPoints());
                        _context.startActivity(intent);
                    }
            ); */
        }
//        int color = -1;
//        if ( currentPath.getUpColor().getColorRgb() != null){
//            color = currentPath.getUpColor().getColorRgb();
//        }
//        holder.imageViewColor.setBackgroundColor(color);

    }

    @Override
    public int getItemCount() {
        return _paths.size();
    }

    public void set_paths(List<GotPath> pathsIn){
        _paths = pathsIn;
        notifyDataSetChanged();
    }

    protected class PathListHolder extends RecyclerView.ViewHolder{
        private TextView textViewStartPoint;
        private TextView textViewEndPoint;
        private TextView textViewPoints;
        private TextView textViewUpPoints;
        private TextView textViewDownPoints;
        private ImageView imageViewColor;
        private ImageButton buttonModify;


        public PathListHolder(View itemView, OnPathClickListener listener) {
            super(itemView);
            textViewStartPoint = itemView.findViewById(R.id.textView_startPoint);
            textViewEndPoint = itemView.findViewById(R.id.textView_endPoint);
            textViewUpPoints = itemView.findViewById(R.id.textView_points_up);
            textViewDownPoints = itemView.findViewById(R.id.textView_points_down);
            //imageViewColor = itemView.findViewById(R.id.imageView_path_item_color);
            buttonModify = itemView.findViewById(R.id.button_modify);

            buttonModify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( listener != null){
                        int position = getAdapterPosition();
                        if ( position != RecyclerView.NO_POSITION){
                            listener.onPathClick(position);
                        }
                    }
                }
            });
        }
    }


    public interface OnPathClickListener{
        void onPathClick(int position);
    }

    public void setOnPathClickListener(OnPathClickListener listener){
        _mListener = listener;
    }


}
