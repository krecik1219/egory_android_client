package pl.taktycznie.egory.view.activities;


import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.TripDao;
import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.repository.TripsRepository;
import pl.taktycznie.egory.view.adapters.TripsListAdapter;
import pl.taktycznie.egory.viewmodel.TripsViewModel;
import pl.taktycznie.egory.viewmodel.factories.TripsViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class TripsUserDefinedFragment extends Fragment implements TripsListAdapter.OnTripClickListener, TripsListAdapter.OnEditClickListener {
    private FloatingActionButton fabAddTrip;
    private RecyclerView rvTripsList;
    private TripsListAdapter tripsListAdapter;
    private List<Trip> tripsList = new ArrayList<>();
    private TripsViewModel tripsViewModel;
    View view;
    private int userId;
    public TripsUserDefinedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_trips_user_defined, container, false);
        Intent intent = getActivity().getIntent();
        userId = intent.getIntExtra("userId", 1);
        initViews();
        makeViewModelBinding();

        return view;
    }



    private void initViews() {
        initTripsListRecyclerView();
        initFloatingActionButton();
    }

    @Override
    public void onTripClick(Trip trip) {
        Intent intent = new Intent(this.getContext(), TripPathsActivity.class);
        intent.putExtra("tripId", trip.getTripId());
        intent.putExtra("tripName", trip.getTripName());
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onEditClick(Trip trip) {
        AlertDialog dialog = getEditTripDialog(trip);
        dialog.show();
    }

    private void initTripsListRecyclerView() {
        rvTripsList = view.findViewById(R.id.rv_trips_list);
        rvTripsList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        tripsListAdapter = new TripsListAdapter(this.getContext(), tripsList, this, this);  // not implemented yet, listeners will be necessary
        rvTripsList.setAdapter(tripsListAdapter);
        initSwipeRemoveCallback(this::removeTripAction, tripsList, rvTripsList);
    }

    private void initFloatingActionButton() {
        fabAddTrip = view.findViewById(R.id.fab_add_trip);
        fabAddTrip.setOnClickListener(view -> {
            AlertDialog dialog = getAddTripDialog();
            dialog.show();
        });
    }

    private void makeViewModelBinding() {
        TripDao tripDao = new TripDao(this.getContext());
        TripsRepository tripsRepository = new TripsRepository(tripDao);
        TripDataValidator validator = new TripDataValidator();
        ViewModelProvider.Factory factory = new TripsViewModelFactory(tripsRepository, validator);
        tripsViewModel = ViewModelProviders.of(this, factory).get(TripsViewModel.class);
        tripsViewModel.init(userId);
        tripsViewModel.getUserTrips().observe(this, trips -> refreshUI(tripsList, trips, tripsListAdapter));
        tripsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void addTripAction(String tripName) {
        Trip trip = new Trip(tripName);
        tripsViewModel.addTrip(trip);
    }

    private void editTripAction(Trip trip, String tripName) {
        tripsViewModel.editTrip(trip, tripName);
    }

    private void removeTripAction(Trip trip) {
        tripsViewModel.removeTrip(trip);
    }

    private AlertDialog getAddTripDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        View layout = getLayoutInflater().inflate(R.layout.dialog_add_edit_trip, null);
        EditText etTripName = layout.findViewById(R.id.et_trip_name);
        builder.setView(layout);
        AlertDialog dialog = builder.create();
        Button btnConfirmAddTrip = layout.findViewById(R.id.btn_confirm_add_edit_trip);
        btnConfirmAddTrip.setOnClickListener(view -> {
            addTripAction(etTripName.getText().toString());
            dialog.dismiss();
        });
        Button btnCancelAddTrip = layout.findViewById(R.id.btn_cancel_add_trip);
        btnCancelAddTrip.setOnClickListener(view -> dialog.dismiss());
        return dialog;
    }

    protected void handleDataAccessErrorInfoInUI(DataAccessError error) {
        Toast.makeText(this.getContext(), error.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    protected <Model> void refreshUI(List<Model> viewModelList, List<Model> updatedModelList, RecyclerView.Adapter adapter) {
        viewModelList.clear();
        viewModelList.addAll(updatedModelList);
        adapter.notifyDataSetChanged();
    }


    private AlertDialog getEditTripDialog(Trip trip) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        View layout = getLayoutInflater().inflate(R.layout.dialog_add_edit_trip, null);
        EditText etTripName = layout.findViewById(R.id.et_trip_name);
        etTripName.setText(trip.getTripName());
        builder.setView(layout);
        AlertDialog dialog = builder.create();
        Button btnConfirmAddEditTrip = layout.findViewById(R.id.btn_confirm_add_edit_trip);
        btnConfirmAddEditTrip.setOnClickListener(view -> {
            editTripAction(trip, etTripName.getText().toString());
            dialog.dismiss();
        });
        Button btnCancelAddTrip = layout.findViewById(R.id.btn_cancel_add_trip);
        btnCancelAddTrip.setOnClickListener(view -> dialog.dismiss());
        return dialog;
    }
    protected <Model> void initSwipeRemoveCallback(CoreActivity.RemoveAction<Model> removeActionCb, List<Model> modelList, RecyclerView rv)
    {
        ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
            {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
            {
                removeActionCb.removeAction(modelList.get(viewHolder.getAdapterPosition()));
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
            {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();
                    p.setColor(Color.RED);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.baseline_delete_white_24);
                    RectF background;
                    RectF icon_dest;
                    if(dX > 0){
                        background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                    } else {
                        background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                    }
                    c.drawRect(background,p);
                    c.drawBitmap(icon,null,icon_dest,p);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallBack);
        itemTouchHelper.attachToRecyclerView(rv);
    }
}