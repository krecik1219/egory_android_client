package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.model.TripStatus;

public class TripsListAdapter extends RecyclerView.Adapter<TripsListAdapter.ViewHolder> {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripsListAdapter.class.getSimpleName();

    private Context context;
    private List<Trip> trips;
    private OnTripClickListener onTripClickListener;
    private OnEditClickListener onEditClickListener;

    public TripsListAdapter(@NonNull Context context,
                            @NonNull List<Trip> trips,
                            @NonNull OnTripClickListener onTripClickListener,
                            @NonNull OnEditClickListener onEditClickListener) {
        this.context = context;
        this.trips = trips;
        this.onTripClickListener = onTripClickListener;
        this.onEditClickListener = onEditClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trips_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Trip trip = trips.get(position);
        String tripName = trip.getTripName();
        String tripCreationDate = trip.getCreationDateString();
        holder.tvTripName.setText(tripName);
        holder.tvTripCreationDate.setText(tripCreationDate);
        holder.clTripItemLayout.setBackground(
                ContextCompat.getDrawable(context, R.drawable.background_item_color_black_border));
        if (TripStatus.tripStatusFromInt(trip.getTripStatusId()) == TripStatus.VERIFIED) {
            holder.clTripItemLayout.setBackground(
                    ContextCompat.getDrawable(context, R.drawable.background_item_verified_color_black_border));
        }
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ConstraintLayout clTripItemLayout;
        private TextView tvTripName;
        private TextView tvTripCreationDate;
        private ImageButton ibEditTrip;

        ViewHolder(View itemView) {
            super(itemView);
            clTripItemLayout = itemView.findViewById(R.id.cl_trip_list_item_layout);
            tvTripName = itemView.findViewById(R.id.tv_trip_name);
            tvTripCreationDate = itemView.findViewById(R.id.tv_trip_creation_date);
            ibEditTrip = itemView.findViewById(R.id.ib_edit_trip);
            ibEditTrip.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ib_edit_trip:
                    onEditClickListener.onEditClick(trips.get(getAdapterPosition()));
                    break;
                default:
                    onTripClickListener.onTripClick(trips.get(getAdapterPosition()));
                    break;
            }
        }
    }

    public interface OnTripClickListener {
        void onTripClick(Trip trip);
    }

    public interface OnEditClickListener {
        void onEditClick(Trip trip);
    }
}
