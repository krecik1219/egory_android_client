package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainPoint;

public class MountainPointListAdapter extends RecyclerView.Adapter<MountainPointListAdapter.ViewHolder> {
    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MountainPointListAdapter.class.getSimpleName();
    private Context context;
    public List<MountainPoint> mountainPoints;
    public List<List<MountainPoint>> listsMountainPoints;

   /* public MountainPointListAdapter(Context context1,List<MountainPoint> list)
    {
        this.context=context1;
        mountainPoints=list;
    }*/

    public MountainPointListAdapter(Context context1,List<List<MountainPoint>>list)
    {
        this.context=context1;
        listsMountainPoints=list;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mountain_point_list_item, viewGroup, false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        MountainPoint point = mountainPoints.get(i);
        String name = point.getPointName();
        Double longitude = point.getLongitude();
        Double latitude = point.getLatitude();
        Double altitude = point.getAltitude();

        viewHolder.mPaltitude.setText(altitude.toString());
        viewHolder.mPlatitude.setText(latitude.toString());
        viewHolder.mPLongitude.setText(longitude.toString());
        viewHolder.mPName.setText(name);

    }

    @Override
    public int getItemCount() {
        return mountainPoints.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout clMPLayout;
        private TextView mPName;
        private TextView mPLongitude;
        private TextView mPlatitude;
        private TextView mPaltitude;

        public ViewHolder(View itemView) {
            super(itemView);
            mPName=itemView.findViewById(R.id.mountain_point_name);
            clMPLayout=itemView.findViewById(R.id.cl_mountain_point_list_item_layout);
            mPLongitude=itemView.findViewById(R.id.mountain_point_longitude);
            mPlatitude=itemView.findViewById(R.id.mountain_point_latitude);
            mPaltitude=itemView.findViewById(R.id.mountain_point_altitude);
        }

    }

}
