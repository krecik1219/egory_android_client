package pl.taktycznie.egory.view.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;

public class PhotoTagsListAdapter extends RecyclerView.Adapter<PhotoTagsListAdapter.ViewHolder> {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + PhotoTagsListAdapter.class.getSimpleName();

    private List<Bitmap> images;

    public PhotoTagsListAdapter(@NonNull List<Bitmap> images) {
        this.images = images;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_tags_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.ivPhotoTag.setImageBitmap(images.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivPhotoTag;

        ViewHolder(View itemView) {
            super(itemView);
            ivPhotoTag = itemView.findViewById(R.id.iv_photo_tag);
        }
    }
}
