package pl.taktycznie.egory.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.UserDataDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.user.User;
import pl.taktycznie.egory.repository.UserDataRepository;
import pl.taktycznie.egory.view.fragments.LoginFragment;
import pl.taktycznie.egory.view.fragments.RegisterFragment;
import pl.taktycznie.egory.view.fragments.helpers.LoginParametersPack;
import pl.taktycznie.egory.view.fragments.helpers.RegisterParametersPack;
import pl.taktycznie.egory.viewmodel.UserLoginRegisterViewModel;
import pl.taktycznie.egory.viewmodel.factories.UserLoginRegisterViewModelFactory;

public class LoginRegisterActivity extends AppCompatActivity
    implements LoginFragment.OnLoginFragmentActionListener, RegisterFragment.OnRegisterFragmentActionListener {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LoginRegisterActivity.class.getSimpleName();

    private UserLoginRegisterViewModel userLoginRegisterViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        initView();
        makeViewModelBindings();
        goToMainActivityIfUserLogged();
    }

    private void goToMainActivityIfUserLogged() {
        if(userLoginRegisterViewModel.logIfCached())
            goToMainActivity();
    }

    private void goToMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void initView() {
        TabLayout tabLayout = findViewById(R.id.tl_login_register);
        tabLayout.addTab(tabLayout.newTab().setText("Login"));
        tabLayout.addTab(tabLayout.newTab().setText("Register"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = findViewById(R.id.vp_login_register_fragments);
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    private void makeViewModelBindings() {
        UserDataDao userDataDao = new UserDataDao(this);
        UserDataRepository userDataRepository = new UserDataRepository(userDataDao);
        ViewModelProvider.Factory factory = new UserLoginRegisterViewModelFactory(getApplication(), userDataRepository);
        userLoginRegisterViewModel = ViewModelProviders.of(this, factory).get(UserLoginRegisterViewModel.class);
        userLoginRegisterViewModel.getUser().observe(this, this::handleUserChanges);
        userLoginRegisterViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    @Override
    public void onLoginFragmentAction(LoginParametersPack params) {
        userLoginRegisterViewModel.logUser(params);
    }

    @Override
    public void onRegisterFragmentAction(RegisterParametersPack params) {
        userLoginRegisterViewModel.registerUser(params);
    }

    private void handleUserChanges(User user) {
        Log.d(TAG, "handleUserChanges()");
        if(userLoginRegisterViewModel.isValidUser(user)) {
            goToMainActivity();
        }
        else {
            Log.d(TAG, "handleUserChanges user not valid:" + user);
        }
    }

    private void handleDataAccessErrorInfoInUI(DataAccessError error) {
        Log.d(TAG, "handleDataAccessErrorInfoInUI: " + error.getErrorMessage());
        Toast.makeText(this, "Wrong credentials", Toast.LENGTH_SHORT).show();
    }

    private static class TabsAdapter extends FragmentStatePagerAdapter {

        private int mNumOfTabs;

        private TabsAdapter(FragmentManager fm, int numOfTabs){
            super(fm);
            this.mNumOfTabs = numOfTabs;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public Fragment getItem(int position){
            switch (position){
                case 0:
                    return new LoginFragment();
                case 1:
                    return new RegisterFragment();
                default:
                    return null;
            }
        }
    }
}
