package pl.taktycznie.egory.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.TripDao;
import pl.taktycznie.egory.dao.TripPathDao;
import pl.taktycznie.egory.globals.UserDataSingleton;
import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.model.GotBadge;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.repository.TripsRepository;
import pl.taktycznie.egory.view.adapters.BadgesListAdapter;
import pl.taktycznie.egory.viewmodel.AchievementsViewModel;
import pl.taktycznie.egory.viewmodel.factories.AchievementsViewModelFactory;

public class AllBadgesActivity extends AppCompatActivity {

    private RecyclerView _badgesListView;
    private List<GotBadge> _badgesList;

    AchievementsViewModel achievementsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_badges);
        makeViewModelBinding();

        _badgesList = achievementsViewModel.getBadgesList();

        initViews();
    }

    private void initViews() {

        _badgesListView = findViewById(R.id.all_badged_recycler_view);
        _badgesListView.setHasFixedSize(true);
        _badgesListView.setLayoutManager(new LinearLayoutManager(this));

        BadgesListAdapter badgesAdapter = new BadgesListAdapter(_badgesList, this);
        _badgesListView.setAdapter(badgesAdapter);

    }

    private void makeViewModelBinding() {
        TripDao tripDao = new TripDao(this);
        TripsRepository tripsRepository = new TripsRepository(tripDao);
        TripDataValidator validator = new TripDataValidator();
        TripPathDao tripPathDao = new TripPathDao(this);
        TripPathsRepository tripPathsRepository = new TripPathsRepository(tripPathDao);

        ViewModelProvider.Factory factory = new AchievementsViewModelFactory(tripsRepository, tripPathsRepository, validator);
        achievementsViewModel = ViewModelProviders.of(this, factory).get(AchievementsViewModel.class);
        achievementsViewModel.initTrips(UserDataSingleton.getInstance().getUserId());
        achievementsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    protected void handleDataAccessErrorInfoInUI(DataAccessError error) {
        Toast.makeText(this, error.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }
}
