package pl.taktycznie.egory.view.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.dao.MountainPointDao;
import pl.taktycznie.egory.dao.TripPathDao;
import pl.taktycznie.egory.globals.GlobalConstants;

import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.model.MountainPoint;

import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.repository.GotPathsRepository;
import pl.taktycznie.egory.repository.MtPointRepository;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.view.adapters.GotPathsByPointListAdapter;
import pl.taktycznie.egory.view.adapters.MountainPointListAdapter;
import pl.taktycznie.egory.view.adapters.TripPathsListAdapter;

import pl.taktycznie.egory.view.adapters.TripsListAdapter;
import pl.taktycznie.egory.viewmodel.GotPathsByPointViewModel;
import pl.taktycznie.egory.viewmodel.MountainPointsViewModel;
import pl.taktycznie.egory.viewmodel.TripPathsViewModel;
import pl.taktycznie.egory.viewmodel.TripsViewModel;
import pl.taktycznie.egory.viewmodel.factories.GotPathsByPointViewModelFactory;
import pl.taktycznie.egory.viewmodel.factories.MountainPointsViewModelFactory;
import pl.taktycznie.egory.viewmodel.factories.TripPathsViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 */
// musisz zmienic w mountain point adapter,  pointDao, MtRepository i MtViewModel
public class TripsTrackingBasedFragment extends Fragment implements GotPathsByPointListAdapter.OnGotPathByPointClickListener,
        TripPathsListAdapter.OnTripPathClickListener,
        TripPathsListAdapter.OnCheckDoneClick,
        TripsListAdapter.OnTripClickListener,
        TripsListAdapter.OnEditClickListener{
    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripsTrackingBasedFragment.class.getSimpleName();

    //masz liste gotPath, teraz powinienes z tego zrobic liste TripPat
    List<LocationSession> list;
    private List<MountainPoint> mPList = new ArrayList<>();
    private List<List<MountainPoint>> listsmPList;
   // private List<TripPath> tPList = new ArrayList<>();
    private RecyclerView rvTrip;
    private MountainPointListAdapter mPListAdapter;
    View view;
    private int userId;
    private MountainPointsViewModel pointsViewModel;
    private GotPathsByPointListAdapter gotPathsByPointListAdapter;
    private GotPathsByPointViewModel gotPathViewModel;
    private GotPathsRepository gotPathsRepository;
    private GotPathDao gotPathDao;

    private TripsListAdapter tripsListAdapter;
    private List<Trip> tripsList = new ArrayList<>();
    private TripsViewModel tripsViewModel;


    private List<GotPath> gotPathsByPointList = new ArrayList<>();
  //  private List<GotPath> gotPathsList = new ArrayList<>();
  private List<List<GotPath>> gotPathsList = new ArrayList<>();
    private GotPathsByPointViewModel gotPathsByPointViewModel;
  //
  private List<List<TripPath>> tripPathsList = new ArrayList<>();

    private TripPathsListAdapter tripPathsListAdapter;
    private TripPathsViewModel tripPathsViewModel;
    Trip trip;

    public TripsTrackingBasedFragment() {
        // Required empty public constructor
    }


    //  @RequiresApi(api = Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        listsmPList = new ArrayList<>();
        view = inflater.inflate(R.layout.fragment_trips_tracking_based, container, false);
        Intent intent = getActivity().getIntent();
        userId = intent.getIntExtra("userId", 1);
        //list = locationSessionsImageProvider.getLocationSessionsImage();
        trip=new Trip("Location Trip");

        init();
        makeViewModelBinding();

        return view;

    }

    private void init() {

        initAdapters();
       // initTripsPaths();
        initTripsListRecyclerView();
    }

    private void initTripsListRecyclerView() {
        rvTrip = view.findViewById(R.id.rv_trip_paths);
        rvTrip.setLayoutManager(new LinearLayoutManager(this.getContext()));

    }
    /*private void initTripsListRecyclerView() {
        rvTripsList = view.findViewById(R.id.rv_trips_list);
        rvTripsList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        tripsListAdapter = new TripsListAdapter(this.getContext(), tripsList, this, this);  // not implemented yet, listeners will be necessary
        rvTripsList.setAdapter(tripsListAdapter);
       // initSwipeRemoveCallback(this::removeTripAction, tripsList, rvTripsList);
    }*/

    private void removeTripAction(Trip trip) {
        tripsViewModel.removeTrip(trip);
    }

    @Override
    public void onTripClick(Trip trip) {


       /* Intent intent = new Intent(getContext(),TripPathsActivity.class);
        intent.putExtra("tripId", trip.getTripId());
        intent.putExtra("tripName", trip.getTripName());
        intent.putExtra("userId", userId);

        startActivity(intent);*/
        tripPathsListAdapter = new TripPathsListAdapter(this.getContext(), trip.getTripPaths(), this, this);
          rvTrip.setAdapter(tripPathsListAdapter);

    }


    @Override
    public void onEditClick(Trip trip) {
        AlertDialog dialog = getEditTripDialog(trip);
        dialog.show();
    }
    private AlertDialog getEditTripDialog(Trip trip) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        View layout = getLayoutInflater().inflate(R.layout.dialog_add_edit_trip, null);
        EditText etTripName = layout.findViewById(R.id.et_trip_name);
        etTripName.setText(trip.getTripName());
        builder.setView(layout);
        AlertDialog dialog = builder.create();
        Button btnConfirmAddEditTrip = layout.findViewById(R.id.btn_confirm_add_edit_trip);
        btnConfirmAddEditTrip.setOnClickListener(view -> {
            editTripAction(trip, etTripName.getText().toString());
            dialog.dismiss();
        });
        Button btnCancelAddTrip = layout.findViewById(R.id.btn_cancel_add_trip);
        btnCancelAddTrip.setOnClickListener(view -> dialog.dismiss());
        return dialog;
    }

    private void addTripAction(String tripName) {
        Trip trip = new Trip(tripName);
        tripsViewModel.addTrip(trip);
    }

    private void editTripAction(Trip trip, String tripName) {
        tripsViewModel.editTrip(trip, tripName);
    }


    private void initTripsPaths() {
       // rvTripPaths = view.findViewById(R.id.rv_trip_paths);
      //  rvTripPaths.setLayoutManager(new LinearLayoutManager(this.getContext()));
     //   tripPathsListAdapter = new TripPathsListAdapter(this.getContext(), tripPathsList, this, this);
      //  rvTripPaths.setAdapter(tripPathsListAdapter);
        //tripPathsListAdapter=new TripPathsListAdapter(this.getContext(),tripPathsList,this::onTripPathClick,this::onCheckDoneClick);

    }


    private void initAdapters() {
        //mountainPointList = view.findViewById(R.id.rv_moutainPoints_list);
      //  mountainPointList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mPListAdapter = new MountainPointListAdapter(this.getContext(), listsmPList);  // not implemented yet, listeners will be necessary
        gotPathsByPointListAdapter = new GotPathsByPointListAdapter(gotPathsByPointList, this);
       // mountainPointList.setAdapter(mPListAdapter);

    }

    // @RequiresApi(api = Build.VERSION_CODES.N)
    private void makeViewModelBinding() {
        MountainPointDao mPDao = new MountainPointDao(this.getContext());
        MtPointRepository repository = new MtPointRepository(mPDao);
        ViewModelProvider.Factory factory = new MountainPointsViewModelFactory(repository);
        pointsViewModel = ViewModelProviders.of(this, factory).get(MountainPointsViewModel.class);
        pointsViewModel.init2();
        pointsViewModel.getMountainPoints2().observe(this, pointlist -> refreshUI(listsmPList, pointlist, mPListAdapter));
        mPListAdapter.notifyDataSetChanged();

    }

    protected <Model> void refreshUI(List<List<Model>> viewModelList, List<List<Model>> updatedModelList, RecyclerView.Adapter adapter) {
        List<List<Model>> actualList = new ArrayList<>();
        boolean same;
        for(int k=0;k<updatedModelList.size();k++) {
            actualList.add(new ArrayList<>());
            for (int i = 0; i < updatedModelList.get(k).size(); i++) {
                same = false;
                for (int j = i + 1; j < updatedModelList.get(k).size(); j++) {
                    if (i != j)
                        if (updatedModelList.get(k).get(i).equals(updatedModelList.get(k).get(j)))
                            same = true;
                }
                if (!same)
                    actualList.get(k).add(updatedModelList.get(k).get(i));
            }
        }
        viewModelList.clear();
        viewModelList.addAll(actualList);
        //viewModelList.addAll(updatedModelList);
        adapter.notifyDataSetChanged();
        mPListAdapter.notifyDataSetChanged();
        makeGotPathViewModelBinding();
    }

    private void makeGotPathViewModelBinding() {
        gotPathDao = new GotPathDao(this.getContext());
        gotPathsRepository = new GotPathsRepository(gotPathDao);
        ViewModelProvider.Factory factory = new GotPathsByPointViewModelFactory(gotPathsRepository);
        gotPathsByPointViewModel=ViewModelProviders.of(this, factory).get(GotPathsByPointViewModel.class);
        gotPathsByPointViewModel.changeSearchMode(GotPathsByPointViewModel.SearchMode.GIVEN_START_POINT);
     //   List<String> mountainPointsNames= new ArrayList<>();
        List<List<String>> mountainPointsNames= new ArrayList<>();

        if(mPListAdapter.listsMountainPoints!=null) {
            for (int i = 0; i < mPListAdapter.listsMountainPoints.size(); i++) {
                mountainPointsNames.add(new ArrayList<>());
                for(int j=0;j<mPListAdapter.listsMountainPoints.get(i).size();j++) {
                    mountainPointsNames.get(i).add(mPListAdapter.listsMountainPoints.get(i).get(j).getPointName());
                }
            }
           for(int j=0;j<mountainPointsNames.size();j++) {// czy jest tu branie z bazy?
               gotPathsList.add(new ArrayList<>());
               for (int i = 0; i < mountainPointsNames.get(j).size() - 1; i++) {
                   gotPathsByPointViewModel.getGotPathsByPoint(mountainPointsNames.get(j).get(i)).observe(this, this::refreshDialogUI);
                   if (!checkIfContains(mountainPointsNames.get(j).get(i + 1),j)) {
                       GotPath gotPath = new GotPath(mPListAdapter.listsMountainPoints.get(j).get(i), mPListAdapter.listsMountainPoints.get(j).get(i+1), 0, 0);
                       gotPath.calculatePoints();
                       gotPathsList.get(j).add(gotPath);
                   }

               }

           }
        makeTripPathList();
        }

    //    makeTripPathViewModelBinding();

    }

    private void makeTripPathList() {//tu skonczyles, powinienes miec liste TripPath
        int lastOrderingNumber=0;
        boolean first;
        for(int i=0;i<gotPathsList.size();i++)
        {
            tripPathsList.add(new ArrayList<>());
            first=true;
            for(int j=0;j<gotPathsList.get(i).size();j++) {
                TripPath tp = new TripPath(gotPathsList.get(i).get(j).getPathId(),
                        gotPathsList.get(i).get(j).getStartPoint().getPointName(),
                        gotPathsList.get(i).get(j).getStartPoint().getSubrange().getSubrangeName(),
                        gotPathsList.get(i).get(j).getEndPoint().getPointName(),
                        gotPathsList.get(i).get(j).getEndPoint().getSubrange().getSubrangeName(),
                        gotPathsList.get(i).get(j).getUpPoints(),
                        gotPathsList.get(i).get(j).getDownPoints(),
                        0,
                        lastOrderingNumber + 1);
                for(int k=0;k<tripPathsList.get(i).size();k++)
                {
                    if(tp.getEndPointName().equals(tripPathsList.get(i).get(k).getEndPointName()) && tp.getStartPointName().equals(tripPathsList.get(i).get(k).getStartPointName()))
                     first=false;
                }
                if(first)
                tripPathsList.get(i).add(tp);
                else
                    first=true;
            }
        }
        makeTrips();

    }

    private void makeTrips() {
        boolean first=true;
        boolean firstGotPath=true;

        for(int i=0;i<tripPathsList.size();i++)
        {
            if(tripPathsList.get(i).size()>0) {

            for(int k=0;k<tripsList.size();k++)
            {
                if(tripsList.get(k).getTripName().equals("Trip" + (i + 1)))
                    first=false;
            }
            if(first)
                tripsList.add(new Trip("Trip" + (i + 1)));
            else
                first=true;
            for(int j=0;j<tripPathsList.get(i).size();j++)
            {
                firstGotPath=true;
                for(int h=0;h<tripsList.get(i).getTripPaths().size();h++)
                {
                    if(tripPathsList.get(i).get(j).getStartPointName().equals(tripsList.get(i).getTripPaths().get(h).getStartPointName()) && tripPathsList.get(i).get(j).getEndPointName().equals(tripsList.get(i).getTripPaths().get(h).getEndPointName()))
                        firstGotPath=false;
                }
                if(firstGotPath)
                tripsList.get(i).addTripPath(tripPathsList.get(i).get(j));

            }
            }
        }

        tripsListAdapter=new TripsListAdapter(this.getContext(),tripsList,this,this);
        rvTrip.setAdapter(tripsListAdapter);

    }

    private void makeTripPathViewModelBinding() {
        TripPathDao tripPathDao = new TripPathDao(this.getContext());
        TripPathsRepository tripPathsRepository = new TripPathsRepository(tripPathDao);
        ViewModelProvider.Factory factory = new TripPathsViewModelFactory(tripPathsRepository);
        tripPathsViewModel = ViewModelProviders.of(this, factory).get(TripPathsViewModel.class);
      //  tripPathsViewModel.getTripPaths().observe(this, tripPaths -> refreshUI(tripPathsList, tripPaths, tripPathsListAdapter));
        /*tripPathsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });*/
    }

    private boolean checkIfContains(String s, int j) {

        for(int i=0;i<gotPathsByPointList.size();i++)
        {
            if(gotPathsByPointList.get(i).getEndPoint().getPointName()==s)
            {
                gotPathsList.get(j).add(gotPathsByPointList.get(i));
                return true;
            }
        }
        return false;
    }

    private void refreshDialogUI(List<GotPath> gotPathsByPoint) {
        Log.d(TAG + "refreshDialogUI", "GotPathsByPoint: " + gotPathsByPoint);
        this.gotPathsByPointList.clear();
        this.gotPathsByPointList.addAll(gotPathsByPoint);
        this.gotPathsByPointListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGotPathByPointClick(GotPath gotPathByPoint) {

    }

    @Override
    public void onTripPathClick(TripPath tripPath) {
        Intent intent = new Intent(this.getContext(), TripPathDetailsActivity.class);
        intent.putExtra("tripPathId", tripPath.getTripPathId());
        intent.putExtra("startPointName", tripPath.getStartPointName());
        intent.putExtra("startPointSubrangeName", tripPath.getStartPointSubrangeName());
        intent.putExtra("endPointName", tripPath.getEndPointName());
        intent.putExtra("endPointSubrangeName", tripPath.getEndPointSubrangeName());
        intent.putExtra("upPoints", tripPath.getUpPoints());
        intent.putExtra("downPoints", tripPath.getDownPoints());
        intent.putExtra("pathStatus", tripPath.getPathStatus());
        intent.putExtra("isDone", tripPath.isDone());
        startActivity(intent);
    }

    @Override
    public void onCheckDoneClick(TripPath tripPath) {
        Log.d(TAG + "onCheckDoneClick", "switch check done: " + tripPath);
        tripPathsViewModel.switchCheckDone(tripPath);
    }
}
