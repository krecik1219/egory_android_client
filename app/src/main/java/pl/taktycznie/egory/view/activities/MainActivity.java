package pl.taktycznie.egory.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.globals.UserDataSingleton;
import pl.taktycznie.egory.helpers.UserLoginCacheAccess;
import pl.taktycznie.egory.model.user.UserLoginManager;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MainActivity.class.getSimpleName();

    private TextView tvMainTitle;
    private MaterialButton btnUserTripsMngmt;
    private MaterialButton btnGotPathsMngmt;
    private MaterialButton btnAchievements;
    private MaterialButton btnAbout;
    private MaterialButton btnMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvMainTitle = findViewById(R.id.main_tv_title);
        btnUserTripsMngmt = findViewById(R.id.btn_trip_mngmt);
        btnGotPathsMngmt = findViewById(R.id.btn_got_paths_mngmt);
        btnAchievements = findViewById(R.id.main_btn_achievements);
        btnMap = findViewById(R.id.main_btn_map);
        btnAbout = findViewById(R.id.main_btn_about);

        initViews();
    }

    private void initViews() {
        initButtons();
    }

    private void initButtons() {
        btnUserTripsMngmt.setOnClickListener(view -> goToTripManagementActivity());
        btnGotPathsMngmt.setOnClickListener(view -> goToGotPathsManagementActivity());
        btnAchievements.setOnClickListener(view -> goToAchievementsActivity());
        btnMap.setOnClickListener(view -> goToMapActivity());
        btnAbout.setOnClickListener( view -> goToAboutActivity());
    }

    private void goToMapActivity() {
        Intent i = new Intent(this, MapActivity.class);
        startActivity(i);
    }

    private void goToAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void goToTripManagementActivity() {
        Intent intent = new Intent(this, TripsActivity.class);
        // for demo purpose use user data stored in singleton
        int userId = UserDataSingleton.getInstance().getUserId();
        // this is not necessary, but might provide forward compatibility when user data won't be kept in singleton
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    private void goToGotPathsManagementActivity() {
        Intent intent = new Intent(this, GotPathsMngmtActivity.class);
        // for demo purpose use user data stored in singleton
        int userId = UserDataSingleton.getInstance().getUserId();
        // this is not necessary, but might provide forward compatibility when user data won't be kept in singleton
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    private void goToAchievementsActivity(){
        Intent intent = new Intent(this, MyAchievementsActivity.class);
        int userId = UserDataSingleton.getInstance().getUserId();
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_context_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.user_context_menu_logout) {
            performLogout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void performLogout() {
        UserLoginManager userLoginManager = new UserLoginManager(new UserLoginCacheAccess(this));
        userLoginManager.logoutUser();
        Intent intent = new Intent(this, LoginRegisterActivity.class);
        startActivity(intent);
        finish();
    }

}
