package pl.taktycznie.egory.view.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.repository.GotPathsRepository;
import pl.taktycznie.egory.viewmodel.GotPathsViewModel;
import pl.taktycznie.egory.viewmodel.factories.GotPathsViewModelFactory;

public class ModifyPathActivity extends CoreActivity {

    private String TOAST_ACCEPT_TEXT = "PATH MODIFIED";
    private String TOAST_CANCEL_TEXT = "MODIFYING CANCELED";

    private int _gotPathId;
    private int _subrangeId;

    private TextView _textViewStartPoint;
    private TextView _textViewEndPoint;
    private EditText _editTextPointsUp;
    private EditText _editTextPointsDn;

    private GotPathsViewModel _pathsViewModel;
    private List<GotPath> _gotPaths;

    private Intent _intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_path);

        _gotPaths = new ArrayList<>();
        _intent = getIntent();
        _gotPathId = _intent.getIntExtra("pathId", 0);
        _subrangeId = _intent.getIntExtra("subrangeId", 0);

        makeViewModelBinding();
        initView();
    }

    private void initView() {

        _textViewStartPoint = findViewById(R.id.textView_modify_path_start_point);
        _textViewEndPoint = findViewById(R.id.textView_modify_path_end_point);
        _editTextPointsUp = findViewById(R.id.editText_modify_path_points_UP);
        _editTextPointsDn = findViewById(R.id.editText_modify_path_points_DN);

        _editTextPointsUp.setText(String.valueOf(_intent.getIntExtra("pathPointsUp", 0)));
        _editTextPointsDn.setText(String.valueOf(_intent.getIntExtra("pathPointsDown", 0)));
        _textViewStartPoint.setText(_intent.getStringExtra("pathStartPointName"));
        _textViewEndPoint.setText(_intent.getStringExtra("pathEndPointName"));

    }

    private void makeViewModelBinding() {
        GotPathDao tripDao = new GotPathDao(this);
        GotPathsRepository tripsRepository = new GotPathsRepository(tripDao);
        ViewModelProvider.Factory factory = new GotPathsViewModelFactory(tripsRepository);
        _pathsViewModel = ViewModelProviders.of(this, factory).get(GotPathsViewModel.class);
        _pathsViewModel.initWithSubrangeId(_subrangeId);
        _pathsViewModel.getGotPathsBySubrange().observe(this, this::refreshUI);
        _pathsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void refreshUI(List<GotPath> gotPaths) {
        _gotPaths.clear();
        _gotPaths.addAll(gotPaths);
    }

    public void modifyItemAcceptClick(View v){

        GotPath gotPath = Stream.of(_gotPaths)
                .filter( p -> p.getPathId() == _gotPathId)
                .findSingle()
                .get();

        //Toast.makeText(this, gotPath.getStartPoint().getPointName() + " - " + gotPath.getEndPoint().getPointName() , Toast.LENGTH_SHORT).show();
        int upPoint = Integer.valueOf(_editTextPointsUp.getText().toString());
        int downPoint = Integer.valueOf(_editTextPointsDn.getText().toString());

        _pathsViewModel.editGotPath(gotPath, upPoint, downPoint);

        this.finish();
    }

    public void modifyItemCancelClick(View v){
        Toast.makeText(this, TOAST_CANCEL_TEXT, Toast.LENGTH_SHORT).show();
        this.finish();
    }
}
