package pl.taktycznie.egory.view.activities;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import pl.taktycznie.egory.R;
import pl.taktycznie.egory.view.adapters.ViewPagerAdapter;

public class TripsActivity extends CoreActivity {

    private static final String TAG = TripsActivity.class.getSimpleName();
    // Toolbar toolbar;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private TabLayout tabLayout;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips_list);
        //    toolbar=findViewById(R.id.toolBar_tripsPath);
        //   setSupportActionBar(toolbar);
        viewPager=findViewById(R.id.pager);
        adapter=new ViewPagerAdapter(getSupportFragmentManager());
        setupViewPager(viewPager);
        tabLayout=findViewById(R.id.tabLay);
        tabLayout.setupWithViewPager(viewPager);


    }

    private void setupViewPager(ViewPager viewPager) {
        adapter.addFragment(new TripsUserDefinedFragment(), getResources().getString(R.string.user_defined));
        adapter.addFragment(new TripsTrackingBasedFragment(), getResources().getString(R.string.tracking));
        viewPager.setAdapter(adapter);
    }

    public void setViewPager(int fragmentNumber){
        viewPager.setCurrentItem(fragmentNumber);
    }


}
