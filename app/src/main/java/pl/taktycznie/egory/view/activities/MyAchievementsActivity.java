package pl.taktycznie.egory.view.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.BadgeDao;
import pl.taktycznie.egory.dao.DataAccessError;
import pl.taktycznie.egory.dao.TripDao;
import pl.taktycznie.egory.dao.TripPathDao;
import pl.taktycznie.egory.helpers.Pair;
import pl.taktycznie.egory.helpers.validators.TripDataValidator;
import pl.taktycznie.egory.model.GotBadge;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.repository.TripsRepository;
import pl.taktycznie.egory.viewmodel.AchievementsViewModel;
import pl.taktycznie.egory.viewmodel.TripPathsViewModel;
import pl.taktycznie.egory.viewmodel.factories.AchievementsViewModelFactory;

public class MyAchievementsActivity extends AppCompatActivity {

    private LinearLayout _nextBadgeLinearLayout;
    private LinearLayout _tripsHistoryLinearLayout;
    private TextView _pointsCountTextView;
    private ProgressBar _progressBar;
    private ImageView _badgeImageView;
    private ImageView _nextBadgeImageView;
    private TextView _nextBadgePointsRequiredTextView;


    private int userId;
    private int pathsLoaderCounter;
    private List<Integer> loadedTripsIds;
    //private List<Pair<Trip, Boolean>> isTripPathsLoaded;

    private AchievementsViewModel achievementsViewModel;

    //private List<Trip> tripsList = new ArrayList<>();
    private List<TripPath> tripPathsList = new ArrayList<>();

    private TripPathsViewModel tripPathsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_achievements);

        userId = getIntent().getIntExtra("userId", 1);
        makeViewModelBinding();

        initLayoutActions();

    }

    private void initLayoutActions() {
        _nextBadgeLinearLayout = findViewById(R.id.myAchievements_ll_nextBadge);
        _tripsHistoryLinearLayout = findViewById(R.id.myAchievements_ll_tripHistory);
        _pointsCountTextView = findViewById(R.id.myAchievements_tv_pointsCount);
        _badgeImageView = findViewById(R.id.myAchievements_iv_actualBadge);
        _nextBadgeImageView = findViewById(R.id.myAchievements_iv_nextBadge);
        _progressBar = findViewById(R.id.myAchievements_pb_progress);
        _nextBadgePointsRequiredTextView = findViewById(R.id.myAchievements_tv_nextBadgePointsRequired);


        _nextBadgeLinearLayout.setOnClickListener(v -> goToAllBadgesActivity());
        _tripsHistoryLinearLayout.setOnClickListener(v -> goToTrips());
        //setPoints();
    }

    private void goToAllBadgesActivity() {
        Intent allBadgesIntent = new Intent(this, AllBadgesActivity.class);
        startActivity(allBadgesIntent);
    }

    private void goToTrips() {
        Intent tripsIntent = new Intent(this, TripsActivity.class);
        tripsIntent.putExtra("userId", userId);
        startActivity(tripsIntent);
    }

    private void makeViewModelBinding() {
        TripDao tripDao = new TripDao(this);
        TripsRepository tripsRepository = new TripsRepository(tripDao);
        TripDataValidator validator = new TripDataValidator();
        TripPathDao tripPathDao = new TripPathDao(this);
        TripPathsRepository tripPathsRepository = new TripPathsRepository(tripPathDao);

        //isTripPathsLoaded = new ArrayList<Pair<Trip, Boolean>>();
        loadedTripsIds = new ArrayList<>();

        ViewModelProvider.Factory factory = new AchievementsViewModelFactory(tripsRepository, tripPathsRepository, validator);
        achievementsViewModel = ViewModelProviders.of(this, factory).get(AchievementsViewModel.class);
        achievementsViewModel.initTrips(userId);
        achievementsViewModel.getUserTrips().observe(this, trips -> setPoints(trips));
        achievementsViewModel.getError().observe(this, error -> {
            if (error != null)
                handleDataAccessErrorInfoInUI(error);
        });

    }

    protected <Model> void getUserTripPaths(List<Model> viewModelList, List<Model> updatedModelList) {
        viewModelList.clear();
        viewModelList.addAll(updatedModelList);
    }


    protected void handleDataAccessErrorInfoInUI(DataAccessError error) {
        Toast.makeText(this, error.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    private void setPoints(List<Trip> trips) {

        pathsLoaderCounter = 0;
        if (trips == null) {
            //_pointsCountTextView.setText(String.valueOf(0));
        } else {
            if (trips.size() == 0) {
                _pointsCountTextView.setText(String.valueOf(0));
                Log.i("LOADING...", "TRIPS COUNT:" + String.valueOf(trips.size()));
                return;
            }

//            for (Trip t : trips) {
//                isTripPathsLoaded.add(new Pair<>(t, false));
//            }

            for (Trip t : trips) {
                achievementsViewModel.getUserTripPaths(t).observe(this, tripPathsList -> setTripPath(t, trips, tripPathsList));
            }

        }

    }

    private void setTripPath(Trip trip, List<Trip> trips, List<TripPath> tripPaths) {

        for (TripPath tp : tripPaths) {
            if( tp.getTripId() == trip.getTripId()){
                trip.addTripPath(tp);
            }
        }

        if ( !loadedTripsIds.contains(trip.getTripId())){
            loadedTripsIds.add(trip.getTripId());
        }

        tripPathsList.addAll(tripPaths);
        pathsLoaderCounter++;

        //if (checkIfAllLoaded(isTripPathsLoaded)) {
        //if ( loadedTripsIds.size() == trips.size()){
        if ( pathsLoaderCounter == trips.size()){
            int points = achievementsViewModel.calculateAchievementPoints(trips);
            //int points = 120;
            List<GotBadge> badges = achievementsViewModel.getBadgesList();

            int actualBadgeRequiredPoints = achievementsViewModel.getActualBadge(points, badges).getRequiredPoints();
            int realScore = achievementsViewModel.getActualScoreForBadge(points, badges);
            int nextBadgeRequiredPoints = achievementsViewModel.getNextBadgeRequirePoints(points, badges);
            _pointsCountTextView.setText(String.valueOf(realScore));
            _progressBar.setMax(nextBadgeRequiredPoints);
            _progressBar.setProgress(realScore);
            setActualBadgeImage(points, badges);
            setNextBadge(points, nextBadgeRequiredPoints, badges);
        }

    }

    private boolean checkIfAllLoaded(List<Pair<Trip, Boolean>> isPathsLoaded) {

        for (Pair<Trip, Boolean> tb : isPathsLoaded) {
            if (!tb.second) {
                return false;
            }
        }
        return true;
    }

    private void setActualBadgeImage(int points, List<GotBadge> badges) {
        setBadgeImage(achievementsViewModel.getActualBadge(points, badges).getBadgeId()+1, _badgeImageView);
    }

    private void setNextBadge(int points, int nextBadgeScore, List<GotBadge> badges) {

        GotBadge actual = achievementsViewModel.getActualBadge(points, badges);
        int nextId = actual.getBadgeId()+1;
        if (nextId < badges.size() - 1) {
            nextId++;
        }
        _nextBadgePointsRequiredTextView.setText(String.valueOf(nextBadgeScore));

        setBadgeImage(nextId , _nextBadgeImageView);


    }

    private void setBadgeImage(int badgeId, ImageView imageViewBadgePic) {
        switch (badgeId) {
            case 1:
                Picasso.get().load(R.drawable.badge_w_gory_brazowa).into(imageViewBadgePic);
                break;
            case 2:
                Picasso.get().load(R.drawable.badge_w_gory_srebrna).into(imageViewBadgePic);
                break;
            case 3:
                Picasso.get().load(R.drawable.badge_w_gory_zlota).into(imageViewBadgePic);
                break;
            case 4:
                Picasso.get().load(R.drawable.badge_popularna).into(imageViewBadgePic);
                break;
            case 5:
                Picasso.get().load(R.drawable.badge_mala_brazowa).into(imageViewBadgePic);
                break;
            case 6:
                Picasso.get().load(R.drawable.badge_mala_srebrna).into(imageViewBadgePic);
                break;
            case 7:
                Picasso.get().load(R.drawable.badge_mala_zlota).into(imageViewBadgePic);
                break;
            case 8:
                Picasso.get().load(R.drawable.badge_duza_brazowa).into(imageViewBadgePic);
                break;
            case 9:
                Picasso.get().load(R.drawable.badge_duza_srebrna).into(imageViewBadgePic);
                break;
            case 10:
                Picasso.get().load(R.drawable.badge_duza_zlota).into(imageViewBadgePic);
                break;
            case 11:
                Picasso.get().load(R.drawable.badge_za_wytrwalosc).into(imageViewBadgePic);
                break;
            case 12:
                Picasso.get().load(R.drawable.badge_przodownik).into(imageViewBadgePic);
                break;
            case 13:
                Picasso.get().load(R.drawable.badge_honorowy_przodownik).into(imageViewBadgePic);
                break;
        }
    }


}
