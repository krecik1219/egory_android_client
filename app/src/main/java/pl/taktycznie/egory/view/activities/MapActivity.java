package pl.taktycznie.egory.view.activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;

import androidx.appcompat.app.AlertDialog;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.camera.CameraActivity;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.globals.UserDataSingleton;
import pl.taktycznie.egory.helpers.BoolPaired;
import pl.taktycznie.egory.helpers.Pair;
import pl.taktycznie.egory.model.tracking.LocationMaintainer;
import pl.taktycznie.egory.model.tracking.LocationPoint;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.model.tracking.PhotoTag;
import pl.taktycznie.egory.model.tracking.Tracker;
import pl.taktycznie.egory.repository.LocationRepository;
import pl.taktycznie.egory.services.Constants;
import pl.taktycznie.egory.services.LocationSyncAlarmReceiver;
import pl.taktycznie.egory.utils.IMarkerOnClickFn;
import pl.taktycznie.egory.utils.PermissionManager;
import pl.taktycznie.egory.view.adapters.PhotoTagsListAdapter;
import pl.taktycznie.egory.view.adapters.TrackingSessionsListAdapter;
import pl.taktycznie.egory.viewmodel.MapViewModel;
import pl.taktycznie.egory.viewmodel.factories.MapViewModelFactory;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,
        TrackingSessionsListAdapter.OnTrackingSessionClickListener,
        GoogleMap.OnMarkerClickListener {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MapActivity.class.getSimpleName();

    private FloatingActionButton fabSessionTag;
    private CheckBox cbCurrentSessionTrackingCheck;
    private RecyclerView rvTrackingSessions;
    private DrawerLayout navigationDrawer;
    private TrackingSessionsListAdapter trackingSessionsListAdapter;
    private GoogleMap googleMap;
    private PermissionManager permissionManager;
    private MapViewModel mapViewModel;
    private Tracker.TrackerState trackerState;

    private boolean deferredRestore = false;
    private CameraPosition restoredCameraPosition;
    private boolean deferredUpdate = false;
    private Queue<LocationSession> deferredUpdateQueue = new LinkedList<>();

    //private Queue<LocationSession> deferredUIUpdates = new LinkedList<>();
    //private MapMode mapMode = MapMode.CURRENT_SESSION_TRACKING;

//    private enum MapMode {
//        CURRENT_SESSION_TRACKING,
//        FREE_TRACK_SELECTION
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "activity.onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        checkPermissions();
        bindToViewModel();
        initViews();
        getTrackerState();
        performActionBasedOnTrackerState();
        scheduleSyncAlarmIfNotScheduled();
    }

    private void checkPermissions() {
        permissionManager = new PermissionManager(this);
        permissionManager.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionManager.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        permissionManager.requestPermissions();
    }

    private void getTrackerState() {
        trackerState = mapViewModel.getTrackerState();
    }

    private void bindToViewModel() {
        LocationRepository locationRepository = new LocationRepository(getApplicationContext());
        Tracker tracker = new Tracker(getApplication());
        ViewModelProvider.Factory factory = new MapViewModelFactory(getApplication(), locationRepository, tracker, UserDataSingleton.getInstance().getUserId());
        mapViewModel = ViewModelProviders.of(this, factory).get(MapViewModel.class);
        mapViewModel.init();
        observeOnCurrentLocationUpdates();
        observeOnTrackingStateChanges();
        //mapViewModel.init();
    }

    private void initViews() {
        initMap();
        initTrackingSessionsRecyclerView();
        initCurrentSessionPanel();
        initDrawer();
        initCameraTagButton();
    }

    private void initCameraTagButton() {
        fabSessionTag = findViewById(R.id.fab_tag_tracking_session);
        fabSessionTag.setOnClickListener(v -> cameraTagBtnClicked());
        if(trackerState == Tracker.TrackerState.ACTIVE) {
            showCameraTagFloatingButton();
        }
        else {
            hideCameraTagFloatingButton();
        }
    }

    private void initTrackingSessionsRecyclerView() {
        rvTrackingSessions = findViewById(R.id.rv_tracking_sessions_list);
        rvTrackingSessions.setLayoutManager(new LinearLayoutManager(this));
        trackingSessionsListAdapter = new TrackingSessionsListAdapter(this, mapViewModel.getLocationSessionsList());
        rvTrackingSessions.setAdapter(trackingSessionsListAdapter);
    }

    private void initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if(mapFragment != null)
            mapFragment.getMapAsync(this);
    }

    private void initCurrentSessionPanel() {
        cbCurrentSessionTrackingCheck = findViewById(R.id.cb_current_session_check);
        cbCurrentSessionTrackingCheck.setOnClickListener(v -> handleCurrentSessionClick());
    }

    private void initDrawer() {
        navigationDrawer = findViewById(R.id.dl_sessions_navigation_drawer);
        navigationDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                Log.d(TAG, "onDrawerOpened");
                mapViewModel.init();
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {}

            @Override
            public void onDrawerStateChanged(int newState) {}
        });
    }

    private void handleCurrentSessionClick() {
            Log.d(TAG, "activity handleCurrentSessionCheck(): ");
            BoolPaired<LocationSession> currentTrackingSession = mapViewModel.getCurrentTrackingSession();
            List<BoolPaired<LocationSession>> locationSessionsList = mapViewModel.getLocationSessionsList();
            if(currentTrackingSession == null) {
                Log.d(TAG, "activity onCheckedChangeListener(): current tracking session is null");
                return;
            }
            if(cbCurrentSessionTrackingCheck.isChecked()) {
                Log.d(TAG, "activity onCheckedChangeListener(): CHECKED");
                if(googleMap != null) {
                    currentTrackingSession.setFlag();
                    displayTrackingSessionOnMap(currentTrackingSession.getItem(), true);
                }
            }
            else {
                Log.d(TAG, "activity onCheckedChangeListener(): UNCHECKED");
                if(googleMap != null) {
                    currentTrackingSession.unsetFlag();
                    hideTrackingSessionOnMap(currentTrackingSession.getItem());
                }
            }
            if(locationSessionsList.size() > 0)
                trackingSessionsListAdapter.notifyItemChanged(locationSessionsList.size() - 1);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "activity.onMapReady()");
        this.googleMap = googleMap;

        // Add a marker in Sydney and move the camera
        if(deferredRestore) {
            restoreMap();
            deferredRestore = false;
        }
        if(deferredUpdate) {
            mapViewModel.init();
        }

        this.googleMap.setOnMarkerClickListener(this);

//        if(mapMode == MapMode.CURRENT_SESSION_TRACKING)
//            updateUIWithDeferredQueue();
    }

//    private void updateUIWithDeferredQueue() {
//        if(!deferredUIUpdates.isEmpty()) {
//            Log.d(TAG, "updateUI(): updateUIWithDeferredQueue");
//            while(!deferredUIUpdates.isEmpty())
//                performUIUpdate(deferredUIUpdates.poll());
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(!permissionManager.handlePermissionsResult(requestCode, permissions, grantResults))
            finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "activity.onResume()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "activity.onStart()");
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "activity.onPause()");
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "activity.onSaveInstanceState()");
        super.onSaveInstanceState(outState);
        mapViewModel.destroyGuiShapes();
        outState.putParcelable("cameraPosition", googleMap.getCameraPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "activity.onRestoreInstanceState()");
        super.onRestoreInstanceState(savedInstanceState);
        restoredCameraPosition = savedInstanceState.getParcelable("cameraPosition");
        if(googleMap == null) {
            deferredRestore = true;
        }
        else
            restoreMap();
    }

    private void restoreMap() {
        restorePolylines();
        if (restoredCameraPosition != null)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restoredCameraPosition.target, restoredCameraPosition.zoom));
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "activity.onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "activity.onDestroy()");
        super.onDestroy();
    }

    private void restorePolylines() {
        List<BoolPaired<LocationSession>> locationSessionsList = mapViewModel.getLocationSessionsList();
        for(BoolPaired<LocationSession> ls : locationSessionsList) {
            if(ls.flag())
                displayTrackingSessionOnMap(ls.getItem(), false);
        }
    }

    private void updateUI(LocationMaintainer locationMaintainer) {
        Log.d(TAG, "activity updateUI(): ");
        updateTrackingSessions(locationMaintainer);
        if(shouldUpdateCurrentSessionOnUI())
            handleUIUpdateCurrentSessionTracking();
    }

    private boolean shouldUpdateCurrentSessionOnUI() {
        BoolPaired<LocationSession> currentTrackingSession = mapViewModel.getCurrentTrackingSession();
        return currentTrackingSession != null && trackerState == Tracker.TrackerState.ACTIVE && currentTrackingSession.flag();
    }

    private void updateTrackingSessions(LocationMaintainer locationMaintainer) {
        Log.d(TAG, "updateTrackingSessions");
        List<BoolPaired<LocationSession>> provided = BoolPaired.makeBoolPaierdCollection(locationMaintainer.getLocationSessions());
        applyCurrentUIStateToProvided(provided);
        List<BoolPaired<LocationSession>> locationSessionsList = mapViewModel.getLocationSessionsList();
        locationSessionsList.clear();
        locationSessionsList.addAll(provided);
        if(locationSessionsList.size() > 0) {
            BoolPaired<LocationSession> currentTrackingSession = mapViewModel.getCurrentTrackingSession();
            boolean shouldPresentCurrentTrackingSession = currentTrackingSession != null && currentTrackingSession.flag();
            mapViewModel.setCurrentTrackingSession(locationSessionsList.get(locationSessionsList.size() - 1));
            currentTrackingSession = mapViewModel.getCurrentTrackingSession();
            currentTrackingSession.setFlagTo(shouldPresentCurrentTrackingSession);
            cbCurrentSessionTrackingCheck.setChecked(shouldPresentCurrentTrackingSession);
        }
        trackingSessionsListAdapter.notifyDataSetChanged();
    }

    private void applyCurrentUIStateToProvided(List<BoolPaired<LocationSession>> providedList) {
        List<BoolPaired<LocationSession>> locationSessionsList = mapViewModel.getLocationSessionsList();
        Log.d(TAG, "applyCurrentUIStateToProvided, provided: " + providedList);
        Log.d(TAG, "applyCurrentUIStateToProvided, local locationSessionList: " + locationSessionsList);
        BoolPaired<LocationSession> current;
        BoolPaired<LocationSession> provided;
        for(int i = 0; i < locationSessionsList.size() && i < providedList.size(); i++) {
            current = locationSessionsList.get(i);
            provided = providedList.get(i);
            if(current.getItem().equalsWithoutLastPoint(provided.getItem())) {
                Log.v(TAG, "applyCurrentUIStateToProvided MATCH; local: " + current.getItem());
                Log.v(TAG, "applyCurrentUIStateToProvided; provided: " + provided.getItem());
                provided.setFlagTo(current.flag());
            }
            else {
                Log.v(TAG, "applyCurrentUIStateToProvided MISS MATCH; local: " + current.getItem());
                Log.v(TAG, "applyCurrentUIStateToProvided; provided: " + provided.getItem());
            }
        }
    }

    private void handleUIUpdateCurrentSessionTracking() {
        if(googleMap == null) {
            Log.d(TAG, "handleUIUpdateCurrentSessionTracking, deferred update set");
            deferredUpdate = true;
            return;
        }
        deferredUpdate = false;
        BoolPaired<LocationSession> currentTrackingSession = mapViewModel.getCurrentTrackingSession();
        Log.d(TAG, "handleUIUpdateCurrentSessionTracking, current session: " + currentTrackingSession);
        performUIUpdate(currentTrackingSession.getItem(), false);
    }

    private void performActionBasedOnTrackerState() {
        BoolPaired<LocationSession> currentTrackingSession = mapViewModel.getCurrentTrackingSession();
        Log.d(TAG, "performActionBasedOnTrackerState, current session: " + currentTrackingSession);
        if(trackerState == Tracker.TrackerState.ACTIVE) {
            Log.d(TAG, "performActionBasedOnTrackerState, tracker active");
            Toast.makeText(this, "Tracking active", Toast.LENGTH_SHORT).show();
            showCameraTagFloatingButton();
        }
        else {
            Log.d(TAG, "performActionBasedOnTrackerState, tracker inactive");
            Toast.makeText(this, "Tracking inactive", Toast.LENGTH_SHORT).show();
            hideCameraTagFloatingButton();
        }
    }

    private void observeOnCurrentLocationUpdates() {
        mapViewModel.getLocationMaintainerLD().observe(this, locationMaintainer -> {
            if(locationMaintainer == null)
                return;
            updateUI(new LocationMaintainer(locationMaintainer));
        });
    }

    private void observeOnTrackingStateChanges() {
        mapViewModel.getTrackingStateChangedStatus().observe(this, hasChanged -> {
            if(hasChanged != null && hasChanged) {
                if(mapViewModel.getTrackerState() == Tracker.TrackerState.INACTIVE)
                    displaySessionNamingDialog(mapViewModel.getCurrentTrackingSession().getItem());
            }
        });
    }

    private void displaySessionNamingDialog(LocationSession session) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View layout = getLayoutInflater().inflate(R.layout.dialog_provide_tracking_session_name, null);
        EditText etSessionName = layout.findViewById(R.id.et_tracking_session_name);
        builder.setView(layout);
        AlertDialog dialog = builder.create();
        Button btnConfirmSetSessionName = layout.findViewById(R.id.btn_confirm_set_session_name);
        btnConfirmSetSessionName.setOnClickListener(view -> {
            mapViewModel.updateSessionName(etSessionName.getText().toString(), session);
            dialog.dismiss();
        });
        dialog.show();
    }

    private void scheduleSyncAlarmIfNotScheduled() {
        Intent intent = new Intent(getApplicationContext(), LocationSyncAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, LocationSyncAlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.SYNC_ALARM_INTERVAL, pendingIntent);
        }
        else {
            Log.e(TAG, "Alarm manger system service NULL");
        }
    }

    @Override
    public void onTrackingSessionClick(CheckBox v, BoolPaired<LocationSession> locationSession) {
        Log.d(TAG, "onTrackingSessionClick");
        if(v.isChecked()) {
            locationSession.setFlag();
            displayTrackingSessionOnMap(locationSession.getItem(), true);
        }
        else {
            locationSession.unsetFlag();
            hideTrackingSessionOnMap(locationSession.getItem());
        }
        if(locationSession == mapViewModel.getCurrentTrackingSession())
            cbCurrentSessionTrackingCheck.setChecked(!cbCurrentSessionTrackingCheck.isChecked());
    }

    private void hideTrackingSessionOnMap(LocationSession locationSession) {
        Log.d(TAG, "hideTrackingSessionOnMap(): locationSession: " + locationSession);
        Pair<MapViewModel.GuiShape, Integer> guiShapeAndIndex = mapViewModel.getGuiShapeMatchingTrackingSession(locationSession);
        if(guiShapeAndIndex.first == null) {
            Log.e(TAG, "hideTrackingSessionOnMap(): tracking session which is going to be hidden is not matching any gui shape");
            return;
        }
        mapViewModel.removeGuiShape(guiShapeAndIndex);
        Log.d(TAG, "hideTrackingSessionOnMap(): successful, session: " + locationSession);
    }

    private void displayTrackingSessionOnMap(LocationSession locationSession, boolean shouldSetupCamera) {
        if(googleMap == null) {
            Log.d(TAG, "activity displayTrackingSessionOnMap():deferring update");
            deferredUpdate = true;
            deferredUpdateQueue.offer(new LocationSession(locationSession));
            return;
        }
        deferredUpdate = false;
        List<LatLng> points = Stream.of(locationSession.getLocationPoints()).map(point -> new LatLng(point.getLatitude(), point.getLongitude())).collect(Collectors.toList());
        List<Pair<LatLng, List<PhotoTag>>> pointsTags = new ArrayList<>();
        for(LocationPoint point : locationSession.getLocationPoints()) {
            List<PhotoTag> tags = new ArrayList<>();
            Pair<LatLng, List<PhotoTag>> pair = new Pair<>(new LatLng(point.getLatitude(), point.getLongitude()), tags);
            tags.addAll(point.getPhotoTags());
            if(tags.size() > 0)
                pointsTags.add(pair);
        }
        Log.d(TAG, "activity displayTrackingSessionOnMap(): LatLng points: " + points);
        if(points.size() > 0) {
            mapViewModel.addGuiShape(points, pointsTags, googleMap, locationSession);
            if(shouldSetupCamera) {
                LatLng middlePoint = points.get((points.size() - 1) / 2);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(middlePoint.latitude, middlePoint.longitude), 15));
            }
        }
    }

    public void performUIUpdate(LocationSession locationSession, boolean shouldSetupCamera) {
        Log.d(TAG, "activity performUIUpdate(): LatLng points: " + locationSession);
        hideTrackingSessionOnMap(locationSession);
        displayTrackingSessionOnMap(locationSession, shouldSetupCamera);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.map_menu_item_toggle_location) {
            trackerState = mapViewModel.toggleTracking();
            performActionBasedOnTrackerState();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showCameraTagFloatingButton() {
        Log.d(TAG, "showCameraTagFloatingButton");
        fabSessionTag.show();
    }

    private void hideCameraTagFloatingButton() {
        Log.d(TAG, "hideCameraTagFloatingButton");
        fabSessionTag.hide();
    }

    private void cameraTagBtnClicked() {
        Log.d(TAG, "cameraTagBtnClicked");
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        IMarkerOnClickFn markerOnClickFn = mapViewModel.getMarkerOnClickFn(marker);
        if(markerOnClickFn == null) {
            Log.d(TAG, "on click fn missing for marker: " + marker);
            return true;
        }
        List<String> imagesPaths = markerOnClickFn.onClick();
        displayPhotoTags(imagesPaths);
        return true;
    }

    private void displayPhotoTags(List<String> imagesPaths) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View layout = getLayoutInflater().inflate(R.layout.dialog_photo_tags, null);
        ImageButton ibPhotoTagsBack = layout.findViewById(R.id.ib_photo_tags_back);
        RecyclerView rvPhotoTagsList = layout.findViewById(R.id.rv_photo_tags);
        rvPhotoTagsList.setLayoutManager(new LinearLayoutManager(this));
        List<Bitmap> images = Stream.of(imagesPaths).map(BitmapFactory::decodeFile).collect(Collectors.toList());
        PhotoTagsListAdapter adapter = new PhotoTagsListAdapter(images);
        rvPhotoTagsList.setAdapter(adapter);
        builder.setView(layout);
        final AlertDialog dialog = builder.create();
        ibPhotoTagsBack.setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }
}
