package pl.taktycznie.egory.view.adapters;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.PhotoConfirmation;

public class PhotoConfirmationListAdapter extends RecyclerView.Adapter<PhotoConfirmationListAdapter.ViewHolder> {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + PhotoConfirmationListAdapter.class.getSimpleName();

    private List<PhotoConfirmation> photoConfirmations;
    private OnPhotoClickListener onPhotoClickListener;

    public PhotoConfirmationListAdapter(@NonNull List<PhotoConfirmation> photoConfirmations, OnPhotoClickListener onPhotoClickListener) {
        this.photoConfirmations = photoConfirmations;
        this.onPhotoClickListener = onPhotoClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.confirmations_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bitmap image = photoConfirmations.get(position).getImage();
        holder.ivPhotoConfirmation.setImageBitmap(image);
    }

    @Override
    public int getItemCount() {
        return photoConfirmations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivPhotoConfirmation;

        ViewHolder(View itemView) {
            super(itemView);
            ivPhotoConfirmation = itemView.findViewById(R.id.iv_photo_confirmation);
            ivPhotoConfirmation.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onPhotoClickListener.onPhotoClick((ImageView) v);
        }
    }

    public interface OnPhotoClickListener {
        void onPhotoClick(ImageView photo);
    }
}
