package pl.taktycznie.egory.view.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.view.fragments.helpers.LoginParametersPack;

public class LoginFragment extends Fragment {

    private View view;

    private OnLoginFragmentActionListener interactionListener;
    private EditText etLogin;
    private EditText etPassword;
    private Button btnLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews();
    }

    private void initViews() {
        etLogin = view.findViewById(R.id.et_login);
        etPassword = view.findViewById(R.id.et_password);
        btnLogin = view.findViewById(R.id.login_btn);
        btnLogin.setOnClickListener(
                view1 -> interactionListener.onLoginFragmentAction(
                        new LoginParametersPack(
                                etLogin.getText().toString(),
                                etPassword.getText().toString()
                        )
                )
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentActionListener) {
            interactionListener = (OnLoginFragmentActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    public interface OnLoginFragmentActionListener {
        void onLoginFragmentAction(LoginParametersPack params);
    }
}
