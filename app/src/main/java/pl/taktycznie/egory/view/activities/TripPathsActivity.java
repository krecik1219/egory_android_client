package pl.taktycznie.egory.view.activities;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.dao.TripPathDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.TripPathParamsHolder;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.model.Trip;
import pl.taktycznie.egory.model.TripPath;
import pl.taktycznie.egory.repository.GotPathsRepository;
import pl.taktycznie.egory.repository.TripPathsRepository;
import pl.taktycznie.egory.view.adapters.GotPathsByPointListAdapter;
import pl.taktycznie.egory.view.adapters.TripPathsListAdapter;
import pl.taktycznie.egory.viewmodel.GotPathsByPointViewModel;
import pl.taktycznie.egory.viewmodel.TripPathsViewModel;
import pl.taktycznie.egory.viewmodel.factories.GotPathsByPointViewModelFactory;
import pl.taktycznie.egory.viewmodel.factories.TripPathsViewModelFactory;

public class TripPathsActivity extends CoreActivity implements
        TripPathsListAdapter.OnTripPathClickListener,
        TripPathsListAdapter.OnCheckDoneClick,
        SearchView.OnQueryTextListener,
        CompoundButton.OnCheckedChangeListener,
        GotPathsByPointListAdapter.OnGotPathByPointClickListener {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripPathsActivity.class.getSimpleName();

    private RecyclerView rvTripPaths;
    private SearchView svMountainPointSearch;
    private SwitchCompat scSwitchDirection;
    AlertDialog selectPathDialog;

    private int tripId;
    private int userId;
    private String tripName;
    private List<TripPath> tripPathsList = new ArrayList<>();
    private List<GotPath> gotPathsByPointList = new ArrayList<>();
    private GotPathsByPointListAdapter gotPathsByPointListAdapter;
    private TripPathsListAdapter tripPathsListAdapter;
    private TripPathsViewModel tripPathsViewModel;
    private GotPathsByPointViewModel gotPathsByPointViewModel;
    private Trip trip;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_paths);

        Intent intent = getIntent();
        tripId = intent.getIntExtra("tripId", 0);
        userId = intent.getIntExtra("userId", 0);

        if(trip!=null)
            initView();
        setTitle(tripName);
        initViews();
        makeMainViewModelBinding();
        makeViewModelBindingForGotPathSelectionDialog();
    }

    private void initView() {
        rvTripPaths = findViewById(R.id.rv_trip_paths);
        rvTripPaths.setLayoutManager(new LinearLayoutManager(this));
        tripPathsListAdapter = new TripPathsListAdapter(this, trip.getTripPaths(), this, this);
        rvTripPaths.setAdapter(tripPathsListAdapter);
    }

    @Override
    public void onTripPathClick(TripPath tripPath) {
        Intent intent = new Intent(this, TripPathDetailsActivity.class);
        intent.putExtra("tripPathId", tripPath.getTripPathId());
        intent.putExtra("startPointName", tripPath.getStartPointName());
        intent.putExtra("startPointSubrangeName", tripPath.getStartPointSubrangeName());
        intent.putExtra("endPointName", tripPath.getEndPointName());
        intent.putExtra("endPointSubrangeName", tripPath.getEndPointSubrangeName());
        intent.putExtra("upPoints", tripPath.getUpPoints());
        intent.putExtra("downPoints", tripPath.getDownPoints());
        intent.putExtra("pathStatus", tripPath.getPathStatus());
        intent.putExtra("isDone", tripPath.isDone());
        startActivity(intent);
    }

    @Override
    public void onCheckDoneClick(TripPath tripPath) {
        Log.d(TAG + "onCheckDoneClick", "switch check done: " + tripPath);
        tripPathsViewModel.switchCheckDone(tripPath);
    }

    @Override
    public boolean onQueryTextSubmit(String pointName) {
        selectPathDialog = getSelectGotPathDialog();
        selectPathDialog.show();
        svMountainPointSearch.clearFocus();
        gotPathsByPointViewModel
                .getGotPathsByPoint(pointName)
                .observe(this, this::refreshDialogUI);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;  // default action
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        gotPathsByPointViewModel.changeSearchMode(GotPathsByPointViewModel.SearchMode.fromBoolean(isChecked));
    }

    @Override
    public void onGotPathByPointClick(GotPath gotPathByPoint) {
        addTripPathAction(gotPathByPoint);
        dismissSelectPathDialogIfShowing();
        updateSearchViewWithProperPointName(gotPathByPoint);
    }

    private void initViews() {
        initTripPathsListRecyclerView();
        initSearchMountainPointsViews();
    }

    private void initTripPathsListRecyclerView() {
        rvTripPaths = findViewById(R.id.rv_trip_paths);
        rvTripPaths.setLayoutManager(new LinearLayoutManager(this));
        tripPathsListAdapter = new TripPathsListAdapter(this, tripPathsList, this, this);
        rvTripPaths.setAdapter(tripPathsListAdapter);
        initSwipeRemoveCallback(this::removeTripPathAction, tripPathsList, rvTripPaths);
    }

    private void initSearchMountainPointsViews() {
        svMountainPointSearch = findViewById(R.id.sv_point_search);
        svMountainPointSearch.setOnQueryTextListener(this);
        scSwitchDirection = findViewById(R.id.sc_switch_direction);
        scSwitchDirection.setOnCheckedChangeListener(this);
    }

    private void makeMainViewModelBinding() {
        TripPathDao tripPathDao = new TripPathDao(this);
        TripPathsRepository tripPathsRepository = new TripPathsRepository(tripPathDao);
        ViewModelProvider.Factory factory = new TripPathsViewModelFactory(tripPathsRepository);
        tripPathsViewModel = ViewModelProviders.of(this, factory).get(TripPathsViewModel.class);
        tripPathsViewModel.init(tripId);
        tripPathsViewModel.getTripPaths().observe(this, tripPaths -> refreshUI(tripPathsList, tripPaths, tripPathsListAdapter));
        tripPathsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void makeViewModelBindingForGotPathSelectionDialog() {
        GotPathDao gotPathDao = new GotPathDao(this);
        GotPathsRepository gotPathsRepository = new GotPathsRepository(gotPathDao);
        ViewModelProvider.Factory factory = new GotPathsByPointViewModelFactory(gotPathsRepository);
        gotPathsByPointViewModel = ViewModelProviders.of(this, factory).get(GotPathsByPointViewModel.class);
        gotPathsByPointViewModel.init(GotPathsByPointViewModel.SearchMode.fromBoolean(scSwitchDirection.isChecked()));
        gotPathsByPointViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void addTripPathAction(GotPath gotPath) {
        TripPathParamsHolder paramsHolder = new TripPathParamsHolder(gotPath, tripId);
        tripPathsViewModel.addTripPath(paramsHolder);
    }

    private void removeTripPathAction(TripPath tripPath) {
        this.tripPathsViewModel.removeTripPath(tripPath);
    }

    private void refreshDialogUI(List<GotPath> gotPathsByPoint) {
        Log.d(TAG + "refreshDialogUI", "GotPathsByPoint: " + gotPathsByPoint);
        this.gotPathsByPointList.clear();
        this.gotPathsByPointList.addAll(gotPathsByPoint);
        this.gotPathsByPointListAdapter.notifyDataSetChanged();
    }

    private void updateSearchViewWithProperPointName(GotPath gotPathByPoint) {
        GotPathsByPointViewModel.SearchMode searchMode =
                GotPathsByPointViewModel.SearchMode.fromBoolean(scSwitchDirection.isChecked());
        switch(searchMode) {
            case GIVEN_START_POINT:
                svMountainPointSearch.setQuery(gotPathByPoint.getEndPoint().getPointName(), false);
                break;
            case GIVEN_END_POINT:
                svMountainPointSearch.setQuery(gotPathByPoint.getStartPoint().getPointName(), false);
                break;
        }
    }

    private AlertDialog getSelectGotPathDialog() {
        if(selectPathDialog != null)
            return selectPathDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View layout = getLayoutInflater().inflate(R.layout.dialog_select_got_path, null);
        RecyclerView rvGotPathsByPoint = layout.findViewById(R.id.rv_got_paths_by_point);
        rvGotPathsByPoint.setLayoutManager(new LinearLayoutManager(this));
        gotPathsByPointListAdapter = new GotPathsByPointListAdapter(gotPathsByPointList, this);
        rvGotPathsByPoint.setAdapter(gotPathsByPointListAdapter);
        builder.setView(layout);
        return builder.create();
    }

    private void dismissSelectPathDialogIfShowing() {
        if(selectPathDialog != null && selectPathDialog.isShowing())
            selectPathDialog.dismiss();
    }
}
