package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.model.MountainSubrange;
import pl.taktycznie.egory.view.activities.PathListActivity;

public class SubrangeListAdapter extends RecyclerView.Adapter<SubrangeListAdapter.SubrangeListHolder> {

    private List<MountainSubrange> _subranges;
    private Context _context;

    public SubrangeListAdapter(List<MountainSubrange> subranges, Context context){
        _subranges = subranges;
        _context = context;
    }

    @NonNull
    @Override
    public SubrangeListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.subrange_list_item, parent, false);

        return new SubrangeListHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SubrangeListHolder holder, int position) {
        MountainSubrange subrange = _subranges.get(position);

        holder.textViewSubrangeName.setText(subrange.getSubrangeName());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_context, PathListActivity.class);
                int subrangeId = subrange.getSubrangeId();
                intent.putExtra("subrangeId", subrangeId);
                _context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return _subranges.size();
    }

    protected class SubrangeListHolder extends RecyclerView.ViewHolder {

        private TextView textViewSubrangeName;
        private ConstraintLayout container;

        public SubrangeListHolder(View itemView) {
            super(itemView);
            textViewSubrangeName = itemView.findViewById(R.id.textView_subrange_name);
            container = itemView.findViewById(R.id.subrange_item_container);
        }
    }

}
