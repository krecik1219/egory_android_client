package pl.taktycznie.egory.view.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.GotPathDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.globals.UserDataSingleton;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.repository.GotPathsRepository;
import pl.taktycznie.egory.view.adapters.PathListAdapter;
import pl.taktycznie.egory.viewmodel.GotPathsViewModel;
import pl.taktycznie.egory.viewmodel.factories.GotPathsViewModelFactory;

public class PathListActivity extends CoreActivity {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MainActivity.class.getSimpleName();

    private List<GotPath> _pathsList;
    private GotPathsViewModel _pathsViewModel;
    private RecyclerView _pathsRecyclerView;
    private PathListAdapter _adapter;
    private Context _context;
    private ImageButton _addButton;
    private UserDataSingleton _user;
    private int _subrangeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_list);

        _context = this;
        _user = UserDataSingleton.getInstance();
        Intent intent = getIntent();
        _subrangeId = intent.getIntExtra("subrangeId", 0);
    }

    @Override
    protected void onResume() {
        setUpAddButtonIfLeader();
        initView();
        makeViewModelBinding();
        
        super.onResume();
    }

    public int getListSize() {
        return _pathsList.size();
    }

    private void makeViewModelBinding() {
        GotPathDao tripDao = new GotPathDao(this);
        GotPathsRepository tripsRepository = new GotPathsRepository(tripDao);
        ViewModelProvider.Factory factory = new GotPathsViewModelFactory(tripsRepository);
        _pathsViewModel = ViewModelProviders.of(this, factory).get(GotPathsViewModel.class);
        _pathsViewModel.initWithSubrangeId(_subrangeId);
        _pathsViewModel.getGotPathsBySubrange().observe(
                this,
                gotPaths -> refreshUI(gotPaths));
        _pathsViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }

    private void refreshUI(List<GotPath> gotPaths) {
        _pathsList.clear();
        _pathsList.addAll(gotPaths);
//        _pathsList.sort(new Comparator<GotPath>() {
//            @Override
//            public int compare(GotPath path_1, GotPath path_2) {
//                return path_1.getStartPoint().getPointName().compareTo(path_2.getStartPoint().getPointName());
//            }
//        });
        Collections.sort(_pathsList, (GotPath gp1, GotPath gp2) ->  gp1.getStartPoint().getPointName().compareTo(gp2.getStartPoint().getPointName()));
        _adapter.notifyDataSetChanged();
    }

    private void setUpAddButtonIfLeader() {
        _addButton = findViewById(R.id.button_add);
        if (_user.isAdmin()){
            _addButton.setVisibility(View.VISIBLE);
            _addButton.setOnClickListener(view -> {
                Intent intent = new Intent(_context, AddPathActivity.class);
                intent.putExtra("subrangeId", _subrangeId);
                startActivity(intent);
            });
        }
    }

    private void initView() {
        _pathsRecyclerView = findViewById(R.id.paths_recycler_view);
        _pathsRecyclerView.setHasFixedSize(true);
        _pathsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        _pathsList = new ArrayList<>();

        _adapter = new PathListAdapter(_pathsList, this, _user.isAdmin());
        _pathsRecyclerView.setAdapter(_adapter);
        _adapter.setOnPathClickListener(new PathListAdapter.OnPathClickListener() {
            @Override
            public void onPathClick(int position) {
                GotPath currentPath = _pathsList.get(position);
                Intent intent = new Intent(_context, ModifyPathActivity.class);
                intent.putExtra("pathId", currentPath.getPathId());
                intent.putExtra("subrangeId", currentPath.getStartPoint().getSubrange().getSubrangeId());
                intent.putExtra("pathEndPointName", currentPath.getEndPoint().getPointName());
                intent.putExtra("pathStartPointName", currentPath.getStartPoint().getPointName());
                intent.putExtra("pathPointsUp", currentPath.getUpPoints());
                intent.putExtra("pathPointsDown", currentPath.getDownPoints());
                _context.startActivity(intent);
            }
        });

        if (_user.isAdmin())
            initSwipeRemoveCallback(this::removePathAction, _pathsList, _pathsRecyclerView);
    }

    private void removePathAction(GotPath gotPath) {
        _pathsViewModel.removeGotPath(gotPath);
    }
}
