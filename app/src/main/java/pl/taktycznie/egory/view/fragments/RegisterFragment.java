package pl.taktycznie.egory.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.view.fragments.helpers.RegisterParametersPack;

public class RegisterFragment extends Fragment {

    private View view;

    private OnRegisterFragmentActionListener interactionListener;
    private EditText etLogin;
    private EditText etName;
    private EditText etSurname;
    private EditText etPassword1;
    private EditText etPassword2;
    private EditText etBithDate;
    private Button btnRegister;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.register_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews();
    }

    private void initViews() {
        etLogin = view.findViewById(R.id.et_login_register);
        etName = view.findViewById(R.id.et_name);
        etSurname = view.findViewById(R.id.et_surname);
        etPassword1 = view.findViewById(R.id.et_password1);
        etPassword2 = view.findViewById(R.id.et_password2);
        etBithDate = view.findViewById(R.id.et_birth_date);
        btnRegister = view.findViewById(R.id.register_btn);
        btnRegister.setOnClickListener(
                view1 -> interactionListener.onRegisterFragmentAction(
                        new RegisterParametersPack(
                                etLogin.getText().toString(),
                                etName.getText().toString(),
                                etSurname.getText().toString(),
                                etPassword1.getText().toString(),
                                etPassword2.getText().toString(),
                                etBithDate.getText().toString()
                        )
                )
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterFragmentActionListener) {
            interactionListener = (OnRegisterFragmentActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    public interface OnRegisterFragmentActionListener {
        void onRegisterFragmentAction(RegisterParametersPack params);
    }
}
