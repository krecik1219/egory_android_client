package pl.taktycznie.egory.view.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.model.MountainRange;
import pl.taktycznie.egory.view.activities.SubrangeListActivity;

public class RangeListAdapter extends RecyclerView.Adapter<RangeListAdapter.RangeListHolder> {

    private List<MountainRange> _ranges;
    private Context _context;

    public RangeListAdapter(List<MountainRange> ranges, Context context) {
        _ranges = ranges;
        _context = context;
    }

    @NonNull
    @Override
    public RangeListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.range_list_item, parent, false);

        return new RangeListHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RangeListHolder holder, int position) {
        MountainRange range = _ranges.get(position);

        holder.textViewRangeName.setText(range.getRangeName());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_context, SubrangeListActivity.class);
                int rangeId = range.getRangeId();
                String rangeName = range.getRangeName();
                intent.putExtra("rangeId", rangeId);
                intent.putExtra("rangeName", rangeName);
                _context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return _ranges.size();
    }


    public class RangeListHolder extends RecyclerView.ViewHolder{

        private TextView textViewRangeName;
        private ConstraintLayout container;

        public RangeListHolder(View itemView) {
            super(itemView);
            textViewRangeName = itemView.findViewById(R.id.textView_range_name);
            container = itemView.findViewById(R.id.range_item_container);
        }

        public String getTextOnTextViewRangeName() {
            return textViewRangeName.getText().toString();
        }
    }

}
