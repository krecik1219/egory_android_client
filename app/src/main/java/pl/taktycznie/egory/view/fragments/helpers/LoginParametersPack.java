package pl.taktycznie.egory.view.fragments.helpers;

public class LoginParametersPack {

    public String login;
    public String password;

    public LoginParametersPack(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
