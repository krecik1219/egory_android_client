package pl.taktycznie.egory.view.activities;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.R;
import pl.taktycznie.egory.dao.MountainSubrangeDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainSubrange;
import pl.taktycznie.egory.repository.SubrangesRepository;
import pl.taktycznie.egory.view.adapters.SubrangeListAdapter;
import pl.taktycznie.egory.viewmodel.SubrangesViewModel;
import pl.taktycznie.egory.viewmodel.factories.SubrangesViewModelFactory;


public class SubrangeListActivity extends CoreActivity {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MainActivity.class.getSimpleName();

    private RecyclerView _recyclerView;
    private RecyclerView.Adapter _adapter;

    private int _rangeId;
    private List<MountainSubrange> _subrangesList;
    private SubrangesViewModel _subrangesViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subrange_list);

        Intent intent = getIntent();
        _rangeId = intent.getIntExtra("rangeId", 1);

        initView();
        makeViewModelBinding();
    }

    private void initView() {
        _subrangesList = new ArrayList<>();
        _recyclerView = findViewById(R.id.subranges_recycler_view);
        _recyclerView.setHasFixedSize(true);
        _recyclerView.setLayoutManager(new LinearLayoutManager(this));
        _subrangesList = new ArrayList<>();

        _adapter = new SubrangeListAdapter(_subrangesList, this);
        _recyclerView.setAdapter(_adapter);
    }

    private void makeViewModelBinding() {
        MountainSubrangeDao subrangesDao = new MountainSubrangeDao(this);
        SubrangesRepository subrangesRepository = new SubrangesRepository(subrangesDao);
        ViewModelProvider.Factory factory = new SubrangesViewModelFactory(subrangesRepository);
        _subrangesViewModel = ViewModelProviders.of(this, factory).get(SubrangesViewModel.class);
        _subrangesViewModel.init(_rangeId);
        _subrangesViewModel.getMountainSubranges().observe(
                this,
                mountainSubranges -> refreshUI(_subrangesList, mountainSubranges, _adapter));
        _subrangesViewModel.getError().observe(this, error -> {
            if(error != null)
                handleDataAccessErrorInfoInUI(error);
        });
    }
}
