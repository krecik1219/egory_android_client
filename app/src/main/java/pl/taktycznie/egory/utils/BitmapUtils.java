package pl.taktycznie.egory.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import pl.taktycznie.egory.globals.GlobalConstants;

public class BitmapUtils {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + BitmapUtils.class.getSimpleName();

    public static Bitmap decodeBase64JpgString(String base64EncodedImage) {
        byte[] imageBytes = Base64.decode(base64EncodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    public static String encodeBitmapJpgToBase64String(Bitmap image) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteStream);
        byte[] imageBytes = byteStream.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public static String saveBase64ImageOnDevice(String base64EncodedImage, String imageName, String dirPath) {
        if(!createDirIfNotExists(dirPath))
            return null;

        File file = new File(dirPath, imageName);
        Log.d(TAG, "saveBase64ImageOnDevice entered, imageName=" + imageName + ", dir=" + dirPath + " ; absolute path=" + file.getAbsolutePath());
        if (file.exists()) {
            Log.d(TAG, "saveBase64ImageOnDevice, file with name=" + imageName + " already exists in dir=" + dirPath + " omitting save action");
            return file.getAbsolutePath();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            Bitmap image = decodeBase64JpgString(base64EncodedImage);
            image.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Log.d(TAG, "saveBase64ImageOnDevice, image save SUCCESS, absolute path=" + file.getAbsolutePath());
        } catch (Exception e) {
            Log.d(TAG, "saveBase64ImageOnDevice, image save error. File absolute path=" + file.getAbsolutePath() + " ; Exception: " + e.getMessage());
            return null;
        }
        return file.getAbsolutePath();
    }

    private static boolean createDirIfNotExists(String dirPath) {
        final File dir = new File(dirPath);
        if (!dir.exists())
            return dir.mkdirs();
        else
            return true;
    }
}
