package pl.taktycznie.egory.utils;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.Iterator;
import java.util.List;

import pl.taktycznie.egory.helpers.BoolPaired;

public class ColorProvider {
    private static final Integer PREDEFINED_COLORS[] = {
            0xffff0000, 0xff00ff00, 0xff3399ff, 0xffccff33, 0xff990099,
            0xff339966, 0xffcc33ff, 0xffff66cc, 0xff0000ff, 0xff990000,
            0xffff8000, 0xffffcc00, 0xffcc3300, 0xff99ff99, 0xff00cccc
    };

    private List<BoolPaired<Integer>> colors;

    public ColorProvider() {
        this.colors = Stream.of(PREDEFINED_COLORS).map(BoolPaired::new).collect(Collectors.toList());
    }

    public int getColorInt() {
        for (BoolPaired<Integer> cPair : colors) {
            if (!cPair.flag()) {
                cPair.setFlag();
                return cPair.getItem();
            }
        }
        resetAllColors();
        colors.get(0).setFlag();
        return colors.get(0).getItem();
    }

    public void freeColor(int colorInt) {
        Stream.of(colors).filter(value -> value.getItem().equals(colorInt)).findFirst().ifPresent(BoolPaired::unsetFlag);
    }

    private void resetAllColors() {
        for(BoolPaired<Integer> c : colors)
            c.unsetFlag();
    }
}
