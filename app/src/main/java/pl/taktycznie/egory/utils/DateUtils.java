package pl.taktycznie.egory.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static String getCurrentDateString() {
        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return dateFormat.format(date);
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(year, month, day, 0, 0, 0);
        return cal.getTime();
    }

    public static Date getCurrentDatetime() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        cal.set(year, month, day, hour, minute, second);
        return cal.getTime();
    }

    public static Date getDateFromString(String strDate) {
        if("0000-00-00".equals(strDate))
            return null;
        String[] splittedDate = strDate.split("-");
        if(splittedDate.length < 3)
            return null;
        Calendar calendarInstance = Calendar.getInstance();
        try {
            calendarInstance.set(
                    Integer.parseInt(splittedDate[0]),
                    Integer.parseInt(splittedDate[1]),
                    Integer.parseInt(splittedDate[2]),
                    0,
                    0,
                    0);
            return calendarInstance.getTime();
        } catch(NumberFormatException e) {
            return null;
        }
    }

    public static Date parseDatetimeString(String datetimeStr) {
        if("0000-00-00 00:00:00".equals(datetimeStr))
            return null;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            return df.parse(datetimeStr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String datetimeToString(Date datetime) {
        if(datetime == null)
            return "0000-00-00 00:00:00";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return df.format(datetime);
    }

    public static String dateToString(Date date) {
        if(date == null)
            return "0000-00-00";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return df.format(date);
    }
}
