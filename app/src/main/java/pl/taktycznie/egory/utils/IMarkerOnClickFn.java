package pl.taktycznie.egory.utils;

import java.util.List;

public interface IMarkerOnClickFn {

    List<String> onClick();
}
