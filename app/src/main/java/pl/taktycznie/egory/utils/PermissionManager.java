package pl.taktycznie.egory.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PermissionManager {

    private Activity activity;
    private int reqCode;
    private Map<String, Integer> permissionsToRequest;


    @SuppressLint("UseSparseArrays")
    public PermissionManager(Activity activity) {
        this.activity = activity;
        this.reqCode = 0;
        this.permissionsToRequest = new HashMap<>();
    }

    public void checkPermission(String permission) {
        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsToRequest.put(permission, getNextReqCode());
        }
    }

    public void requestPermissions() {
        for(Map.Entry<String, Integer> permission : permissionsToRequest.entrySet()) {
            ActivityCompat.requestPermissions(activity, new String[]{permission.getKey()}, permission.getValue());
        }
    }

    public int howManyInNeedOfRequest() {
        return permissionsToRequest.size();
    }

    public boolean handlePermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(permissions.length == 0)
            return false;
        if(!permissionsToRequest.containsKey(permissions[0]))
            return false;
        if(permissionsToRequest.get(permissions[0]) == null || requestCode != permissionsToRequest.get(permissions[0]))
            return false;
        permissionsToRequest.remove(permissions[0]);
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    private int getNextReqCode() {
        this.reqCode++;
        return reqCode;
    }
}
