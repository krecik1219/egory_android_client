package pl.taktycznie.egory.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

public class PolylineMaker {

    private ColorProvider colorProvider;

    public PolylineMaker(ColorProvider colorProvider) {
        this.colorProvider = colorProvider;
    }

    public PolylineOptions makePolylineOptions(List<LatLng> points) {
        PolylineOptions polylineOptions = new PolylineOptions();
        for(LatLng p: points)
            polylineOptions.add(p);
        polylineOptions.color(colorProvider.getColorInt());
        return polylineOptions;
    }

    public void freeColor(Polyline polyline) {
        colorProvider.freeColor(polyline.getColor());
    }
}
