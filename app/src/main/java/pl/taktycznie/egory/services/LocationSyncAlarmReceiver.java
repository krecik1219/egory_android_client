package pl.taktycznie.egory.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.globals.UserDataSingleton;

public class LocationSyncAlarmReceiver extends BroadcastReceiver {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationSyncAlarmReceiver.class.getSimpleName();

    public static final int REQUEST_CODE = 101;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, " alarm onReceive");
        LocationDataAccessService.startActionSyncWithRemote(context, UserDataSingleton.getInstance().getUserId());
    }
}
