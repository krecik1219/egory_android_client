package pl.taktycznie.egory.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Log;

import com.annimon.stream.Optional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Semaphore;

import pl.taktycznie.egory.dao.LocationDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.tracking.LocationMaintainer;
import pl.taktycznie.egory.model.tracking.LocationPoint;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.model.tracking.PhotoTag;
import pl.taktycznie.egory.repository.persistance.LocationDb;

public class LocationDataAccessService extends IntentService {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationDataAccessService.class.getSimpleName();

    public static class LocationDataAccessParamsQueue {
        private static Semaphore mutex = new Semaphore(1);
        private static Queue<Object> paramsQueue = new LinkedList<>();
        public static void enqueueParams(Object ...params) {
            try {
                mutex.acquire();
                paramsQueue.addAll(Arrays.asList(params));
                mutex.release();
            } catch (InterruptedException e) {
                e.printStackTrace(); // TODO
            }
        }

        public static List<Object> popParams(int paramsNum) {
            List<Object> params = new ArrayList<>();
            try {
                mutex.acquire();
                for(int i = 0; i < paramsNum; i++)
                    params.add(paramsQueue.poll());
                mutex.release();
                return params;
            } catch (InterruptedException e) {
                e.printStackTrace(); // TODO
                return params;
            }
        }
    }

    private static final String ACTION_SAVE_TRACKING_SESSION = "ACTION_SAVE_TRACKING_SESSION";
    private static final String ACTION_SAVE_LOCATION = "ACTION_SAVE_LOCATION";
    private static final String ACTION_UPDATE_SESSION = "ACTION_UPDATE_SESSION";
    private static final String ACTION_GET_LOCATION_SESSIONS = "ACTION_GET_LOCATION_SESSIONS";
    private static final String ACTION_SYNC_WITH_REMOTE = "ACTION_SYNC_WITH_REMOTE";
    private static final String ACTION_BIND_PHOTO_TAG = "ACTION_BIND_PHOTO_TAG";

    private LocationDb locationDb = LocationDb.getInstance();
    private LocationDao locationDao;

    public LocationDataAccessService() {
        super("LocationDataAccessService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationDao = new LocationDao(getApplicationContext());
    }

    public static void startActionSaveTrackingSession(Context context, int userId, LocationSession locationSession) {
        Log.d(TAG, "startActionSaveTrackingSession");
        Intent intent = new Intent(context, LocationDataAccessService.class);
        intent.setAction(ACTION_SAVE_TRACKING_SESSION);
        intent.putExtra("userId", userId);
        LocationDataAccessParamsQueue.enqueueParams(locationSession);
        context.startService(intent);
    }

    public static void startActionSaveLocation(Context context, Location location, LocationSession locationSession) {
        Log.d(TAG, "startActionSaveLocation");
        Intent intent = new Intent(context, LocationDataAccessService.class);
        intent.setAction(ACTION_SAVE_LOCATION);
        LocationDataAccessParamsQueue.enqueueParams(location, locationSession);
        context.startService(intent);
    }

    public static void startActionUpdateSession(Context context, LocationSession locationSession) {
        Log.d(TAG, "startActionUpdateSession");
        Intent intent = new Intent(context, LocationDataAccessService.class);
        intent.setAction(ACTION_UPDATE_SESSION);
        LocationDataAccessParamsQueue.enqueueParams(locationSession);
        context.startService(intent);
    }

    public static void startActionGetLocationSessions(Context context, int userId) {
        Log.d(TAG, "startActionGetLocationSessions");
        Intent intent = new Intent(context, LocationDataAccessService.class);
        intent.setAction(ACTION_GET_LOCATION_SESSIONS);
        intent.putExtra("userId", userId);
        context.startService(intent);
    }

    public static void startActionSyncWithRemote(Context context, int userId) {
        Log.d(TAG, "startActionSyncWithRemote");
        Intent intent = new Intent(context, LocationDataAccessService.class);
        intent.setAction(ACTION_SYNC_WITH_REMOTE);
        intent.putExtra("userId", userId);
        context.startService(intent);
    }

    public static void startActionBindPhotoTagWithLastPoint(Context context, String photoFilePath) {
        Log.d(TAG, "startActionBindPhotoTagWithLastPoint");
        Intent intent = new Intent(context, LocationDataAccessService.class);
        intent.setAction(ACTION_BIND_PHOTO_TAG);
        LocationDataAccessParamsQueue.enqueueParams(photoFilePath);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SAVE_TRACKING_SESSION.equals(action)) {
                final int userId = intent.getIntExtra("userId", 0);
                List<Object> params = LocationDataAccessParamsQueue.popParams(1);
                final LocationSession locationSession = (LocationSession) params.get(0);
                handleActionSaveTrackingSession(userId, locationSession);
            }
            else if(ACTION_SAVE_LOCATION.equals(action)) {
                List<Object> params = LocationDataAccessParamsQueue.popParams(2);
                final Location location = (Location) params.get(0);
                final LocationSession locationSession = (LocationSession) params.get(1);
                handleActionSaveLocation(location, locationSession);
            }
            else if(ACTION_UPDATE_SESSION.equals(action)) {
                List<Object> params = LocationDataAccessParamsQueue.popParams(1);
                final LocationSession locationSession = (LocationSession) params.get(0);
                handleActionUpdateSession(locationSession);
            }
            else if(ACTION_GET_LOCATION_SESSIONS.equals(action)) {
                int userId = intent.getIntExtra("userId", 0);
                handleActionGetLocationSessions(userId);
            }
            else if(ACTION_SYNC_WITH_REMOTE.equals(action)) {
                int userId = intent.getIntExtra("userId", 0);
                handleActionSyncWithRemote(userId);
            }
            else if(ACTION_BIND_PHOTO_TAG.equals(action)) {
                List<Object> params = LocationDataAccessParamsQueue.popParams(1);
                String photoFilePath = (String) params.get(0);
                handleBindPhotoTag(photoFilePath);
            }
        }
    }

    private void handleActionSyncWithRemote(int userId) {
        // TODO sync photo tags!!!
        Log.d(TAG, "handleActionSyncWithRemote");
        LocationDb.lock();
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        Queue<LocationSession> syncAwaiting = locationMaintainer.getSyncAwaitingQueue();
        Log.d(TAG, "sync awaiting: " + syncAwaiting);
        if(syncAwaiting.size() > 0) {
            Optional<List<LocationSession>> result = locationDao.syncTrackingSessionsByUserId(userId, syncAwaiting);
            if (result.isPresent()) {
                locationMaintainer.clearSyncAwaiting();
                locationMaintainer.setLocationSessions(result.get());
            }
            locationDb.editNotify();
        }
        LocationDb.release();
    }

    private void handleActionGetLocationSessions(int userId) {
        Log.d(TAG, "handleActionGetLocationSessions");
        Optional<List<LocationSession>> result = locationDao.getTrackingSessionsByUserSync(userId);
        if(result.isPresent()) {
            LocationDb.lock("handleActionGetLocationSessions");
            LocationMaintainer locationMaintainer = locationDb.getDataValue();
            locationMaintainer.setLocationSessions(result.get());
        }
        locationDb.editNotify();
        LocationDb.release("handleActionGetLocationSessions");
    }

    private void handleActionUpdateSession(LocationSession locationSession) {
        Log.d(TAG, "handleActionUpdateSession");
        LocationDb.lock("handleActionUpdateSession");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        boolean result = locationDao.updateTrackingSessionSync(locationSession);
        if(!result) {
            locationMaintainer.pushCurrentToSyncAwaiting();
        }
        locationDb.editNotify();
        LocationDb.release("handleActionUpdateSession");
    }

    private void handleActionSaveLocation(Location location, LocationSession locationSession) {
        Log.d(TAG, "handleActionSaveLocation");
        LocationDb.lock("handleActionSaveLocation");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        int orderNum = locationMaintainer.getNextOrderNum();
        LocationPoint point = LocationPoint.getPointFromLocation(location, orderNum);
        locationMaintainer.handleNewLocationProvided(point);
        if(locationMaintainer.getCurrentSession().getLocationSessionId() == 0) {
            locationMaintainer.pushCurrentToSyncAwaiting(point);
            locationDb.editNotify();
            LocationDb.release("handleActionSaveLocation, currentSessionId=0");
            return;
        }
        Optional<Integer> result = locationDao.insertLocationPointToTrackingSessionSync(point, locationSession);
        if(result.isEmpty()) {
            locationMaintainer.pushCurrentToSyncAwaiting(point);
        }
        else {
            point.setTrackingPointId(result.get());
        }
        locationDb.editNotify();
        LocationDb.release("handleActionSaveLocation");
    }

    private void handleActionSaveTrackingSession(int userId, LocationSession locationSession) {
        Log.d(TAG, "handleActionSaveTrackingSession");
        LocationDb.lock("handleActionSaveTrackingSession");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        Optional<Integer> result = locationDao.insertTrackingSessionSync(userId, locationSession);
        if(result.isPresent()) {
            Log.d(TAG, "handleActionSaveTrackingSession, id: " + result.get());
            locationSession.setLocationSessionId(result.get());
            Log.d(TAG, "handleActionSaveTrackingSession, id session: " + locationMaintainer.getCurrentSession().getLocationSessionId());
        }
        else {
            locationMaintainer.pushCurrentToSyncAwaiting();
        }
        locationDb.editNotify();
        LocationDb.release("handleActionSaveTrackingSession");
    }

    private void handleBindPhotoTag(String photoFilePath) {
        Log.d(TAG, "handleBindPhotoTag");
        LocationDb.lock("handleBindPhotoTag");
        LocationMaintainer locationMaintainer = locationDb.getDataValue();
        PhotoTag tag = new PhotoTag(photoFilePath);
        if(locationMaintainer.setPhotoTagTLastSessionPoint(tag)) {
            LocationPoint lastPoint = locationMaintainer.getCurrentSession().getLastPoint();
            Log.d(TAG, "handleBindPhotoTag, last point id: " + lastPoint.getTrackingPointId());
            Bitmap photo = BitmapFactory.decodeFile(photoFilePath);
            Optional<Integer> result = locationDao.insertLocationPointPhotoTagSync(lastPoint, photo);
            if(result.isPresent()) {
                Log.d(TAG, "handleBindPhotoTag, result present: " + result.get());
                tag.setPhotoTagId(result.get());
            }
            else {
                Log.d(TAG, "handleBindPhotoTag, result MISSING");
            }
            locationDb.editNotify();
            LocationDb.release("handleBindPhotoTag, successful local bind");
            return;
        }
        locationDb.editNotify();
        LocationDb.release("handleBindPhotoTag");
    }
}
