package pl.taktycznie.egory.services.talkers;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.globals.UserDataSingleton;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.repository.LocationRepository;
import pl.taktycznie.egory.repository.persistance.LocationDb;
import pl.taktycznie.egory.services.LocationDataAccessService;


public class LocationModelTalker {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationModelTalker.class.getSimpleName();

    private Context context;

    public LocationModelTalker(Context context) {
        this.context = context;
    }

    public void provide(Location location) {
        Log.d(TAG, " provide: " + location);
        LocationDb.lock("provide");
        LocationSession currentSession = LocationDb.getInstance().getDataValue().getCurrentSession();
        LocationDb.release("provide");
        LocationDataAccessService.startActionSaveLocation(context, location, currentSession);
    }

    public void provideFirst(Location location) {
        Log.d(TAG, " provideFirst: " + location);
        LocationSession currentSession = LocationDb.getInstance().getDataValue().getCurrentSession();
        LocationDataAccessService.startActionSaveLocation(context, location, currentSession);
    }
}
