package pl.taktycznie.egory.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import pl.taktycznie.egory.dao.LocationDao;
import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.tracking.LocationPoint;
import pl.taktycznie.egory.services.talkers.LocationModelTalker;
import pl.taktycznie.egory.repository.LocationRepository;

public class LocationProvidingService extends Service {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationProvidingService.class.getSimpleName();

    private static final int LOCATION_REQ_PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY;
    private static final int LOCATION_UPDATE_INTERVAL = 5000;  // ms
    private static final int LOCATION_FASTEST_UPDATE_INTERVAL = 5000;  // ms

    private static final double EARTH_RADIUS_METERS = 6371e3;
    private static final double MIN_SIGNIFICANT_DISTANCE_METERS = 7.0;  // meters

    private boolean isFirstReadout;
    private LocationModelTalker locationModelTalker;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location lastLocation;
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if(isFirstReadout) {
                Log.d(TAG, "service.onLocationChanged(): first readout, saving location: " + locationResult.getLastLocation());
                lastLocation = new Location(locationResult.getLastLocation());
                locationModelTalker.provideFirst(locationResult.getLastLocation());
                isFirstReadout = false;
            }
            else {
                Location candidate = locationResult.getLastLocation();
                if(locationChangeIsSignificant(candidate)) {
                    Log.d(TAG, "service.onLocationChanged() candidate SIGNIFICANT change (>= " + MIN_SIGNIFICANT_DISTANCE_METERS +"): " + candidate);
                    lastLocation = new Location(candidate);
                    locationModelTalker.provide(candidate);
                }
                else {
                    Log.d(TAG, "service.onLocationChanged() candidate is NOT significant change (< " + MIN_SIGNIFICANT_DISTANCE_METERS +"): " + candidate);
                }
            }
        }
    };

    private boolean locationChangeIsSignificant(Location candidate) {

        double latDistance = Math.toRadians(lastLocation.getLatitude() - candidate.getLatitude());
        double lngDistance = Math.toRadians(lastLocation.getLongitude() - candidate.getLongitude());

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lastLocation.getLatitude())) * Math.cos(Math.toRadians(candidate.getLatitude()))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double distance = Math.round(EARTH_RADIUS_METERS * c);

        return distance >= MIN_SIGNIFICANT_DISTANCE_METERS;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isFirstReadout = true;
        locationModelTalker = new LocationModelTalker(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocationRequest req = initLocationProviderClient();
        requestForLocationUpdates(req);

        return START_NOT_STICKY;
    }

    @SuppressLint("MissingPermission")
    private void requestForLocationUpdates(LocationRequest req) {
        fusedLocationProviderClient.requestLocationUpdates(req, locationCallback, null);
    }

    private LocationRequest initLocationProviderClient() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LOCATION_REQ_PRIORITY);
        locationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(LOCATION_FASTEST_UPDATE_INTERVAL);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        return locationRequest;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "service.onDestroy()");
        unsubscribeFromLocationUpdates();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("This service is not meant to be used with direct binding." +
                "Location data updates are available through particular ViewModel and Model classes");
    }

    private void unsubscribeFromLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
}
