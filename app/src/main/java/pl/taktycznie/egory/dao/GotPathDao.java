package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.Resource;
import pl.taktycznie.egory.model.GotPath;
import pl.taktycznie.egory.model.MountainPoint;

public class GotPathDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + GotPathDao.class.getSimpleName();
    private static final String FETCH_GOT_PATHS_BY_SUBRANGE_URL = "fetch-got-paths-by-subrange.php";
    private static final String FETCH_GOT_PATHS_BY_START_POINT_URL = "fetch-got-paths-by-start-point.php";
    private static final String FETCH_GOT_PATHS_BY_END_POINT_URL = "fetch-got-paths-by-end-point.php";
    private static final String INSERT_GOT_PATH_URL = "insert-got-path.php";
    private static final String DELETE_GOT_PATH_URL = "delete-got-path.php";
    private static final String UPDATE_GOT_PATH_URL = "update-got-path.php";

    public GotPathDao(Context context) {
        super(context);
    }

    public void getGotPathsBySubrange(int subrangeId,
                                      OnResourceDataPreparedListener<List<GotPath>> onResourceDataPreparedListener,
                                      OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getGotPathsBySubrange", "entered");
        String requestUrl = DaoConstants.FETCH_FACADE_PREFIX
                        + FETCH_GOT_PATHS_BY_SUBRANGE_URL
                        + "?subrangeId="
                        + subrangeId;
        getGotPath(requestUrl, onResourceDataPreparedListener, onResponseErrorListener);// tu cos moze byc zle
    }

    public void getGotPathsByStartPoint(String startPointName,
                                      OnResourceDataPreparedListener<List<GotPath>> onResourceDataPreparedListener,
                                      OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getGotPathsBySubrange", "entered");
        String requestUrl = DaoConstants.FETCH_FACADE_PREFIX
                        + FETCH_GOT_PATHS_BY_START_POINT_URL
                        + "?startPointName="
                        + startPointName;
        String requestUrl2 = DaoConstants.FETCH_FACADE_PREFIX
                + FETCH_GOT_PATHS_BY_END_POINT_URL
                + "?endPointName="
                + startPointName;

        getGotPath2(requestUrl,requestUrl2, onResourceDataPreparedListener, onResponseErrorListener);
    }

    public void getGotPathsByEndPoint(String endPointName,
                                        OnResourceDataPreparedListener<List<GotPath>> onResourceDataPreparedListener,
                                        OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getGotPathsBySubrange", "entered");
        String requestUrl = DaoConstants.FETCH_FACADE_PREFIX
                        + FETCH_GOT_PATHS_BY_END_POINT_URL
                        + "?endPointName="
                        + endPointName;
        getGotPath(requestUrl, onResourceDataPreparedListener, onResponseErrorListener);// tu cos moze byc zle
    }


    private void getGotPath2(String url,String url2,
                            OnResourceDataPreparedListener<List<GotPath>> onResourceDataPreparedListener,
                            OnResponseErrorListener onResponseErrorListener) {
        List<GotPath> gotPaths = new ArrayList<>();
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {

            Log.d(TAG + " getGotPath", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject gotPath = fetchResult.getJSONObject(i);
                int pathId = gotPath.getInt("path_id");
                int startPointId = gotPath.getInt("start_point_id");
                String startPointName = gotPath.getString("start_point_name");
                int startPointSubrangeId = gotPath.getInt("start_point_subrange_id");
                String startPointSubrangeName = gotPath.getString("start_point_subrange_name");
                int startPointRangeId = gotPath.getInt("start_point_range_id");
                String startPointRangeName = gotPath.getString("start_point_range_name");
                double startPointLatitude = gotPath.getDouble("start_point_latitude");
                double startPointLongitude = gotPath.getDouble("start_point_longitude");
                double startPointAltitude = gotPath.getDouble("start_point_altitude");
                int endPointSubrangeId = gotPath.getInt("end_point_subrange_id");
                String endPointSubrangeName = gotPath.getString("end_point_subrange_name");
                int endPointRangeId = gotPath.getInt("end_point_range_id");
                String endPointRangeName = gotPath.getString("end_point_range_name");
                int endPointId = gotPath.getInt("end_point_id");
                String endPointName = gotPath.getString("end_point_name");
                double endPointLatitude = gotPath.getDouble("end_point_latitude");
                double endPointLongitude = gotPath.getDouble("end_point_longitude");
                double endPointAltitude = gotPath.getDouble("end_point_altitude");
                int upPoints = gotPath.getInt("up_points");
                int downPoints = gotPath.getInt("down_points");
                gotPaths.add(
                        new GotPath(
                                pathId,
                                new MountainPoint(
                                        startPointId,
                                        startPointName,
                                        startPointSubrangeId,
                                        startPointSubrangeName,
                                        startPointRangeId,
                                        startPointRangeName,
                                        startPointLatitude,
                                        startPointLongitude,
                                        startPointAltitude),
                                new MountainPoint(
                                        endPointId,
                                        endPointName,
                                        endPointSubrangeId,
                                        endPointSubrangeName,
                                        endPointRangeId,
                                        endPointRangeName,
                                        endPointLatitude,
                                        endPointLongitude,
                                        endPointAltitude),
                                upPoints,
                                downPoints));
            }
            return gotPaths;
        });

        fetch(url2, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {

            Log.d(TAG + " getGotPath", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject gotPath = fetchResult.getJSONObject(i);
                int pathId = gotPath.getInt("path_id");
                int startPointId = gotPath.getInt("start_point_id");
                String startPointName = gotPath.getString("start_point_name");
                int startPointSubrangeId = gotPath.getInt("start_point_subrange_id");
                String startPointSubrangeName = gotPath.getString("start_point_subrange_name");
                int startPointRangeId = gotPath.getInt("start_point_range_id");
                String startPointRangeName = gotPath.getString("start_point_range_name");
                double startPointLatitude = gotPath.getDouble("start_point_latitude");
                double startPointLongitude = gotPath.getDouble("start_point_longitude");
                double startPointAltitude = gotPath.getDouble("start_point_altitude");
                int endPointSubrangeId = gotPath.getInt("end_point_subrange_id");
                String endPointSubrangeName = gotPath.getString("end_point_subrange_name");
                int endPointRangeId = gotPath.getInt("end_point_range_id");
                String endPointRangeName = gotPath.getString("end_point_range_name");
                int endPointId = gotPath.getInt("end_point_id");
                String endPointName = gotPath.getString("end_point_name");
                double endPointLatitude = gotPath.getDouble("end_point_latitude");
                double endPointLongitude = gotPath.getDouble("end_point_longitude");
                double endPointAltitude = gotPath.getDouble("end_point_altitude");
                int upPoints = gotPath.getInt("up_points");
                int downPoints = gotPath.getInt("down_points");
                gotPaths.add(
                        new GotPath(
                                pathId,
                                new MountainPoint(
                                        endPointId,
                                        endPointName,
                                        endPointSubrangeId,
                                        endPointSubrangeName,
                                        endPointRangeId,
                                        endPointRangeName,
                                        endPointLatitude,
                                        endPointLongitude,
                                        endPointAltitude),
                                new MountainPoint(
                                        startPointId,
                                        startPointName,
                                        startPointSubrangeId,
                                        startPointSubrangeName,
                                        startPointRangeId,
                                        startPointRangeName,
                                        startPointLatitude,
                                        startPointLongitude,
                                        startPointAltitude),
                                downPoints,
                                upPoints
                                ));
            }
            return gotPaths;
        });

    }
    private void getGotPath(String url,
                             OnResourceDataPreparedListener<List<GotPath>> onResourceDataPreparedListener,
                             OnResponseErrorListener onResponseErrorListener) {
        List<GotPath> gotPaths = new ArrayList<>();
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {

            Log.d(TAG + " getGotPath", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject gotPath = fetchResult.getJSONObject(i);
                int pathId = gotPath.getInt("path_id");
                int startPointId = gotPath.getInt("start_point_id");
                String startPointName = gotPath.getString("start_point_name");
                int startPointSubrangeId = gotPath.getInt("start_point_subrange_id");
                String startPointSubrangeName = gotPath.getString("start_point_subrange_name");
                int startPointRangeId = gotPath.getInt("start_point_range_id");
                String startPointRangeName = gotPath.getString("start_point_range_name");
                double startPointLatitude = gotPath.getDouble("start_point_latitude");
                double startPointLongitude = gotPath.getDouble("start_point_longitude");
                double startPointAltitude = gotPath.getDouble("start_point_altitude");
                int endPointSubrangeId = gotPath.getInt("end_point_subrange_id");
                String endPointSubrangeName = gotPath.getString("end_point_subrange_name");
                int endPointRangeId = gotPath.getInt("end_point_range_id");
                String endPointRangeName = gotPath.getString("end_point_range_name");
                int endPointId = gotPath.getInt("end_point_id");
                String endPointName = gotPath.getString("end_point_name");
                double endPointLatitude = gotPath.getDouble("end_point_latitude");
                double endPointLongitude = gotPath.getDouble("end_point_longitude");
                double endPointAltitude = gotPath.getDouble("end_point_altitude");
                int upPoints = gotPath.getInt("up_points");
                int downPoints = gotPath.getInt("down_points");
                gotPaths.add(
                        new GotPath(
                                pathId,
                                new MountainPoint(
                                        startPointId,
                                        startPointName,
                                        startPointSubrangeId,
                                        startPointSubrangeName,
                                        startPointRangeId,
                                        startPointRangeName,
                                        startPointLatitude,
                                        startPointLongitude,
                                        startPointAltitude),
                                new MountainPoint(
                                        endPointId,
                                        endPointName,
                                        endPointSubrangeId,
                                        endPointSubrangeName,
                                        endPointRangeId,
                                        endPointRangeName,
                                        endPointLatitude,
                                        endPointLongitude,
                                        endPointAltitude),
                                upPoints,
                                downPoints));
            }
            return gotPaths;
        });
    }
    public void insertGotPath(GotPath gotPath,
                              OnResourceDataPreparedListener<Integer> onResourceDataPreparedListener,
                              OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " insertGotPath()", "entered");
        String url = DaoConstants.INSERT_FACADE_PREFIX + INSERT_GOT_PATH_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("startPointId", gotPath.getStartPoint().getPointId());
        params.put("endPointId", gotPath.getEndPoint().getPointId());
        params.put("upPoints", gotPath.getUpPoints());
        params.put("downPoints", gotPath.getDownPoints());
        insert(url, params, onResourceDataPreparedListener, onResponseErrorListener, insertResult -> insertResult.getInt("lastId"));
    }

    public void deleteGotPath(GotPath gotPath,
                              OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                              OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " deleteGotPath()", "entered");
        String url = DaoConstants.DELETE_FACADE_PREFIX + DELETE_GOT_PATH_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("pathId", gotPath.getPathId());
        delete(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }

    public void updateGotPath(GotPath gotPath,
                              int newUpPoints,
                              int newDownPoints,
                              OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                              OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " updateGotPath()", "entered");
        String url = DaoConstants.UPDATE_FACADE_PREFIX + UPDATE_GOT_PATH_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("pathId", gotPath.getPathId());
        params.put("upPoints", newUpPoints);
        params.put("downPoints", newDownPoints);
        update(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }
}
