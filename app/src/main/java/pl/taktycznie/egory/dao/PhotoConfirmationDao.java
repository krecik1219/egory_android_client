package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.PhotoConfirmation;
import pl.taktycznie.egory.utils.BitmapUtils;

public class PhotoConfirmationDao extends AbstractDao{

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + PhotoConfirmationDao.class.getSimpleName();
    private static final String FETCH_PHOTO_CONFIRMATIONS_BY_TRIP_PATH_URL = "fetch-photo-confirmations-by-trip-path.php";
    private static final String INSERT_PHOTO_CONFIRMATION_URL = "insert-photo-confirmation.php";

    public PhotoConfirmationDao(Context context) {
        super(context);
    }

    public void getPhotoConfirmationsByTripPath(int tripPathId,
                                                OnResourceDataPreparedListener<List<PhotoConfirmation>> onResourceDataPreparedListener,
                                                OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getPConfirmsByTripPath", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX +
                FETCH_PHOTO_CONFIRMATIONS_BY_TRIP_PATH_URL +
                "?tripPathId=" + tripPathId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<PhotoConfirmation> photoConfirmations = new ArrayList<>();
            Log.d(TAG + " getPConfirmsByTripPath", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject photoConfirmation = fetchResult.getJSONObject(i);
                int photoConfirmationId = photoConfirmation.getInt("photo_confirmation_id");
                String base64EncodedImage = photoConfirmation.getString("image");
                int parentTripPathId = photoConfirmation.getInt("trip_path_id");
                photoConfirmations.add(
                        new PhotoConfirmation(
                                photoConfirmationId,
                                BitmapUtils.decodeBase64JpgString(base64EncodedImage),
                                parentTripPathId
                        )
                );
            }
            return photoConfirmations;
        });
    }

    public void insertPhotoConfirmationToDb(PhotoConfirmation photoConfirmation,
                                            OnResourceDataPreparedListener<Integer> onResourceDataPreparedListener,
                                            OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " insertPConfirm", "entered");
        String url = DaoConstants.INSERT_FACADE_PREFIX + INSERT_PHOTO_CONFIRMATION_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("image", BitmapUtils.encodeBitmapJpgToBase64String(photoConfirmation.getImage()));
        params.put("tripPathId", photoConfirmation.getTripPathId());
        insert(url, params, onResourceDataPreparedListener, onResponseErrorListener, insertResult -> insertResult.getInt("lastId"));
    }
}
