package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.GotBadge;
import pl.taktycznie.egory.utils.DateUtils;

public class BadgeDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + BadgeDao.class.getSimpleName();
    private static final String FETCH_BADGES_BY_USER_URL = "fetch-badges-by-user.php";
    private static final String INSERT_USER_BADGE_URL = "insert-user-badge.php";

    public BadgeDao(Context context) {
        super(context);
    }

    public void getBadgesByUser(int userId,
                               OnResourceDataPreparedListener<List<GotBadge>> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getBadgesByUser()", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_BADGES_BY_USER_URL + "?userId=" + userId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<GotBadge> badges = new ArrayList<>();
            Log.d(TAG + " getTripsByUser()", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject badge = fetchResult.getJSONObject(i);
                int badgeId = badge.getInt("badge_id");
                String badgeName = badge.getString("badge_name");
                int requiredPoints = badge.getInt("required_points");
                Date awardDate = DateUtils.getDateFromString(badge.getString("award_date"));
                badges.add(new GotBadge(badgeId, badgeName, requiredPoints, awardDate));
            }
            return badges;
        });
    }

    public void insertUserBadgeToDb(GotBadge badge,
                               int userId,
                               OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " insertUserBadgeToDb()", "entered");
        String url = DaoConstants.INSERT_FACADE_PREFIX + INSERT_USER_BADGE_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("badgeId", badge.getBadgeId());
        params.put("userId", userId);
        params.put("awardDate", DateUtils.datetimeToString(badge.getAwardDate()));
        insert(url, params, onResourceDataPreparedListener, onResponseErrorListener, insertResult -> new EmptyData());
    }
}
