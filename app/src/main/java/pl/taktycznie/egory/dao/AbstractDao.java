package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.Resource;

public abstract class AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + AbstractDao.class.getSimpleName();

    protected Context context;

    public AbstractDao(Context context) {
        this.context = context;
    }

    public boolean hasAnyErrorOccured(JSONObject responseJson) {
        Log.d("AbstractDao anyError?", "response: " + responseJson);
        Log.d("AbstractDao anyError?", "hasAnyError entered");
        try {
            JSONArray errors = responseJson.getJSONArray("errors");
            if(errors.length() > 0) {
                Log.d("AbstractDao anyError?", "if passed returns true");
                Log.d("AbstractDao anyError?", errors.getString(0));
                return true;
            }
        } catch (JSONException e) {
            Log.d("AbstractDao anyError?", "exception returns true");
            return true;
        }
        Log.d("AbstractDao anyError?", "end of method returns false");
        return false;
    }

    protected <Model> void fetch(String url,
                                 OnResourceDataPreparedListener<Model> onResourceDataPreparedListener,
                                 OnResponseErrorListener onResponseErrorListener,
                                 FetchResultParser<Model> fetchResultParser,
                                 int method,
                                 JSONObject requestParams) {
        Log.d(TAG + " fetch", "entered");
        String requestUrl =
                DaoConstants.SERVER_URL + url;
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request =
                new JsonObjectRequest(method, requestUrl, requestParams, jsonResponse -> {
                    try {
                        if (hasAnyErrorOccured(jsonResponse)) {
                            Resource<Model> resource = new Resource<>(
                                    null,
                                    new DataAccessError("Server error")
                            );
                            onResourceDataPreparedListener.onResourceDataPrepared(resource);
                            return;
                        }
                        JSONArray fetchResult = jsonResponse.getJSONArray("fetchResult");
                        Log.d(TAG + " fetch", "JSON response: " + fetchResult.toString());
                        Model m = fetchResultParser.fetchResultParse(fetchResult);
                        Resource<Model> resource = new Resource<>(m, null);
                        onResourceDataPreparedListener.onResourceDataPrepared(resource);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG + " fetch", "JSON Exception: " + e.getMessage());
                        Resource<Model> resource = new Resource<>(
                                null,
                                new DataAccessError("Server data error")
                        );
                        onResourceDataPreparedListener.onResourceDataPrepared(resource);
                    }
                }, error -> {
                    Log.e(TAG + " fetch", "Network error");
                    DataAccessError dataAccessError;
                    if(error instanceof TimeoutError || error instanceof NoConnectionError)
                        dataAccessError = new DataAccessError("Connection timeout :(");
                    else
                        dataAccessError = new DataAccessError(error.getMessage());
                    onResponseErrorListener.onResponseError(dataAccessError);
                });
        requestQueue.add(request);
    }

    protected <Model> void fetch(String url,
                                 OnResourceDataPreparedListener<Model> onResourceDataPreparedListener,
                                 OnResponseErrorListener onResponseErrorListener,
                                 FetchResultParser<Model> fetchResultParser) {
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResultParser, Request.Method.GET, null);
    }

    protected <InsertionResult> void insert(String url,
                                            Map<String, ?> params,
                                            OnResourceDataPreparedListener<InsertionResult> onResourceDataPreparedListener,
                                            OnResponseErrorListener onResponseErrorListener,
                                            InsertResultParser<InsertionResult> insertResultParser) {
        Log.d(TAG + " insert()", "entered");
        String requestUrl = DaoConstants.SERVER_URL + url;
        JSONObject requestParams = new JSONObject();
        try {
            for(Map.Entry<String, ?> entry: params.entrySet()) {
                requestParams.put(entry.getKey(), entry.getValue());
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + " insert()", "Exception during setting parameters: " + e.getMessage());
            Resource<InsertionResult> resource = new Resource<>(
                    null,
                    new DataAccessError("Parameters error")
            );
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
            return;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, jsonResponse -> {
            try {
                if(hasAnyErrorOccured(jsonResponse)) {
                    Resource<InsertionResult> resource = new Resource<>(
                            null,
                            new DataAccessError("Server error")
                    );
                    onResourceDataPreparedListener.onResourceDataPrepared(resource);
                    return;
                }
                Log.d(TAG + " insert()", "JSON response: " + jsonResponse.toString());
                JSONObject insertResult = jsonResponse.getJSONObject("insertResult");
                InsertionResult insertionResult = insertResultParser.insertResultParse(insertResult);
                Resource<InsertionResult> resource = new Resource<>(insertionResult, null);
                onResourceDataPreparedListener.onResourceDataPrepared(resource);
            } catch(JSONException e) {
                e.printStackTrace();
                Log.e(TAG + " insert()", "Exception: " + e.getMessage());
                Resource<InsertionResult> resource = new Resource<>(
                        null,
                        new DataAccessError("Server data error")
                );
                onResourceDataPreparedListener.onResourceDataPrepared(resource);
            }
        }, error -> {
            Log.e(TAG + "insert", "Network error");
            DataAccessError dataAccessError;
            if(error instanceof TimeoutError || error instanceof NoConnectionError)
                dataAccessError = new DataAccessError("Connection timeout :(");
            else
                dataAccessError = new DataAccessError(error.getMessage());
            onResponseErrorListener.onResponseError(dataAccessError);
        });
        requestQueue.add(request);
    }

    public void delete(String url,
                       Map<String, ?> params,
                       OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                       OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " delete()", "entered");
        String requestUrl = DaoConstants.SERVER_URL + url;
        JSONObject requestParams = new JSONObject();
        try {
            for(Map.Entry<String, ?> entry: params.entrySet()) {
                requestParams.put(entry.getKey(), entry.getValue());
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + " deleteTripPath()", "Exception during setting parameters: " + e.getMessage());
            Resource<EmptyData> resource = new Resource<>(
                    null,
                    new DataAccessError("Parameters error")
            );
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
            return;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, jsonResponse -> {
            Log.d(TAG + " delete()", "JSON response: " + jsonResponse.toString());
            Resource<EmptyData> resource;
            if(hasAnyErrorOccured(jsonResponse)) {
                resource = new Resource<>(
                        null,
                        new DataAccessError("Server error")
                );
            }
            else
                resource = new Resource<>(new EmptyData(), null);
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
        }, error -> {
            Log.e(TAG + "delete", "Network error");
            DataAccessError dataAccessError;
            if(error instanceof TimeoutError || error instanceof NoConnectionError)
                dataAccessError = new DataAccessError("Connection timeout :(");
            else
                dataAccessError = new DataAccessError(error.getMessage());
            onResponseErrorListener.onResponseError(dataAccessError);
        });
        requestQueue.add(request);
    }

    public void update(String url,
                       Map<String, ?> params,
                       OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                       OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " update()", "entered");
        JSONObject requestParams = new JSONObject();
        try {
            for(Map.Entry<String, ?> entry: params.entrySet()) {
                requestParams.put(entry.getKey(), entry.getValue());
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + " update()", "Exception during setting parameters: " + e.getMessage());
            Resource<EmptyData> resource = new Resource<>(
                    null,
                    new DataAccessError("Parameters error")
            );
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
            return;
        }
        Log.d(TAG + " update", "request params: " + requestParams);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestParams, jsonResponse -> {
            Log.d(TAG + " update()", "JSON response: " + jsonResponse.toString());
            Resource<EmptyData> resource;
            if(hasAnyErrorOccured(jsonResponse)) {
                resource = new Resource<>(
                        null,
                        new DataAccessError("Server error")
                );
            }
            else
                resource = new Resource<>(new EmptyData(), null);
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
        }, error -> {
            Log.e(TAG + "update", "Network error");
            DataAccessError dataAccessError;
            if(error instanceof TimeoutError || error instanceof NoConnectionError)
                dataAccessError = new DataAccessError("Connection timeout :(");
            else
                dataAccessError = new DataAccessError(error.getMessage());
            onResponseErrorListener.onResponseError(dataAccessError);
        });
        requestQueue.add(request);
    }

    protected interface FetchResultParser<Model> {
        Model fetchResultParse(JSONArray fetchResult) throws JSONException;
    }

    protected interface InsertResultParser<Model> {
        Model insertResultParse(JSONObject insertResult) throws JSONException;
    }

    public interface OnResourceDataPreparedListener<DataType> {
        void onResourceDataPrepared(Resource<DataType> resource);
    }

    public interface OnResponseErrorListener {
        void onResponseError(DataAccessError error);
    }
}
