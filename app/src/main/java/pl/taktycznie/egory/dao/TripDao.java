package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.Trip;

public class TripDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripDao.class.getSimpleName();
    private static final String FETCH_TRIPS_BY_USER_URL = "fetch-trips-by-user.php";
    private static final String INSERT_TRIP_URL = "insert-trip.php";
    private static final String DELETE_TRIP_URL = "delete-trip.php";
    private static final String UPDATE_TRIP_URL = "update-trip.php";

    public TripDao(Context context) {
        super(context);
    }

    public void getTripsByUser(int userId,
                               OnResourceDataPreparedListener<List<Trip>> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getTripsByUser()", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_TRIPS_BY_USER_URL + "?userId=" + userId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<Trip> trips = new ArrayList<>();
            Log.d(TAG + " getTripsByUser()", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject trip = fetchResult.getJSONObject(i);
                int tripId = trip.getInt("trip_id");
                String tripName = trip.getString("trip_name");
                String creationDateString = trip.getString("creation_date");
                int tripStatusId = trip.getInt("trip_status_id");
                String tripStatusName = trip.getString("trip_status_name");
                trips.add(new Trip(tripId, tripName, creationDateString, tripStatusId, tripStatusName));
            }
            return trips;
        });
    }

    public void insertTripToDb(Trip trip,
                               int userId,
                               OnResourceDataPreparedListener<Integer> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " insertTripToDb()", "entered");
        String url = DaoConstants.INSERT_FACADE_PREFIX + INSERT_TRIP_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("tripName", trip.getTripName());
        params.put("userId", userId);
        insert(url, params, onResourceDataPreparedListener, onResponseErrorListener, insertResult -> insertResult.getInt("lastId"));
    }

    public void deleteTrip(Trip trip,
                           OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                           OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " deleteTrip()", "entered");
        String url = DaoConstants.DELETE_FACADE_PREFIX + DELETE_TRIP_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("tripId", trip.getTripId());
        delete(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }

    public void updateTrip(Trip trip,
                           String newTripName,
                           OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                           OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " updateTrip()", "entered");
        String url = DaoConstants.UPDATE_FACADE_PREFIX + UPDATE_TRIP_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("tripId", trip.getTripId());
        params.put("tripName", newTripName);
        update(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }
}
