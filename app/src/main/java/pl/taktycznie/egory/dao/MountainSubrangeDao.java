package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainSubrange;


public class MountainSubrangeDao extends AbstractDao{

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MountainSubrangeDao.class.getSimpleName();
    private static final String FETCH_ALL_MOUNTAIN_SUBRANGES_URL = "fetch-all-mountain-subranges.php";
    private static final String FETCH_MOUNTAIN_SUBRANGES_BY_RANGE_URL = "fetch-mountain-subranges-by-range.php";

    public MountainSubrangeDao(Context context) {
        super(context);
    }

    public void getAllMountainSubranges(OnResourceDataPreparedListener<List<MountainSubrange>> onResourceDataPreparedListener,
                                        OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getAllSubranges", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_ALL_MOUNTAIN_SUBRANGES_URL;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainSubrange> mountainSubranges = new ArrayList<>();
            Log.d(TAG + " getAllSubranges()", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainRange = fetchResult.getJSONObject(i);
                int subrangeId = mountainRange.getInt("subrange_id");
                String subrangeName = mountainRange.getString("subrange_name");
                int rangeId = mountainRange.getInt("range_id");
                String rangeName = mountainRange.getString("range_name");
                mountainSubranges.add(new MountainSubrange(subrangeId, subrangeName, rangeId, rangeName));
            }
            return mountainSubranges;
        });
    }

    public void getMountainSubrangesByRange(int rangeId,
                                            OnResourceDataPreparedListener<List<MountainSubrange>> onResourceDataPreparedListener,
                                            OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getSubrangesByRange", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_MOUNTAIN_SUBRANGES_BY_RANGE_URL + "?rangeId=" + rangeId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainSubrange> mountainSubranges = new ArrayList<>();
            Log.d(TAG + " getSubrangesByRange()", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainRange = fetchResult.getJSONObject(i);
                int subrangeId = mountainRange.getInt("subrange_id");
                String subrangeName = mountainRange.getString("subrange_name");
                int parentRangeId = mountainRange.getInt("range_id");
                String rangeName = mountainRange.getString("range_name");
                mountainSubranges.add(new MountainSubrange(subrangeId, subrangeName, parentRangeId, rangeName));
            }
            return mountainSubranges;
        });
    }
}
