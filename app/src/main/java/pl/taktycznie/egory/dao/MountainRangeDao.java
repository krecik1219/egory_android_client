package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.MountainRange;

public class MountainRangeDao extends AbstractDao{

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MountainRangeDao.class.getSimpleName();
    private static final String FETCH_ALL_MOUNTAIN_RANGES_URL = "fetch-all-mountain-ranges.php";

    public MountainRangeDao(Context context) {
        super(context);
    }

    public void getAllMountainRanges(OnResourceDataPreparedListener<List<MountainRange>> onResourceDataPreparedListener,
                                     OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getAllMountainRanges()", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_ALL_MOUNTAIN_RANGES_URL;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainRange> mountainRanges = new ArrayList<>();
            Log.d(TAG + " getAllMountainRanges()", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainRange = fetchResult.getJSONObject(i);
                int rangeId = mountainRange.getInt("range_id");
                String rangeName = mountainRange.getString("range_name");
                mountainRanges.add(new MountainRange(rangeId, rangeName));
            }
            return mountainRanges;
        });
    }
}
