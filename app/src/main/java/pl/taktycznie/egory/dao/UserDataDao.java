package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.Resource;
import pl.taktycznie.egory.model.user.User;
import pl.taktycznie.egory.view.fragments.helpers.LoginParametersPack;
import pl.taktycznie.egory.view.fragments.helpers.RegisterParametersPack;

public class UserDataDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + UserDataDao.class.getSimpleName();

    private static final String LOGIN_USER_URL = "login.php";
    private static final String REGISTER_USER_URL = "register.php";

    public UserDataDao(Context context) {
        super(context);
    }

    public void loginUser(LoginParametersPack params,
                          OnResourceDataPreparedListener<User> onResourceDataPreparedListener,
                          OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getTripsByUser()", "entered");
        try {
            JSONObject requestParams = new JSONObject();
            requestParams.put("login", params.login);
            requestParams.put("email", "");  // ignore email - set it to empty string
            requestParams.put("password", params.password);

            String url = LOGIN_USER_URL;
            String requestUrl =
                    DaoConstants.SERVER_URL + url;
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JsonObjectRequest request =
                    new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, jsonResponse -> {
                        try {
                            if (hasAnyErrorOccured(jsonResponse)) {
                                Resource<User> resource = new Resource<>(
                                        null,
                                        new DataAccessError("Server error")
                                );
                                onResourceDataPreparedListener.onResourceDataPrepared(resource);
                                return;
                            }
                            JSONObject loginResult = jsonResponse.getJSONObject("loginResult");
                            Log.d(TAG + " login", "JSON response: " + loginResult.toString());
                            User user = new User(
                                    loginResult.getInt("user_id"),
                                    loginResult.getString("login"),
                                    loginResult.getString("name"),
                                    loginResult.getString("surname")
                            );
                            Resource<User> resource = new Resource<>(user, null);
                            onResourceDataPreparedListener.onResourceDataPrepared(resource);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG + " loginUser", "JSON Exception: " + e.getMessage());
                            Resource<User> resource = new Resource<>(
                                    null,
                                    new DataAccessError("Server data error")
                            );
                            onResourceDataPreparedListener.onResourceDataPrepared(resource);
                        }
                    }, error -> {
                        Log.e(TAG + " loginUser", "Network error");
                        DataAccessError dataAccessError;
                        if(error instanceof TimeoutError || error instanceof NoConnectionError)
                            dataAccessError = new DataAccessError("Connection timeout :(");
                        else
                            dataAccessError = new DataAccessError(error.getMessage());
                        onResponseErrorListener.onResponseError(dataAccessError);
                    });
            requestQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + " loginUser()", "Exception during setting parameters: " + e.getMessage());
            Resource<User> resource = new Resource<>(
                    null,
                    new DataAccessError("Parameters error")
            );
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
        }
    }

    public void registerUser(RegisterParametersPack params,
                             OnResourceDataPreparedListener<Integer> onResourceDataPreparedListener,
                             OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " registerUser()", "entered");
        try {
            String url = REGISTER_USER_URL;
            String requestUrl = DaoConstants.SERVER_URL + url;
            JSONObject requestParams = new JSONObject();
            requestParams.put("login", params.login);
            requestParams.put("email", "");  // ignore email - set it to empty string
            requestParams.put("name", params.name);
            requestParams.put("surname", params.surname);
            requestParams.put("password1", params.password1);
            requestParams.put("password2", params.password2);
            requestParams.put("birthDate", params.birthDate);

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, jsonResponse -> {
                try {
                    if(hasAnyErrorOccured(jsonResponse)) {
                        Resource<Integer> resource = new Resource<>(
                                null,
                                new DataAccessError("Server error")
                        );
                        onResourceDataPreparedListener.onResourceDataPrepared(resource);
                        return;
                    }
                    Log.d(TAG + " registerUser()", "JSON response: " + jsonResponse.toString());
                    JSONObject registerResultJson = jsonResponse.getJSONObject("registerResult");
                    Integer registerResult = registerResultJson.getInt("userId");
                    Resource<Integer> resource = new Resource<>(registerResult, null);
                    onResourceDataPreparedListener.onResourceDataPrepared(resource);
                } catch(JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG + " registerUser()", "Exception: " + e.getMessage());
                    Resource<Integer> resource = new Resource<>(
                            null,
                            new DataAccessError("Server data error")
                    );
                    onResourceDataPreparedListener.onResourceDataPrepared(resource);
                }
            }, error -> {
                Log.e(TAG + "registerUser", "Network error");
                DataAccessError dataAccessError;
                if(error instanceof TimeoutError || error instanceof NoConnectionError)
                    dataAccessError = new DataAccessError("Connection timeout :(");
                else
                    dataAccessError = new DataAccessError(error.getMessage());
                onResponseErrorListener.onResponseError(dataAccessError);
            });
            requestQueue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + " registerUser()", "Exception during setting parameters: " + e.getMessage());
            Resource<Integer> resource = new Resource<>(
                    null,
                    new DataAccessError("Parameters error")
            );
            onResourceDataPreparedListener.onResourceDataPrepared(resource);
        }
    }
}
