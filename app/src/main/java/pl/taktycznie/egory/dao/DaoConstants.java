package pl.taktycznie.egory.dao;

class DaoConstants {

    static final String SERVER_URL = "http://192.168.0.115:80/php_web_service_database_access/api/";
    static final String FETCH_FACADE_PREFIX = "fetch_facade/";
    static final String INSERT_FACADE_PREFIX = "insert_facade/";
    static final String UPDATE_FACADE_PREFIX = "update_facade/";
    static final String DELETE_FACADE_PREFIX = "delete_facade/";
    static final String COMPLEX_TASK_FACADE = "complex_facade/";
    static final long REQUEST_TIMEOUT = 7000L;  // ms
}
