package pl.taktycznie.egory.dao;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.annimon.stream.Optional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.tracking.LocationPoint;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.model.tracking.PhotoTag;
import pl.taktycznie.egory.utils.BitmapUtils;
import pl.taktycznie.egory.utils.DateUtils;

public class LocationDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + LocationDao.class.getSimpleName();
    private static final String FETCH_TRACKING_SESSION_BY_USER_URL = "fetch-tracking-sessions-by-user.php";
    private static final String INSERT_TRACKING_SESSION_URL = "insert-tracking-session.php";
    private static final String INSERT_PHOTO_TAG_URL = "insert-photo-tag-to-point.php";
    private static final String INSERT_LOCATION_POINT_TO_TRACKING_SESSION_URL = "insert-location-point-to-tracking-session.php";
    private static final String UPDATE_TRACKING_SESSION_URL = "update-tracking-session.php";
    private static final String SYNC_LOCATION_SESSIONS_URL = "sync-tracking-sessions.php";

    public LocationDao(Context context) {
        super(context);
    }

    public void getTrackingSessionsByUser(int userId,
                                          OnResourceDataPreparedListener<List<LocationSession>> onResourceDataPreparedListener,
                                          OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getTrackingSessions", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_TRACKING_SESSION_BY_USER_URL + "?userId=" + userId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<LocationSession> locationSessions = new ArrayList<>();
            Log.d(TAG + " getTripPathsByTripId", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject locationSession = fetchResult.getJSONObject(i);
                int locationSessionId = locationSession.getInt("session_id");
                String locationSessionName = locationSession.getString("session_name");
                Date startDatetime = DateUtils.parseDatetimeString(locationSession.getString("start_datetime"));
                Date endDatetime = DateUtils.parseDatetimeString(locationSession.getString("end_datetime"));
                List<LocationPoint> trackingPoints = new ArrayList<>();
                JSONArray points = locationSession.getJSONArray("tracking_points");
                for (int j = 0; j < points.length(); j++) {
                    JSONObject point = points.getJSONObject(i);
                    LocationPoint locationPoint = new LocationPoint(
                            point.getInt("location_point_id"),
                            point.getDouble("latitude"),
                            point.getDouble("longitude"),
                            point.getDouble("altitude"),
                            point.getInt("order_num")
                    );
                    trackingPoints.add(locationPoint);
                }
                locationSessions.add(new LocationSession(locationSessionId, locationSessionName, startDatetime, endDatetime, trackingPoints));
            }
            return locationSessions;
        });
    }

    public Optional<List<LocationSession>> getTrackingSessionsByUserSync(int userId) {
        Log.d(TAG + " getTrackingSessions", "entered");
        String requestUrl = DaoConstants.SERVER_URL + DaoConstants.FETCH_FACADE_PREFIX + FETCH_TRACKING_SESSION_BY_USER_URL + "?userId=" + userId;
        Log.d(TAG + " reqUrl", requestUrl);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RequestFuture<JSONObject> reqFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, requestUrl, null, reqFuture, reqFuture);
        requestQueue.add(request);
        try {
            JSONObject jsonResponse = reqFuture.get(DaoConstants.REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
            JSONArray fetchResult = jsonResponse.getJSONArray("fetchResult");
            Log.d(TAG + " getTrackingSessions", "JSON response: " + fetchResult.toString());
            if(hasAnyErrorOccured(jsonResponse)) {
                return Optional.empty();
            }
            List<LocationSession> locationSessions = new ArrayList<>();
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject locationSession = fetchResult.getJSONObject(i);
                int locationSessionId = locationSession.getInt("session_id");
                String locationSessionName = locationSession.getString("session_name");
                Date startDatetime = DateUtils.parseDatetimeString(locationSession.getString("start_datetime"));
                Date endDatetime = DateUtils.parseDatetimeString(locationSession.getString("end_datetime"));
                List<LocationPoint> trackingPoints = new ArrayList<>();
                JSONArray points = locationSession.getJSONArray("tracking_points");
                for (int j = 0; j < points.length(); j++) {
                    JSONObject point = points.getJSONObject(j);
                    LocationPoint locationPoint = new LocationPoint(
                            point.getInt("location_point_id"),
                            point.getDouble("latitude"),
                            point.getDouble("longitude"),
                            point.getDouble("altitude"),
                            point.getInt("order_num")
                    );
                    JSONArray photoTags = point.getJSONArray("photo_tags");
                    for(int k = 0; k < photoTags.length(); k++) {
                        JSONObject photoTag = photoTags.getJSONObject(k);
                        int photoTagId = photoTag.getInt("photo_tag_id");
                        String base64EncodedImage = photoTag.getString("image");
                        String filePath = BitmapUtils.saveBase64ImageOnDevice(base64EncodedImage,
                                photoTagId + ".jpg", PhotoTag.PHOTO_TAGS_DIR);
                        PhotoTag tag = new PhotoTag(photoTagId, filePath);
                        locationPoint.addPhotoTag(tag);
                    }
                    trackingPoints.add(locationPoint);
                }
                locationSessions.add(new LocationSession(locationSessionId, locationSessionName, startDatetime, endDatetime, trackingPoints));
            }
            return Optional.of(locationSessions);
        } catch (Exception e) {
            Log.e(TAG, "getTrackingSessionsByUserSync ERROR, exception: " + e.getMessage());
            return Optional.empty();
        }
    }

    public void insertTrackingSession(int userId,
                                      LocationSession locationSession,
                                      OnResourceDataPreparedListener<Integer> onResourceDataPreparedListener,
                                      OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " insertTrackingSession", "entered");
        String url = DaoConstants.INSERT_FACADE_PREFIX + INSERT_TRACKING_SESSION_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("sessionName", locationSession.getLocationSessionId());
        params.put("startDatetime", DateUtils.datetimeToString(locationSession.getStartDatetime()));
        insert(url, params, onResourceDataPreparedListener, onResponseErrorListener, insertResult -> insertResult.getInt("lastId"));
    }

    public Optional<Integer> insertTrackingSessionSync(int userId, LocationSession locationSession) {
        Log.d(TAG + " insertSessionSync()", "entered");
        String requestUrl = DaoConstants.SERVER_URL + DaoConstants.INSERT_FACADE_PREFIX + INSERT_TRACKING_SESSION_URL;
        Log.d(TAG + " reqUrl", requestUrl);
        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("userId", userId);
            requestParams.put("sessionName", locationSession.getSessionName());
            requestParams.put("startDatetime", DateUtils.datetimeToString(locationSession.getStartDatetime()));
        } catch (JSONException e) {
            Log.e(TAG, "insertTrackingSessionSync ERROR, JSONException: " + e.getMessage());
            return Optional.empty();
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RequestFuture<JSONObject> reqFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, reqFuture, reqFuture);
        requestQueue.add(request);
        try {
            JSONObject response = reqFuture.get(DaoConstants.REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
            Log.d(TAG + " insert session", "JSON response: " + response.toString());
            return Optional.of((response.getJSONObject("insertResult").getInt("lastId")));
        } catch (Exception e) {
            Log.e(TAG, "insertTrackingSessionSync ERROR, exception: " + e.getMessage());
            return Optional.empty();
        }
    }

    public Optional<Integer> insertLocationPointToTrackingSessionSync(LocationPoint locationPoint, LocationSession locationSession) {
        Log.d(TAG + " insertPtToSessSync()", "entered");
        String requestUrl = DaoConstants.SERVER_URL + DaoConstants.INSERT_FACADE_PREFIX + INSERT_LOCATION_POINT_TO_TRACKING_SESSION_URL;
        Log.d(TAG + " reqUrl", requestUrl);
        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("sessionId", locationSession.getLocationSessionId());
            requestParams.put("latitude", locationPoint.getLatitude());
            requestParams.put("longitude",locationPoint.getLongitude());
            requestParams.put("altitude",locationPoint.getAltitude());
            requestParams.put("orderNum", locationPoint.getOrderNum());
        } catch (JSONException e) {
            Log.e(TAG, "insertLocationPointToTrackingSessionSync ERROR, JSONException: " + e.getMessage());
            return Optional.empty();
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RequestFuture<JSONObject> reqFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, reqFuture, reqFuture);
        requestQueue.add(request);
        try {
            JSONObject response = reqFuture.get(DaoConstants.REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
            Log.d(TAG + " point to session", "JSON response: " + response.toString());
            return Optional.of((response.getJSONObject("insertResult").getInt("lastId")));
        } catch (Exception e) {
            Log.e(TAG, "insertLocationPointToTrackingSessionSync ERROR, exception: " + e.getMessage());
            return Optional.empty();
        }
    }

    public void updateTrackingSession(LocationSession locationSession,
                                      OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                                      OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " updateTrackingSession", "entered");
        String url = DaoConstants.UPDATE_FACADE_PREFIX + UPDATE_TRACKING_SESSION_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("sessionId", locationSession.getLocationSessionId());
        params.put("endDatetime", DateUtils.datetimeToString(locationSession.getEndDatetime()));
        update(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }

    public boolean updateTrackingSessionSync(LocationSession locationSession) {
        Log.d(TAG + " update()", "entered");
        String requestUrl = DaoConstants.SERVER_URL + DaoConstants.UPDATE_FACADE_PREFIX + UPDATE_TRACKING_SESSION_URL;
        Log.d(TAG + " reqUrl", requestUrl);
        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("sessionId", locationSession.getLocationSessionId());
            requestParams.put("sessionName", locationSession.getSessionName());
            requestParams.put("endDatetime", DateUtils.datetimeToString(locationSession.getEndDatetime()));
        } catch (JSONException e) {
            return false;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RequestFuture<JSONObject> reqFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, reqFuture, reqFuture);
        requestQueue.add(request);
        try {
            JSONObject response = reqFuture.get(DaoConstants.REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
            Log.d(TAG + " update session", "JSON response: " + response.toString());
            return !hasAnyErrorOccured(response);
        } catch (Exception e) {
            Log.e(TAG, "updateTrackingSessionSync ERROR, exception: " + e.getMessage());
            return false;
        }
    }

    public Optional<List<LocationSession>> syncTrackingSessionsByUserId(int userId, Queue<LocationSession> syncAwaiting) {
        Log.d(TAG + " sync()", "entered");
        String requestUrl = DaoConstants.SERVER_URL + DaoConstants.COMPLEX_TASK_FACADE + SYNC_LOCATION_SESSIONS_URL;
        Log.d(TAG + " reqUrl", requestUrl);
        JSONObject requestParams = new JSONObject();
        JSONArray sessions = new JSONArray();
        try {
            for(LocationSession ls : syncAwaiting) {
                JSONObject locationSession = new JSONObject();
                locationSession.put("sessionId", ls.getLocationSessionId());
                locationSession.put("sessionName", ls.getSessionName());
                locationSession.put("startDatetime", DateUtils.datetimeToString(ls.getStartDatetime()));
                if(ls.getEndDatetime() != null)
                    locationSession.put("endDatetime", DateUtils.datetimeToString(ls.getEndDatetime()));
                JSONArray locationPoints = new JSONArray();
                for(LocationPoint point : ls.getSyncAwaiting()) {
                    JSONObject trackingPoint = new JSONObject();
                    trackingPoint.put("latitude", point.getLatitude());
                    trackingPoint.put("longitude", point.getLongitude());
                    trackingPoint.put("altitude", point.getAltitude());
                    trackingPoint.put("orderNum", point.getOrderNum());
                    locationPoints.put(trackingPoint);
                }
                locationSession.put("trackingPoints", locationPoints);
                sessions.put(locationSession);
            }
            requestParams.put("userId", userId);
            requestParams.put("sessions", sessions);
        } catch (JSONException e) {
            Log.e(TAG, "syncTrackingSessionsByUserId ERROR, JSONEexception: " + e.getMessage());
            return Optional.empty();
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RequestFuture<JSONObject> reqFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, reqFuture, reqFuture);
        requestQueue.add(request);
        try {
            JSONObject jsonResponse = reqFuture.get(DaoConstants.REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
            JSONArray fetchResult = jsonResponse.getJSONArray("taskResult");
            Log.d(TAG + " getTrackingSessions", "JSON response: " + fetchResult.toString());
            if(hasAnyErrorOccured(jsonResponse)) {
                return Optional.empty();
            }
            List<LocationSession> locationSessions = new ArrayList<>();
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject locationSession = fetchResult.getJSONObject(i);
                int locationSessionId = locationSession.getInt("session_id");
                String locationSessionName = locationSession.getString("session_name");
                Date startDatetime = DateUtils.parseDatetimeString(locationSession.getString("start_datetime"));
                Date endDatetime = DateUtils.parseDatetimeString(locationSession.getString("end_datetime"));
                List<LocationPoint> trackingPoints = new ArrayList<>();
                JSONArray points = locationSession.getJSONArray("tracking_points");
                for (int j = 0; j < points.length(); j++) {
                    JSONObject point = points.getJSONObject(j);
                    LocationPoint locationPoint = new LocationPoint(
                            point.getInt("location_point_id"),
                            point.getDouble("latitude"),
                            point.getDouble("longitude"),
                            point.getDouble("altitude"),
                            point.getInt("order_num")
                    );
                    trackingPoints.add(locationPoint);
                }
                locationSessions.add(new LocationSession(locationSessionId, locationSessionName, startDatetime, endDatetime, trackingPoints));
            }
            return Optional.of(locationSessions);
        } catch (Exception e) {
            Log.e(TAG, "syncTrackingSessionsByUserId ERROR, exception: " + e.getMessage());
            return Optional.empty();
        }
    }

    public Optional<Integer> insertLocationPointPhotoTagSync(LocationPoint lastPoint, Bitmap photo) {
        Log.d(TAG + " insertPhotoTagSync()", "entered, last point id: " + lastPoint.getTrackingPointId() + " photo: " + photo);
        String requestUrl = DaoConstants.SERVER_URL + DaoConstants.INSERT_FACADE_PREFIX + INSERT_PHOTO_TAG_URL;
        Log.d(TAG + " reqUrl", requestUrl);
        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("image", BitmapUtils.encodeBitmapJpgToBase64String(photo));
            requestParams.put("locationPointId", lastPoint.getTrackingPointId());
        } catch (JSONException e) {
            Log.e(TAG, "insertLocationPointPhotoTagSync ERROR, JSONException: " + e.getMessage());
            return Optional.empty();
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RequestFuture<JSONObject> reqFuture = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, requestUrl, requestParams, reqFuture, reqFuture);
        requestQueue.add(request);
        try {
            JSONObject response = reqFuture.get(DaoConstants.REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
            Log.d(TAG + " insert photo tag", "JSON response: " + response.toString());
            return Optional.of((response.getJSONObject("insertResult").getInt("lastId")));
        } catch (Exception e) {
            Log.e(TAG, "insertLocationPointPhotoTagSync ERROR, exception: " + e.getMessage());
            return Optional.empty();
        }
    }
}
