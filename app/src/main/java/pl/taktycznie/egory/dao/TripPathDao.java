package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.model.TripPath;

public class TripPathDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + TripPathDao.class.getSimpleName();
    private static final String FETCH_TRIP_PATHS_BY_TRIP_URL = "fetch-trip-paths-by-trip.php";
    private static final String INSERT_TRIP_PATH_URL = "insert-trip-path.php";
    private static final String DELETE_TRIP_PATH_URL = "delete-trip-path.php";
    private static final String UPDATE_TRIP_PATH_URL = "update-trip-path.php";

    public TripPathDao(Context context) {
        super(context);
    }

    public void getTripPathsByTrip(int tripId,
                                   OnResourceDataPreparedListener<List<TripPath>> onResourceDataPreparedListener,
                                   OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " getTripPathsByTripId", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_TRIP_PATHS_BY_TRIP_URL + "?tripId=" + tripId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<TripPath> tripPaths = new ArrayList<>();
            Log.d(TAG + " getTripPathsByTripId", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject tripPath = fetchResult.getJSONObject(i);
                int tripPathId = tripPath.getInt("trip_path_id");
                int gotPathId = tripPath.getInt("got_path_id");
                String startPointName = tripPath.getString("start_point_name");
                String startPointSubrangeName = tripPath.getString("start_point_subrange_name");
                String endPointName = tripPath.getString("end_point_name");
                String endPointSubrangeName = tripPath.getString("end_point_subrange_name");
                int upPoints = tripPath.getInt("up_points");
                int downPoints = tripPath.getInt("down_points");
                int parentTripId = tripPath.getInt("trip_id");
                int pathStatusId = tripPath.getInt("path_status_id");
                int pathOrderNumber = tripPath.getInt("path_order_number");
                boolean isDone = intToBoolean(tripPath.getInt("is_done"));

                tripPaths.add(
                        new TripPath(
                                tripPathId,
                                gotPathId,
                                startPointName,
                                startPointSubrangeName,
                                endPointName,
                                endPointSubrangeName,
                                upPoints,
                                downPoints,
                                parentTripId,
                                pathStatusId,
                                pathOrderNumber,
                                isDone));
            }
            return tripPaths;
        });
    }

    public void insertTripPath(TripPath tripPath,
                               OnResourceDataPreparedListener<Integer> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " insertTripPath()", "entered");
        String url = DaoConstants.INSERT_FACADE_PREFIX + INSERT_TRIP_PATH_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("gotPathId", tripPath.getGotPathId());
        params.put("startPointName", tripPath.getStartPointName());
        params.put("startPointSubrangeName", tripPath.getStartPointSubrangeName());
        params.put("endPointName", tripPath.getEndPointName());
        params.put("endPointSubrangeName", tripPath.getEndPointSubrangeName());
        params.put("upPoints", tripPath.getUpPoints());
        params.put("downPoints", tripPath.getDownPoints());
        params.put("tripId", tripPath.getTripId());
        params.put("pathOrderNumber", tripPath.getPathOrderNumber());
        insert(url, params, onResourceDataPreparedListener, onResponseErrorListener, insertResult -> insertResult.getInt("lastId"));
    }

    public void deleteTripPath(TripPath tripPath,
                               OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " deleteTrippath()", "entered");
        String url = DaoConstants.DELETE_FACADE_PREFIX + DELETE_TRIP_PATH_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("tripPathId", tripPath.getTripPathId());
        delete(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }

    public void updateTripPath(TripPath tripPath,
                               OnResourceDataPreparedListener<EmptyData> onResourceDataPreparedListener,
                               OnResponseErrorListener onResponseErrorListener) {
        Log.d(TAG + " updateTripPath()", "entered");
        String url = DaoConstants.UPDATE_FACADE_PREFIX + UPDATE_TRIP_PATH_URL;
        Map<String, Object> params = new HashMap<>();
        params.put("tripPathId", tripPath.getTripPathId());
        params.put("isDone", booleanToInt(!tripPath.isDone()));
        update(url, params, onResourceDataPreparedListener, onResponseErrorListener);
    }

    private boolean intToBoolean(int i) {
        return i != 0;
    }

    private int booleanToInt(boolean b) {
        return b? 1: 0;
    }
}
