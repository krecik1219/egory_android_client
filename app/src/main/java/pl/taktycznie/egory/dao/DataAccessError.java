package pl.taktycznie.egory.dao;

public class DataAccessError {
    private final String errorMessage;

    public DataAccessError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
