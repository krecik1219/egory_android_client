package pl.taktycznie.egory.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.taktycznie.egory.globals.GlobalConstants;
import pl.taktycznie.egory.helpers.Resource;
import pl.taktycznie.egory.model.MountainPoint;
import pl.taktycznie.egory.model.tracking.LocationPoint;
import pl.taktycznie.egory.model.tracking.LocationSession;
import pl.taktycznie.egory.providers.LocationSessionsImageProvider;

public class MountainPointDao extends AbstractDao {

    private static final String TAG = GlobalConstants.TAG_PROLOGUE + MountainPointDao.class.getSimpleName();
    private static final String FETCH_ALL_MOUNTAIN_POINTS_URL = "fetch-all-mountain-points.php";
    private static final String FETCH_END_POINTS_BY_START_POINT_URL = "fetch-end-points-by-start-point.php";
    private static final String FETCH_START_POINTS_BY_END_POINT_URL = "fetch-start-points-by-end-point.php";
    private LocationSessionsImageProvider provider;

    public MountainPointDao(Context context) {
        super(context);
    }

    public void getAllMountainPoints(OnResourceDataPreparedListener<List<MountainPoint>> onResourceDataPreparedListener,
                                     OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getAllMountainPoints()", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_ALL_MOUNTAIN_POINTS_URL;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainPoint> mountainPoints = new ArrayList<>();
            Log.d(TAG + " getAllMountainPoints()", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainPoint = fetchResult.getJSONObject(i);
                int pointId = mountainPoint.getInt("point_id");
                String pointName = mountainPoint.getString("point_name");
                int subrangeId = mountainPoint.getInt("subrange_id");
                String subrangeName = mountainPoint.getString("subrange_name");
                int rangeId = mountainPoint.getInt("range_id");
                String rangeName = mountainPoint.getString("range_name");
                double latitude = mountainPoint.getDouble("latitude");
                double longitude = mountainPoint.getDouble("longitude");
                double altitude = mountainPoint.getDouble("altitude");
                mountainPoints.add(new MountainPoint(pointId, pointName, subrangeId, subrangeName, rangeId, rangeName, latitude, longitude, altitude));
            }
            return mountainPoints;
        });
    }

   public void getSpecificMountainPoints(OnResourceDataPreparedListener<List<MountainPoint>> onResourceDataPreparedListener,
                                     OnResponseErrorListener onResponseErrorListener)
    {

        Log.d(TAG + " getAllMountainPoints()", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_ALL_MOUNTAIN_POINTS_URL;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainPoint> mountainPoints = new ArrayList<>();
            Log.d(TAG + " getAllMountainPoints()", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainPoint = fetchResult.getJSONObject(i);
                int pointId = mountainPoint.getInt("point_id");
                String pointName = mountainPoint.getString("point_name");
                int subrangeId = mountainPoint.getInt("subrange_id");
                String subrangeName = mountainPoint.getString("subrange_name");
                int rangeId = mountainPoint.getInt("range_id");
                String rangeName = mountainPoint.getString("range_name");
                double latitude = mountainPoint.getDouble("latitude");
                double longitude = mountainPoint.getDouble("longitude");
                double altitude = mountainPoint.getDouble("altitude");
                mountainPoints.add(new MountainPoint(pointId, pointName, subrangeId, subrangeName, rangeId, rangeName, latitude, longitude, altitude));
            }
            List<MountainPoint> mountainPointsSelect = new ArrayList<>();

            /*List<LocationSession> list=provider.getLocationSessionsImage();

            boolean find;

            for(int i=0;i<mountainPoints.size();i++)
            {
                find=false;
                for(int k=0;k<list.size();k++)
                for(int j=0;j<list.get(k).getLocationPoints().size();j++)
                {
                    if (distance(mountainPoints.get(i).getLatitude(), list.get(k).getLocationPoints().get(j).getLatitude(), mountainPoints.get(i).getLongitude(), list.get(k).getLocationPoints().get(j).getLongitude(), mountainPoints.get(i).getAltitude(), list.get(k).getLocationPoints().get(j).getAltitude()) < 100)
                         find=true;
                }

                if(find)
                    mountainPointsSelect.add(mountainPoints.get(i));
            }*/

            LocationPoint lp=new LocationPoint(49.2607771,20.0895038,1238.845825195312,3);
            LocationPoint lp2=new LocationPoint(49.260774,20.0895036,1238.845825195315,3);
            LocationPoint lp3=new LocationPoint(49.2404045,20.0366751,1654.149291992188,3);
            LocationPoint lp4=new LocationPoint(49.1979133,20.0424179, 	2076.076904296875,3);
            LocationPoint lp5=new LocationPoint(49.1979133,20.0424179, 	2076.076904296875,3);
            List<LocationPoint>listaLocPoint=new ArrayList<LocationPoint>();
            listaLocPoint.add((lp));
            listaLocPoint.add((lp2));
            listaLocPoint.add((lp3));
            listaLocPoint.add((lp4));
            listaLocPoint.add((lp5));
            Date date=new Date(2019,01,01);
            Date date2=new Date(2019,02,01);
           LocationSession ls=new LocationSession(1,"Super",date ,date2,listaLocPoint);
            int k = mountainPoints.size();
            for (int i = 0; i < k; i++) {
                for (int j = 0; j < ls.getLocationPoints().size(); j++) {
                    if (distance(mountainPoints.get(i).getLatitude(), ls.getLocationPoints().get(j).getLatitude(), mountainPoints.get(i).getLongitude(), ls.getLocationPoints().get(j).getLongitude(), mountainPoints.get(i).getAltitude(), ls.getLocationPoints().get(j).getAltitude()) < 100)
                        mountainPointsSelect.add(mountainPoints.get(i));
                }
            }

            return mountainPointsSelect;
        });
    }


public void getSpecificMountainPoints2(OnResourceDataPreparedListener<List<List<MountainPoint>>> onResourceDataPreparedListener,
        OnResponseErrorListener onResponseErrorListener)
        {

        Log.d(TAG + " getAllMountainPoints()", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_ALL_MOUNTAIN_POINTS_URL;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
        List<MountainPoint> mountainPoints = new ArrayList<>();
        Log.d(TAG + " getAllMountainPoints()", "JSON response: " + fetchResult.toString());
        for(int i = 0; i < fetchResult.length(); i++) {
        JSONObject mountainPoint = fetchResult.getJSONObject(i);
        int pointId = mountainPoint.getInt("point_id");
        String pointName = mountainPoint.getString("point_name");
        int subrangeId = mountainPoint.getInt("subrange_id");
        String subrangeName = mountainPoint.getString("subrange_name");
        int rangeId = mountainPoint.getInt("range_id");
        String rangeName = mountainPoint.getString("range_name");
        double latitude = mountainPoint.getDouble("latitude");
        double longitude = mountainPoint.getDouble("longitude");
        double altitude = mountainPoint.getDouble("altitude");
        mountainPoints.add(new MountainPoint(pointId, pointName, subrangeId, subrangeName, rangeId, rangeName, latitude, longitude, altitude));
        }

        List<List<MountainPoint>> listsMountainPointsSelect = new ArrayList<>();

            /*List<LocationSession> list=provider.getLocationSessionsImage();

            boolean find;

            for(int i=0;i<mountainPoints.size();i++)
            {
                find=false;
                for(int k=0;k<list.size();k++)
                for(int j=0;j<list.get(k).getLocationPoints().size();j++)
                {
                    if (distance(mountainPoints.get(i).getLatitude(), list.get(k).getLocationPoints().get(j).getLatitude(), mountainPoints.get(i).getLongitude(), list.get(k).getLocationPoints().get(j).getLongitude(), mountainPoints.get(i).getAltitude(), list.get(k).getLocationPoints().get(j).getAltitude()) < 100)
                         find=true;
                }

                if(find)
                    mountainPointsSelect.add(mountainPoints.get(i));
            }*/
            List<LocationSession> list=new ArrayList<>();

        LocationPoint lp=new LocationPoint(49.2607771,20.0895038,1238.845825195312,3);
        LocationPoint lp2=new LocationPoint(49.260774,20.0895036,1238.845825195315,3);
        LocationPoint lp3=new LocationPoint(49.2404045,20.0366751,1654.149291992188,3);
        LocationPoint lp4=new LocationPoint(49.1979133,20.0424179, 	2076.076904296875,3);
        LocationPoint lp5=new LocationPoint(49.1979133,20.0424179, 	2076.076904296875,3);
        List<LocationPoint>listaLocPoint=new ArrayList<LocationPoint>();
        listaLocPoint.add((lp));
        listaLocPoint.add((lp2));
        listaLocPoint.add((lp3));
        listaLocPoint.add((lp4));
        listaLocPoint.add((lp5));
        Date date=new Date(2019,01,01);
        Date date2=new Date(2019,02,01);
        LocationSession ls=new LocationSession(1,"Super",date ,date2,listaLocPoint);

            LocationPoint lp6=new LocationPoint(49.2607771,20.0895038,1238.845825195312,3);
            LocationPoint lp7=new LocationPoint(49.260774,20.0895036,1238.845825195315,3);
            LocationPoint lp8=new LocationPoint(49.2404045,20.0366751,1654.149291992188,3);
            LocationPoint lp9=new LocationPoint(49.1979133,20.0424179, 	2076.076904296875,3);
            LocationPoint lp10=new LocationPoint(49.1979133,20.0424179, 	2076.076904296875,3);
            List<LocationPoint>listaLocPoint2=new ArrayList<LocationPoint>();
            listaLocPoint2.add((lp6));
            listaLocPoint2.add((lp7));
            listaLocPoint2.add((lp8));
            listaLocPoint2.add((lp9));
            listaLocPoint2.add((lp10));
            Date date3=new Date(2019,01,01);
            Date date4=new Date(2019,02,01);
            LocationSession ls2=new LocationSession(1,"Super",date3 ,date4,listaLocPoint2);
            list.add(ls);
            list.add(ls2);

        int k = mountainPoints.size();
        for(int h=0;h<list.size();h++)
        for (int i = 0; i < k; i++) {
        for (int j = 0; j < ls.getLocationPoints().size(); j++) {
            if (distance(mountainPoints.get(i).getLatitude(), ls.getLocationPoints().get(j).getLatitude(), mountainPoints.get(i).getLongitude(), ls.getLocationPoints().get(j).getLongitude(), mountainPoints.get(i).getAltitude(), ls.getLocationPoints().get(j).getAltitude()) < 100) {

                listsMountainPointsSelect.add(new ArrayList<>());//
                listsMountainPointsSelect.get(h).add(mountainPoints.get(i));
            }
        }
        }

        return listsMountainPointsSelect;
        });
        }



    public void getEndPointsBasedOnStartPoint(int startPointId,
                                              OnResourceDataPreparedListener<List<MountainPoint>> onResourceDataPreparedListener,
                                              OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getEndPoints", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_END_POINTS_BY_START_POINT_URL + "?startPointId=" + startPointId;
        fetch(url, onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainPoint> mountainPoints = new ArrayList<>();
            Log.d(TAG + " getEndPoints()", "JSON response: " + fetchResult.toString());
            for (int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainPoint = fetchResult.getJSONObject(i);
                int pointId = mountainPoint.getInt("end_point_id");
                String pointName = mountainPoint.getString("point_name");
                int subrangeId = mountainPoint.getInt("subrange_id");
                String subrangeName = mountainPoint.getString("subrange_name");
                int rangeId = mountainPoint.getInt("range_id");
                String rangeName = mountainPoint.getString("range_name");
                double latitude = mountainPoint.getDouble("latitude");
                double longitude = mountainPoint.getDouble("longitude");
                double altitude = mountainPoint.getDouble("altitude");
                mountainPoints.add(new MountainPoint(pointId, pointName, subrangeId, subrangeName, rangeId, rangeName, latitude, longitude, altitude));
            }
            return mountainPoints;
        });
    }

    public void getStartPointsBasedOnEndPoint(int endPointId,
                                              OnResourceDataPreparedListener<List<MountainPoint>> onResourceDataPreparedListener,
                                              OnResponseErrorListener onResponseErrorListener)
    {
        Log.d(TAG + " getStartPoints", "entered");
        String url = DaoConstants.FETCH_FACADE_PREFIX + FETCH_START_POINTS_BY_END_POINT_URL + "?endPointId=" + endPointId;
        fetch(url ,onResourceDataPreparedListener, onResponseErrorListener, fetchResult -> {
            List<MountainPoint> mountainPoints = new ArrayList<>();
            Log.d(TAG + " getStartPoints()", "JSON response: " + fetchResult.toString());
            for(int i = 0; i < fetchResult.length(); i++) {
                JSONObject mountainPoint = fetchResult.getJSONObject(i);
                int pointId = mountainPoint.getInt("start_point_id");
                String pointName = mountainPoint.getString("point_name");
                int subrangeId = mountainPoint.getInt("subrange_id");
                String subrangeName = mountainPoint.getString("subrange_name");
                int rangeId = mountainPoint.getInt("range_id");
                String rangeName = mountainPoint.getString("range_name");
                double latitude = mountainPoint.getDouble("latitude");
                double longitude = mountainPoint.getDouble("longitude");
                double altitude = mountainPoint.getDouble("altitude");
                mountainPoints.add(new MountainPoint(pointId, pointName, subrangeId, subrangeName, rangeId, rangeName, latitude, longitude, altitude));
            }
            return mountainPoints;
        });
    }

    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double al1, double al2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = al1 - al2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}
